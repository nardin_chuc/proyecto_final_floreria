/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Conexion.Conexion;
import Controlador.ControladorLogin;
import Controlador.ControladorNotificaciones;
import Controlador.ControladorPedidos;
import Controlador.ControladorPerecederos;
import Interfaz.Notificaciones;
import Interfaz.Pedidos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import sms.HttpJavaAltiria;

/**
 *
 * @author CCNAR
 */
public class ModeloMensaje {
    
    String telefonos [];

    HttpJavaAltiria men = new HttpJavaAltiria();
    ControladorLogin clg = new ControladorLogin();
    Notificaciones nt = new Notificaciones();
    ControladorNotificaciones cnt = new ControladorNotificaciones(nt);
    Pedidos ped = new Pedidos();
    ControladorPedidos pe = new ControladorPedidos(ped);
    ControladorPerecederos pere = new ControladorPerecederos();
    
    String fecha = "";
    
    
    boolean opcion = true;

    public ModeloMensaje() {

    }

    public void Mensaje() {
        pere.DiasAutomaticos(fecha);
        cnt.BuscarNotificaciones();
        pe.DiasAutomaticos();
        BuscarTelefono();
        System.out.println("cant "+telefonos.length);
        String mensaje = clg.EnviarMensaje();
        for(int i= 0; i < telefonos.length;i++){
         String numero = telefonos[i];
         AutorizarMsj(opcion,"+52"+numero,mensaje);
         VerificarContenido(numero);
        }
                //Se envia dependiendo de la bandera de la opcion 
    }

    public void VerificarContenido(String numero) {
        if (cnt.getPedidos().isEmpty() && !(cnt.getPerecederos().isEmpty())) {
            String mensaje = cnt.getPerecederos();
            System.out.println("mes 1 "+mensaje);
            AutorizarMsj(opcion,"+52"+numero,mensaje);        //Se envia dependiendo de la bandera de la opcion 
        } else if (!(cnt.getPedidos().isEmpty()) && (cnt.getPerecederos().isEmpty())) {
            String mensaje = cnt.getPedidos();
            System.out.println("mes 1 "+mensaje);
            AutorizarMsj(opcion,"+52"+numero,mensaje);        //Se envia dependiendo de la bandera de la opcion 
        } else if (!(cnt.getPerecederos().isEmpty()) && !(cnt.getPedidos().isEmpty())) {
            String mensaje = "";
            mensaje = cnt.getPedidos();
            mensaje = mensaje + cnt.getPerecederos();
            System.out.println("mes 1 "+mensaje);
             AutorizarMsj(opcion,"+52"+numero,mensaje);        //Se envia dependiendo de la bandera de la opcion 
        } else if (cnt.getPedidos().isEmpty() && cnt.getPerecederos().isEmpty()) {
            System.out.println("cnt.getPedidos() "+cnt.getPedidos());
            System.exit(0);
        }
    }
    
    public void AutorizarMsj(boolean op, String numero, String mensaje){
        if(op){
            men.EnviarSMS(numero, mensaje);
        }else{
            
        }
    }
    
      public int CantidadTelefono() {
            
        int cantidad = 0;
        Connection conectar = Conexion.establecerConnection();
        ResultSet rs;
        PreparedStatement ps;
        try {
            ps = conectar.prepareStatement("SELECT COUNT(Telefono) as cant FROM db_Usuarios WHERE Cargo = ? AND Estado = 'Activo'");
            ps.setString(1, "Administrador");
            rs = ps.executeQuery();
            while (rs.next()) {
                cantidad = (rs.getInt("cant"));
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cantidad;
    }
    
        public void BuscarTelefono() {
            int cantidad = CantidadTelefono();
            telefonos=new String[cantidad];
        Connection conectar = Conexion.establecerConnection();
        ResultSet rs;
        PreparedStatement ps;
        try {
            ps = conectar.prepareStatement("SELECT Telefono FROM db_Usuarios WHERE Cargo = ? AND Estado = 'Activo'");
            ps.setString(1, "Administrador");
            rs = ps.executeQuery();
            int i= 0;
            while (rs.next()) {
                telefonos[i] = rs.getString("Telefono");
                i++;
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
      public void VerificarFecha() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        LocalDateTime dia = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        fecha = dia.format(formato);
        String fechaSQLPedi = "";
        String fechaSQLPere = "";
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT FechaMensaje FROM db_Pedidos WHERE DiasPendEstado = 'Preparación' OR DiasPendEstado='Hoy'");
            rs = ps.executeQuery();
            while (rs.next()) {
                fechaSQLPedi = (rs.getString("FechaMensaje"));
            }
            System.out.println("fecha "+fechaSQLPedi);
            rs.close();
        } catch (Exception e) {
            System.err.println("error buescarEstaoPedido : " + e.getMessage());
        }
        
        if(fecha.equals(fechaSQLPedi)){
        }else{
            
            Mensaje();
            ModificarFecha(fecha);
        }
    }
      
      
      public void ModificarFecha(String fecha){
          try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("UPDATE db_Pedidos SET FechaMensaje = ? WHERE DiasPendEstado = 'Preparación' OR DiasPendEstado='Hoy'");
            ps.setString(1, fecha);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            System.err.println("error ModificarEstadoPere : " + e.getMessage());
        }
          
      }
    
    
    
    

}
