/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Controlador.ControladorLogin;
import Controlador.ControladorNotificaciones;
import Interfaz.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.swing.ImageIcon;
//import javax.swing.Icon;
//import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author HP
 */
public class Bienvenida extends javax.swing.JFrame {

    String contraseña;
    String contraNueva;
    String nombre;
    String User;
    String Contra;
    int inicio = 001;

    private boolean bandera;

    public boolean getBandera() {
        return bandera;
    }

    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }

    public Bienvenida() {
        initComponents();

        this.setLocationRelativeTo(this);
        this.setTitle(" Floreria y Productos de limpieza ");
        setIconImage(new ImageIcon(getClass().getResource("/Imag/logo_LaVanda.png")).getImage());
        this.setLocation(0, 0);
        dia();
        Mes();
        Año();
        txt_fecha.setVisible(false);
        txt_Mes.setVisible(false);
        txt_AÑO.setVisible(false);
        jLabel18.setVisible(false);
        jLabel19.setVisible(false);
        jLabel20.setVisible(false);
    }

    public void dia() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");

        LocalDateTime dia = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        String fecha = dia.format(formato);
        txt_fecha.setText("" + fecha);
    }

    public void Mes() {
        LocalDate currentDate = LocalDate.now();
        Month month = currentDate.getMonth();
        String mes = month.getDisplayName(TextStyle.FULL, Locale.US);

        if (mes.equals("Junuary")) {
            txt_Mes.setText("ENERO");
        } else if (mes.equals("July")) {
            txt_Mes.setText("February");
        } else if (mes.equals("March")) {
            txt_Mes.setText("MARZO");
        } else if (mes.equals("April")) {
            txt_Mes.setText("ABRIL");
        } else if (mes.equals("May")) {
            txt_Mes.setText("MAYO");
        } else if (mes.equals("June")) {
            txt_Mes.setText("JUNIO");
        } else if (mes.equals("July")) {
            txt_Mes.setText("JULIO");
        } else if (mes.equals("August")) {
            txt_Mes.setText("AGOSTO");
        } else if (mes.equals("October")) {
            txt_Mes.setText("OCTUBRE");
        } else if (mes.equals("September")) {
            txt_Mes.setText("SEPTIEMBRE");
        } else if (mes.equals("November")) {
            txt_Mes.setText("Noviembre");
        } else if (mes.equals("December")) {
            txt_Mes.setText("DICIEMBRE");
        } else {
            txt_Mes.setText("" + month);
        }

    }

    public void Año() {
        LocalDate currentDate = LocalDate.now();
        int month = currentDate.getYear();
        txt_AÑO.setText("" + month);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem8 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        Label_fondo = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        lb_fondo2 = new javax.swing.JLabel();
        Usuario = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_user = new javax.swing.JTextField();
        txt_cargo = new javax.swing.JTextField();
        txt_fecha = new javax.swing.JTextField();
        txt_Mes = new javax.swing.JTextField();
        txt_AÑO = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        AgregarUser = new javax.swing.JMenuItem();
        AgregarProd = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        mn_Avisos = new javax.swing.JMenu();
        avisosMN = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem10 = new javax.swing.JMenuItem();

        jMenuItem8.setText("jMenuItem8");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("INGRESAR");
        setBackground(new java.awt.Color(255, 153, 0));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(195, 142, 186));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Label_fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/logo_floreria.png"))); // NOI18N
        jPanel1.add(Label_fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 0, 990, 350));

        jButton1.setBackground(new java.awt.Color(255, 0, 0));
        jButton1.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("CERRAR SESIÓN");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 160, 42));

        lb_fondo2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/logo_LaVanda.jpeg"))); // NOI18N
        lb_fondo2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jPanel1.add(lb_fondo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 350, 1180, 290));

        Usuario.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        Usuario.setText("USUARIOS: ");
        jPanel1.add(Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 30, -1, -1));

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel2.setText("CARGO: ");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 60, -1, 20));

        txt_user.setEditable(false);
        txt_user.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jPanel1.add(txt_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(1236, 20, 110, -1));

        txt_cargo.setEditable(false);
        txt_cargo.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jPanel1.add(txt_cargo, new org.netbeans.lib.awtextra.AbsoluteConstraints(1236, 60, 110, -1));

        txt_fecha.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        txt_fecha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(txt_fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, 100, -1));

        txt_Mes.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        txt_Mes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(txt_Mes, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 100, -1));

        txt_AÑO.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        txt_AÑO.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel1.add(txt_AÑO, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 169, 100, -1));

        jLabel18.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel18.setText("AÑO");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, -1, -1));

        jLabel19.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel19.setText("MES");
        jPanel1.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, -1, -1));

        jLabel20.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel20.setText("FECHA");
        jPanel1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, -1, -1));

        jMenuBar1.setBackground(new java.awt.Color(204, 0, 0));
        jMenuBar1.setForeground(new java.awt.Color(0, 102, 102));
        jMenuBar1.setFocusable(false);
        jMenuBar1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N

        jMenu1.setBackground(new java.awt.Color(255, 0, 0));
        jMenu1.setForeground(new java.awt.Color(255, 255, 255));
        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/menu.png"))); // NOI18N
        jMenu1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N

        AgregarUser.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        AgregarUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/usuarios.png"))); // NOI18N
        AgregarUser.setText("Usuarios");
        AgregarUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarUserActionPerformed(evt);
            }
        });
        jMenu1.add(AgregarUser);

        AgregarProd.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        AgregarProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/productos.png"))); // NOI18N
        AgregarProd.setText("Productos");
        AgregarProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarProdActionPerformed(evt);
            }
        });
        jMenu1.add(AgregarProd);

        jMenuItem2.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/proveedor.png"))); // NOI18N
        jMenuItem2.setText("Proveedores");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu3.setBackground(new java.awt.Color(255, 0, 0));
        jMenu3.setForeground(new java.awt.Color(255, 255, 255));
        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/perecederos.png"))); // NOI18N
        jMenu3.setText("Perecederos");
        jMenu3.setFont(new java.awt.Font("Comic Sans MS", 3, 18)); // NOI18N

        jMenuItem9.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/ver.png"))); // NOI18N
        jMenuItem9.setText("Ver Perecederos");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem9);

        jMenuBar1.add(jMenu3);

        jMenu2.setBackground(new java.awt.Color(255, 0, 0));
        jMenu2.setForeground(new java.awt.Color(255, 255, 255));
        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/pedidos.png"))); // NOI18N
        jMenu2.setText("Pedidos");
        jMenu2.setFont(new java.awt.Font("Comic Sans MS", 3, 18)); // NOI18N

        jMenuItem1.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/registro.png"))); // NOI18N
        jMenuItem1.setText("Registrar Pedidos");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuItem7.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/ver.png"))); // NOI18N
        jMenuItem7.setText("Ver Pedidos");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        mn_Avisos.setBackground(new java.awt.Color(255, 0, 0));
        mn_Avisos.setForeground(new java.awt.Color(255, 255, 255));
        mn_Avisos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/anunciar.png"))); // NOI18N
        mn_Avisos.setText("AVISOS");
        mn_Avisos.setFont(new java.awt.Font("Comic Sans MS", 3, 18)); // NOI18N

        avisosMN.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        avisosMN.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/advertencia.png"))); // NOI18N
        avisosMN.setText("Ver Avisos");
        avisosMN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                avisosMNActionPerformed(evt);
            }
        });
        mn_Avisos.add(avisosMN);

        jMenuBar1.add(mn_Avisos);

        jMenu5.setBackground(new java.awt.Color(255, 0, 0));
        jMenu5.setForeground(new java.awt.Color(255, 255, 255));
        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/compra.png"))); // NOI18N
        jMenu5.setText("Compras");
        jMenu5.setFont(new java.awt.Font("Comic Sans MS", 3, 18)); // NOI18N

        jMenuItem4.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/registro.png"))); // NOI18N
        jMenuItem4.setText("Realizar Compra");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem4);

        jMenuItem5.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/ver.png"))); // NOI18N
        jMenuItem5.setText("Ver Compras");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem5);

        jMenuBar1.add(jMenu5);

        jMenu4.setBackground(new java.awt.Color(255, 0, 0));
        jMenu4.setForeground(new java.awt.Color(255, 255, 255));
        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/venta.png"))); // NOI18N
        jMenu4.setText("Ventas");
        jMenu4.setFont(new java.awt.Font("Comic Sans MS", 3, 18)); // NOI18N

        jMenuItem3.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/registro.png"))); // NOI18N
        jMenuItem3.setText("Realizar Ventas");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem3);

        jMenuItem6.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/ver.png"))); // NOI18N
        jMenuItem6.setText("Ver Ventas");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem6);

        jMenuBar1.add(jMenu4);

        jMenu6.setBackground(new java.awt.Color(255, 51, 51));
        jMenu6.setForeground(new java.awt.Color(255, 255, 255));
        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/cliente.png"))); // NOI18N
        jMenu6.setText("Clientes");
        jMenu6.setFont(new java.awt.Font("Comic Sans MS", 3, 18)); // NOI18N

        jMenuItem10.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jMenuItem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imag/registro.png"))); // NOI18N
        jMenuItem10.setText("Ingresar Cliente");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem10);

        jMenuBar1.add(jMenu6);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1353, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AgregarUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarUserActionPerformed
        AgregarUsuario user = new AgregarUsuario();
        user.setVisible(true);
    }//GEN-LAST:event_AgregarUserActionPerformed

    private void AgregarProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarProdActionPerformed
        AgregarProducto prod = new AgregarProducto();
        prod.setVisible(true);

    }//GEN-LAST:event_AgregarProdActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        String user = txt_user.getText();
        String Cargo = txt_cargo.getText();
        Ventas vn = new Ventas();
        vn.txt_user.setText(user);
        vn.txt_cargo.setText(Cargo);
        vn.setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        IngresarProveedor pro = new IngresarProveedor();
        pro.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        Login lg = new Login();
        lg.setVisible(true);
        this.setVisible(false);


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        Compras cp = new Compras();
        String user = txt_user.getText();
        String Cargo = txt_cargo.getText();
        cp.txt_user.setText(user);
        cp.txt_cargo.setText(Cargo);
        cp.setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        VerVentas vc = new VerVentas();
        vc.setVisible(true);
        vc.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        VerCompras vc = new VerCompras();
        vc.setVisible(true);
        vc.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        IngresarCliente cl = new IngresarCliente();
        cl.setVisible(true);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Pedidos pe = new Pedidos();
        String user = txt_user.getText();
        String Cargo = txt_cargo.getText();
        pe.txt_user.setText(user);
        pe.txt_cargo.setText(Cargo);
        pe.setVisible(true);
        pe.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        VerPedidos pe = new VerPedidos();
        pe.setLocationRelativeTo(null);
        pe.setVisible(true);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        Perecederos pe = new Perecederos();
        pe.setLocationRelativeTo(null);
        pe.setVisible(true);
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void avisosMNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_avisosMNActionPerformed
        Notificaciones nt = new Notificaciones();
        ControladorNotificaciones nts = new ControladorNotificaciones(nt);
        System.out.println("bander antes " + getBandera());
        if (getBandera() == true) {
            JOptionPane.showMessageDialog(null, "Ya se encuentra abierta la Ventana de Notificaciones");
            System.out.println("bander true " + getBandera());
        } else {
            System.out.println("bander else " + getBandera());
            nt.setLocationRelativeTo(null);
            nt.setVisible(true);
        }


    }//GEN-LAST:event_avisosMNActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Bienvenida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Bienvenida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Bienvenida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Bienvenida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Bienvenida().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AgregarProd;
    public javax.swing.JMenuItem AgregarUser;
    private javax.swing.JLabel Label_fondo;
    private javax.swing.JLabel Usuario;
    public javax.swing.JMenuItem avisosMN;
    private javax.swing.JButton jButton1;
    public javax.swing.JLabel jLabel18;
    public javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel20;
    public javax.swing.JMenu jMenu1;
    public javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    public javax.swing.JMenu jMenu4;
    public javax.swing.JMenu jMenu5;
    public javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    public javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lb_fondo2;
    public javax.swing.JMenu mn_Avisos;
    public javax.swing.JTextField txt_AÑO;
    public javax.swing.JTextField txt_Mes;
    public javax.swing.JTextField txt_cargo;
    public javax.swing.JTextField txt_fecha;
    public javax.swing.JTextField txt_user;
    // End of variables declaration//GEN-END:variables
}
