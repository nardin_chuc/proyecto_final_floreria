/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelos;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author CCNAR
 */
public class Tabla extends DefaultTableCellRenderer{

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 
    if(table.getValueAt(row, column).toString().equals("Inactivo")){
        setBackground(Color.red);
        setForeground(Color.white);
    }else if(table.getValueAt(row, column).toString().equals("Activo")){
        setBackground(Color.white);
        setForeground(Color.black);
    } else if(table.isRowSelected(row)){
        setForeground(Color.white);
        table.setSelectionBackground(new Color(0,120,215));
        
    }else {
        setForeground(Color.black);
    }
    return this;
    }
}
