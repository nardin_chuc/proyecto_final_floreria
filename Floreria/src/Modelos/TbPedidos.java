/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelos;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author CCNAR
 */
    
public class TbPedidos extends DefaultTableCellRenderer{
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 
        
        
    if(table.isRowSelected(row)){
        setForeground(Color.white);
        table.setSelectionBackground(new Color(0,120,215));
    }else if(table.getValueAt(row, 9).toString().equals("0") || table.getValueAt(row, 10).toString().equals("Hoy") || table.getValueAt(row, 9).toString().equals("Hoy")){
        setBackground(Color.green);
        setForeground(Color.black);
    }else if(table.getValueAt(row, 10).toString().equals("Preparación") || table.getValueAt(row, 9).toString().equals("Preparación")){
        setBackground(Color.red);
        setForeground(Color.white);
    }else if(table.getValueAt(row, 10).toString().equals("Pendiente") || table.getValueAt(row, 9).toString().equals("Pendiente") ){
        setBackground(Color.orange);
        setForeground(Color.black);
    }else if(table.getValueAt(row, 10).toString().equals("Registro Activo") || table.getValueAt(row, 9).toString().equals("Registro Activo") ){
        setBackground(Color.PINK);
        setForeground(Color.black);
    }else if(table.getValueAt(row, 10).toString().equals("Cancelado") ||table.getValueAt(row, 9).toString().equals("Cancelado") ){
        setBackground(Color.GRAY);
        setForeground(Color.black);
    }else if(table.getValueAt(row, 10).toString().equals("Terminado") || table.getValueAt(row, 9).toString().equals("Terminado")){
         setBackground(Color.CYAN);
        setForeground(Color.black);
    }else{
        setBackground(Color.white);
        setForeground(Color.black);
    }
    return this;
        
    }
}



