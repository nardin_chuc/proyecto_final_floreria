/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelos;

import Interfaz.Notificaciones;

/**
 *
 * @author CCNAR
 */
public class HiloNotificaciones implements Runnable{
    Thread h1;
    Notificaciones nt = new Notificaciones();

    @Override
    public void run() {
        Thread ct = Thread.currentThread();
        if(ct == h1){
            try {
                nt.setVisible(true);
                Thread.sleep(5800);
                nt.dispose();
                
                
            } catch (Exception e) {
                System.out.println("error en abrir Notificaciones: "+e.getMessage());
            }
        }
        
    }
    
    
    public void EjecutarNotificaciones(){
        h1 = new Thread(this);
        h1.start();
    }
}
