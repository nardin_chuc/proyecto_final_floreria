/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.IngresarProveedor;
import Modelos.Tabla;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorIngresarProv implements ActionListener, MouseListener, KeyListener{
    
    IngresarProveedor pes;

    public ControladorIngresarProv(IngresarProveedor pes) {
        this.pes = pes;
        ActualizarId();
        this.pes.btn_Ingresar.addActionListener(this);
        this.pes.btn_Modificar.addActionListener(this);
        this.pes.btn_Nuevo.addActionListener(this);
        this.pes.tb_Permisionario.addMouseListener(this);
        this.pes.btn_Eliminar.addActionListener(this);
        this.pes.txt_Nombre.requestFocus(true);
        this.pes.cbx_tipoVenta.addActionListener(this);
        this.pes.tb_Permisionario.addMouseListener(this);
        this.pes.JmenuActivar.addActionListener(this);
        this.pes.JmenuDesactivar.addActionListener(this);
        this.pes.txt_Nombre.addKeyListener(this);
        this.pes.txt_id.addKeyListener(this);
        cargarTabla();
        Botones(false, true, false);
       
    }

        @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == pes.btn_Ingresar){ //////******* BTN INGRESAR
            if(CamposVacios() == true){
            }else{
                if(VericarCliente("El Proveedor se encuentra en la Base de Datos") == true){
                Ingresar();
                Limpiar();
                ActualizarId();
                Botones(false, false, true);
                }else{
                    
                }
            }
        }else if(e.getSource() == pes.btn_Modificar){//////////******* BTN MODIFICAR
            if(CamposVacios() == true){
                
            }else{
                Modificar();
                Limpiar();
                ActualizarId();
                Botones(false, false, true);
            }
        }else if(e.getSource() == pes.JmenuActivar){/////////******** BTN ELIMINAR
            if(CamposVacios() == true){
            }else{
                Desactivar("Activo");
                Limpiar();
                ActualizarId();
                Botones(false, false, true);
            }
        }else if(e.getSource() == pes.JmenuDesactivar){/////////******** BTN ELIMINAR
            if(CamposVacios() == true){
            }else{
                Desactivar("Inactivo");
                Limpiar();
                ActualizarId();
                Botones(false, false, true);
                
            }
        }else if(e.getSource() == pes.btn_Nuevo){
            pes.txt_Nombre.requestFocus(true);
            Limpiar();
            ActualizarId();
            Botones(false, true, false);
        }else if(e.getSource() == pes.btn_Eliminar){
            if(CamposVacios() == true){
            }else{
            Eliminar();
            Limpiar();
            ActualizarId();
            Botones(false, false, true);
            }
        }else if(e.getSource() == pes.cbx_tipoVenta){
            cargarTabla();
            Limpiar();
            ActualizarId();
            Botones(false, true, false);
        }

    }
    
    public boolean CamposVacios() {
        if (pes.txt_Nombre.getText().isEmpty() || pes.txt_id.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Digite los Campos vacios", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }
    
    public String verificacionTabla(){
        String tipoVenta = pes.cbx_tipoVenta.getSelectedItem().toString();
        String tablaSQL="";
        if(tipoVenta.equals("FLORERIA")){
            tablaSQL = "db_Floreria_Proveedor";
        }else if(tipoVenta.equals("PRODUCTOS DE LIMPIEZA")){
             tablaSQL = "db_ProductosLimpieza_Proveedor";
        }
        return tablaSQL;
    }
    
    
    
     public boolean VericarCliente(String textosino){
        String tabla = verificacionTabla();
        String cliente = pes.txt_Nombre.getText();
        String RFC = pes.txt_RFC.getText();
        String nombreSql="";
        String rfc = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Proveedor FROM "+tabla+" WHERE Proveedor = ?");
            ps.setString(1, cliente);
            rs = ps.executeQuery();
            while (rs.next()) { 
                     rfc = (rs.getString("Proveedor"));
                 }
            if (rfc.isEmpty()) {
                return true;
            }else{
                JOptionPane.showMessageDialog(null, textosino);
                return false;
            }
        
        } catch (Exception e) {
           System.out.println(" Error: Verificar "+e.getMessage());
        }
        return false;
    }
     
     
     public void Ingresar() {
        String tabla = verificacionTabla();
        int id = Integer.parseInt(pes.txt_id.getText());
        String Nombre = pes.txt_Nombre.getText();
        String rfc = pes.txt_RFC.getText();
        String Estado = "Activo";
        try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("INSERT INTO "+tabla+
         " (id_Proveedor, Proveedor, Telefono) VALUES (?,?,?)");
        ps.setInt(1, id);
        ps.setString(2, Nombre);
        ps.setString(3, rfc);
        ps.executeUpdate();
        cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Ingreso al Proveedor correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar "+e.getMessage());
        }
    }
     
     
     public void Modificar() {
        String tabla = verificacionTabla();
        int id = Integer.parseInt(pes.txt_id.getText());
        String Nombre = pes.txt_Nombre.getText();
        String rfc = pes.txt_RFC.getText();
       try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("UPDATE "+tabla+" SET Proveedor = ?, Telefono = ? WHERE id_Proveedor = ?");
        ps.setString(1, Nombre);
        ps.setString(2, rfc);
        ps.setInt(3, id);
        ps.executeUpdate();
        cargarTabla();
        JOptionPane.showMessageDialog(null, "Se Modifico al Proveedor correctamente");
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
        }
    }
     
     
     public void Eliminar(){
         String tabla = verificacionTabla();
         int id = Integer.parseInt(pes.txt_id.getText());
       try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("DELETE "+tabla+" WHERE id_Proveedor = ?");
        ps.setInt(1, id);
        ps.executeUpdate();
        cargarTabla();
        JOptionPane.showMessageDialog(null, "Se Elimino al Proveedor correctamente");
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
        }
     }
     
     
     
     public void Desactivar(String Estado) {
         String tabla = verificacionTabla();
        int id = Integer.parseInt(pes.txt_id.getText());
            try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("UPDATE "+tabla+" SET Estado = ? WHERE id_Proveedor = ?");
        ps.setString(1, Estado);
        ps.setInt(2, id);
        ps.executeUpdate();
        cargarTabla();
        } catch (Exception e) {
            System.out.println("error: "+e.getMessage());
        }
    }
     
     
     public void cargarTabla() {
         String nombreProv= pes.txt_Nombre.getText();
         String tabla = verificacionTabla();
         Tabla tableColor = new Tabla();
         int id = 0;
        if(pes.txt_id.getText().isEmpty()){
             id = 0;
        }else{
            id = Integer.parseInt(pes.txt_id.getText());
        }
         pes.tb_Permisionario.setDefaultRenderer(pes.tb_Permisionario.getColumnClass(0), tableColor);
       DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sqlProv = "SELECT * FROM "+tabla+" WHERE Proveedor LIKE '%"+nombreProv+"%'";
            String sql = "SELECT * FROM "+tabla+" ORDER BY id_Proveedor ASC, Estado ASC";
            String sqlId = "SELECT * FROM "+tabla+" WHERE id_Proveedor = "+id;
            if (nombreProv.isEmpty() && id == 0) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!(nombreProv.isEmpty())) {
                ps = con.prepareStatement(sqlProv);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!(id == 0)) {
                ps = con.prepareStatement(sqlId);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
           
        } catch (Exception e) {
            System.err.println("Error en CARGARtabla: " + e.toString());
        }
    }
     
     
     
     public void ActualizarId(){
         String tabla = verificacionTabla();
           int idAux = 0;
        try {
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT MAX(id_Proveedor) AS id_Maximo FROM "+tabla);
                 rs = ps.executeQuery();
                 while (rs.next()) {
                     idAux = (rs.getInt("id_Maximo")+1);                 
                 }
                 pes.txt_id.setText(""+idAux);
           } catch (Exception e) {
               System.err.println("error ActFolio: "+e.getMessage());
           }
       }
     
     
     public void Limpiar() {
        pes.txt_id.setText("");
        pes.txt_Nombre.setText("");
        pes.txt_RFC.setText("");
        pes.txt_estado.setText("");
    }
     
     
     public void Botones(boolean ver, boolean ver2, boolean ver1){
        pes.btn_Eliminar.setVisible(ver);
        pes.btn_Modificar.setVisible(ver);
        pes.btn_Nuevo.setVisible(ver1);
        pes.btn_Ingresar.setEnabled(ver2);
        pes.txt_Nombre.requestFocus(true);
    }
     
     
     
     //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            String tabla = verificacionTabla();
        try {
                int fila = pes.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(pes.tb_Permisionario.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id_Proveedor, Proveedor, Telefono, Estado FROM "+tabla+" WHERE id_Proveedor = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pes.txt_id.setText(""+rs.getInt("id_Proveedor"));
                    pes.txt_Nombre.setText(rs.getString("Proveedor"));
                    pes.txt_RFC.setText(rs.getString("Telefono"));
                    pes.txt_estado.setText(rs.getString("Estado"));  
                   
                }
                Botones(true, false, true);
                if (pes.txt_estado.getText().equals("Inactivo")) {
                    pes.JmenuActivar.setVisible(true);
                    pes.JmenuDesactivar.setVisible(false);
                } else if (pes.txt_estado.getText().equals("Activo")) {
                    pes.JmenuActivar.setVisible(false);
                    pes.JmenuDesactivar.setVisible(true);
                }              
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            pes.lb_selc.setVisible(true);
            pes.lb_selc.setText("Haga Click en una fila para Modificar");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            pes.lb_selc.setVisible(false);
        }
    }
    
        public void keyTyped(KeyEvent e) {

    }

    //*****EVENTO DEL TECLADO AL PONERSE EN EL TXT_ID, PARA PODER REALIZAR CIERTAS ACCIONES
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            
        }
    }

    //********** REALIZA UNA COSULTA EN LA BASE DE DATOS PARA LOCALIZAR LOS DATOS DEL PERMISIONARIO
    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == pes.txt_Nombre){
            cargarTabla();
        }else if(e.getSource() == pes.txt_id){
            cargarTabla();
        }

    }
    
}
