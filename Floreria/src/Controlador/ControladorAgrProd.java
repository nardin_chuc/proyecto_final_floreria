/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.AgregarProducto;
import Interfaz.Bienvenida;
import Interfaz.IngresarProveedor;
import Modelos.Tabla;
import Modelos.TbProducto;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorAgrProd implements ActionListener, MouseListener, KeyListener {

    AgregarProducto agr;

    public ControladorAgrProd(AgregarProducto agr) {
        this.agr = agr;
        this.agr.btn_Eliminar.addActionListener(this);
        this.agr.btn_IngresarPro.addActionListener(this);
        this.agr.btn_modificar.addActionListener(this);
        this.agr.tb_Productos.addMouseListener(this);
        this.agr.btn_nuevoCli.addActionListener(this);
        this.agr.cbx_Proveedor.addActionListener(this);
        this.agr.btn_nuevo.addActionListener(this);
        this.agr.btn_Eliminar.addMouseListener(this);
        this.agr.btn_IngresarPro.addMouseListener(this);
        this.agr.btn_modificar.addMouseListener(this);
        this.agr.btn_nuevoCli.addMouseListener(this);
        this.agr.cbx_Proveedor.addMouseListener(this);
        this.agr.cbx_tipoVenta.addActionListener(this);
        this.agr.JmenuActivar.addActionListener(this);
        this.agr.JmenuDesactivar.addActionListener(this);
        this.agr.txt_Producto.addKeyListener(this);
        this.agr.txt_id.addKeyListener(this);
        cargarTabla();
        ActualizarId();
        LLenarCBX();
        Botones(false, true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == agr.btn_IngresarPro) {

            if (camposVacios() == true) {
                agr.txt_Producto.requestFocus(true);
            } else {
                String producto = agr.txt_Producto.getText();
                if (verificarProducto(producto) == true) {
                    JOptionPane.showMessageDialog(null, "El Producto se encuentra en la Base de Datos");
                    agr.txt_Producto.requestFocus(true);
                } else {
                    Ingresar();
                    Limpiar();
                    ActualizarId();
                }
            }
        } else if (e.getSource() == agr.cbx_tipoVenta) {
            Limpiar();
            ActualizarId();
            cargarTabla();
            LLenarCBX();
        } else if (e.getSource() == agr.btn_modificar) {
            if (camposVacios() == true) {
                agr.txt_Producto.requestFocus(true);
            } else {
                String producto = agr.txt_Producto.getText();
                if (verificarProducto(producto) == true) {
                    JOptionPane.showMessageDialog(null, "El Producto se encuentra en la Base de Datos");
                } else {
                    Modificar();
                }
            }

        } else if (e.getSource() == agr.btn_Eliminar) {
            if (camposVacios() == true) {
                agr.tb_Productos.requestFocus(true);
            } else {
                EliminarRegistro();
            }

        } else if (e.getSource() == agr.btn_nuevoCli) {
            IngresarProveedor prov = new IngresarProveedor();
            prov.setVisible(true);
            prov.cbx_tipoVenta.setSelectedItem(agr.cbx_tipoVenta.getSelectedItem().toString());
        } else if (e.getSource() == agr.btn_nuevo) {
            Limpiar();
            Botones(false, true);
            ActualizarId();
        } else if (e.getSource() == agr.JmenuDesactivar) {
            if (camposVacios()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                Desactivar("Inactivo");
                cargarTabla();
                Botones(true, false);
                Limpiar();
                ActualizarId();
            }
        } else if (e.getSource() == agr.JmenuActivar) {
            if (camposVacios()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                Desactivar("Activo");
                cargarTabla();
                Botones(true, false);
                Limpiar();
                ActualizarId();
            }
        }

    }

    public boolean camposVacios() {
        if (agr.txt_id.getText().isEmpty() || agr.txt_Producto.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos Vacios ", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    public boolean verificarProducto(String Producto) {
        try {
            String prodAux = "";
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto FROM db_Productos WHERE Producto = ?");
            ps.setString(1, Producto);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodAux = (rs.getString("Producto"));
            }
            if (prodAux.isEmpty()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            System.out.println("error Verificar prod " + e.getMessage());
        }
        return false;
    }

    public String verificacionTabla() {
        String tipoVenta = agr.cbx_tipoVenta.getSelectedItem().toString();
        String tablaSQL = "";
        if (tipoVenta.equals("FLORERIA")) {
            tablaSQL = "db_Floreria_Productos";
        } else if (tipoVenta.equals("PRODUCTOS DE LIMPIEZA")) {
            tablaSQL = "db_ProductosLimpieza";
        }
        return tablaSQL;
    }

    public void Ingresar() {
        String validarTabla = verificacionTabla();
        int id = Integer.parseInt(agr.txt_id.getText());
        String Producto = agr.txt_Producto.getText();
        float precio_compra = Float.parseFloat(agr.txt_precioCompra.getText());
        float precio_venta = Float.parseFloat(agr.txt_precioVenta.getText());
        String ubicacion = agr.txt_ubicacion.getText();
        String Proveedor = agr.cbx_Proveedor.getSelectedItem().toString();
        int id_Proveedor = agr.cbx_Proveedor.getSelectedIndex() + 1;
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO " + validarTabla
                    + "(id, Producto, Precio_Compra, Precio_Venta, id_Proveedor, Proveedor, Ubicacion) VALUES (?,?,?,?,?,?,?)");
            ps.setInt(1, id);
            ps.setString(2, Producto);
            ps.setFloat(3, precio_compra);
            ps.setFloat(4, precio_venta);
            ps.setInt(5, id_Proveedor);
            ps.setString(6, Proveedor);
            ps.setString(7, ubicacion);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Ingreso al Producto correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void Modificar() {
        String tabla = verificacionTabla();
        int id = Integer.parseInt(agr.txt_id.getText());
        String Producto = agr.txt_Producto.getText();
        float precio_compra = Float.parseFloat(agr.txt_precioCompra.getText());
        float precio_venta = Float.parseFloat(agr.txt_precioVenta.getText());
        String ubicacion = agr.txt_ubicacion.getText();
        String Proveedor = agr.cbx_Proveedor.getSelectedItem().toString();
        int id_Proveedor = agr.cbx_Proveedor.getSelectedIndex() + 1;
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + tabla + " SET"
                    + " Producto = ?, Precio_Compra = ?, Precio_Venta = ?, id_Proveedor = ?, Proveedor = ?, Ubicacion = ? WHERE id = ?");
            ps.setString(1, Producto);
            ps.setFloat(2, precio_compra);
            ps.setFloat(3, precio_venta);
            ps.setInt(4, id_Proveedor);
            ps.setString(5, Proveedor);
            ps.setString(6, ubicacion);
            ps.setInt(7, id);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Ingreso al Producto correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void Desactivar(String Estado) {
        String tabla = verificacionTabla();
        int id = Integer.parseInt(agr.txt_id.getText());
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + tabla + " SET Estado = ? WHERE id = ?");
            ps.setString(1, Estado);
            ps.setInt(2, id);
            ps.executeUpdate();
            cargarTabla();
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }
    }

    public void EliminarRegistro() {
        String tabla = verificacionTabla();
        int id = Integer.parseInt(agr.txt_id.getText());
        String Proveedor = agr.cbx_Proveedor.getSelectedItem().toString();
        String Producto = agr.txt_Producto.getText();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("DELETE " + tabla + " WHERE id = ? AND Proveedor = ? AND Producto = ?");
            ps.setInt(1, id);
            ps.setString(2, Proveedor);
            ps.setString(3, Producto);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Elimino el registro seleccionado correctamente");
        } catch (Exception e) {
            //System.out.println("error: "+e.getMessage());
        }
    }

    public void ActualizarId() {
        String tabla = verificacionTabla();
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM " + tabla);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            agr.txt_id.setText("" + idAux);
        } catch (Exception e) {
        }
    }

    public void Botones(boolean ver, boolean ver2) {
        agr.btn_Eliminar.setVisible(ver);
        agr.btn_modificar.setVisible(ver);
        agr.btn_AbrirAgrProduc.setVisible(ver);
        agr.btn_Eliminar.setEnabled(ver);
        agr.btn_modificar.setEnabled(ver);
        agr.btn_IngresarPro.setEnabled(ver2);
        agr.btn_nuevo.setVisible(ver);

    }

    public void LLenarCBX() {
        String tabla = verificacionTabla();
        String TbProveedor = "";
        if (tabla.equals("db_Floreria_Productos")) {
            TbProveedor = "db_Floreria_Proveedor";
        } else if (tabla.equals("db_ProductosLimpieza")) {
            TbProveedor = "db_ProductosLimpieza_Proveedor";
        }
        try {
            String Estado = "Activo";
            agr.cbx_Proveedor.removeAllItems();
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Proveedor FROM " + TbProveedor + " WHERE Estado = ?");
            ps.setString(1, Estado);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                String Proveedor = rs.getString("Proveedor");
                agr.cbx_Proveedor.addItem(Proveedor);
                i++;
            }

        } catch (Exception er) {
            System.err.println("Error en cbx Prov: " + er.toString());
        }

    }

    public void cargarTabla() {
        String nombreProv= agr.txt_Producto.getText();
        String validarTabla = verificacionTabla();
        int id = 0;
        if(agr.txt_id.getText().isEmpty()){
             id = 0;
        }else{
            id = Integer.parseInt(agr.txt_id.getText());
        }
        TbProducto tableColor = new TbProducto();
        agr.tb_Productos.setDefaultRenderer(agr.tb_Productos.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) agr.tb_Productos.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 90, 150, 150, 150, 100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            agr.tb_Productos.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id, Producto, Cantidad, Precio_Venta, Proveedor, Ubicacion, Estado FROM " + validarTabla + " ORDER BY id ASC, Estado ASC";
            String sqlNom = "SELECT id, Producto, Cantidad, Precio_Venta, Proveedor, Ubicacion, Estado FROM " + validarTabla + " WHERE Producto LIKE '%"+nombreProv+"%' "
                    + "OR id LIKE '%"+id+"%'";
            String sqlId = "SELECT id, Producto, Cantidad, Precio_Venta, Proveedor, Ubicacion, Estado FROM " + validarTabla + " WHERE id = "+id;
           if (nombreProv.isEmpty() && id == 0) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!(nombreProv.isEmpty())) {
                ps = con.prepareStatement(sqlNom);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!(id == 0)) {
                ps = con.prepareStatement(sqlId);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
        } catch (Exception e) {
            System.err.println("Error en tabla prod: " + e.toString());
        }
    }
    //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == agr.tb_Productos) {
            String tabla = verificacionTabla();
            try {
                int fila = agr.tb_Productos.getSelectedRow();
                int id = Integer.parseInt(agr.tb_Productos.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Producto, Precio_Compra, Precio_Venta, id_Proveedor, Proveedor, Ubicacion, Estado FROM " + tabla + " WHERE id = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    agr.txt_id.setText("" + rs.getInt("id"));
                    agr.txt_Producto.setText(rs.getString("Producto"));
                    agr.txt_precioCompra.setText("" + rs.getFloat("Precio_Compra"));
                    agr.txt_precioVenta.setText("" + rs.getFloat("Precio_Venta"));
                    agr.cbx_Proveedor.setSelectedItem(rs.getString("Proveedor"));
                    agr.txt_ubicacion.setText(rs.getString("Ubicacion"));
                    agr.txt_estado.setText(rs.getString("Estado"));

                }
                if (agr.txt_estado.getText().equals("Inactivo")) {
                    agr.JmenuActivar.setVisible(true);
                    agr.JmenuDesactivar.setVisible(false);
                } else if (agr.txt_estado.getText().equals("Activo")) {
                    agr.JmenuActivar.setVisible(false);
                    agr.JmenuDesactivar.setVisible(true);
                }
                Botones(true, false);
                agr.lb_selc.setVisible(false);
            } catch (Exception er) {
                System.err.println("Error en tabla prod mouse: " + er.toString());
            }
        } else if (e.getSource() == agr.cbx_Proveedor) {
            LLenarCBX();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_nuevoCli) {
            agr.btn_nuevoCli.setBackground(Color.black);
        } else if (e.getSource() == agr.cbx_Proveedor) {
            LLenarCBX();
        } else if (e.getSource() == agr.tb_Productos) {
            agr.lb_selc.setVisible(true);
            agr.lb_selc.setText("Haga Click en una fila para Modificar");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_nuevoCli) {
            agr.btn_nuevoCli.setBackground(Color.red);
        } else if (e.getSource() == agr.tb_Productos) {
            agr.lb_selc.setVisible(false);
        }

    }

    public void Limpiar() {
        agr.txt_Producto.setText("");
        agr.txt_precioCompra.setText("");
        agr.txt_precioVenta.setText("");
        agr.txt_ubicacion.setText("");
        agr.txt_Producto.requestFocus(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == agr.txt_Producto){
            cargarTabla();
        }else if(e.getSource() == agr.txt_id){
            cargarTabla();
        }
   }

}
