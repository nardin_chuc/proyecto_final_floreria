/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.Compras;
import Interfaz.Perecederos;
import Interfaz.Ventas;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorPerecederos implements MouseListener {

    Perecederos pe;
    Compras cr;
    Ventas vn;
    int id[], cantidad[], idPerecederoVenta[];
    String FechaEvento[];
    int idMinimo;
    int idVentasPerecederos[];  //sirve para la obtencion de id en la resta de ventas
    int contadorId = 0;
    int cantTotal = 0;
    int canti = 0;
    String idPerecedero = "", cantPere = "";

    public ControladorPerecederos(Perecederos pe) {
        this.pe = pe;
        DiasAutomaticos(pe.txt_fecha.getText());
        QuitarPercederoProducto();
        cargarCompras();
        this.pe.tb_Perecederos.addMouseListener(this);
    }

    public ControladorPerecederos(Compras cr) {
        this.cr = cr;
    }

    public ControladorPerecederos(Ventas vn) {
        this.vn = vn;
    }

    public ControladorPerecederos() {

    }

    public void cargarCompras() {
        DefaultTableModel modeloTabla = (DefaultTableModel) pe.tb_Perecederos.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int id = 0;

        int columnas;
        int[] ancho = {100, 180, 90, 150, 150, 150,150,150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pe.tb_Perecederos.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_perecedero, Producto, Cantidad, FechaRegistro, FechaVencimiento, DiasPendientes, Estado, StockVencido FROM db_Perecederos WHERE Estado != 'Agotado'";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla CargarComprasprod: " + e.toString());
        }
    }

    public int DiasRestantes(String fechaevento, String FechaAhora) {           //CALCULA LOS DIAS QUE FALTAN PARA EL EVENTO
        String fecha_hoy = FechaAhora;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        int diasf = 0;
        try {
            Date date1 = dateFormat.parse(fecha_hoy);
            Date date2 = dateFormat.parse(fechaevento);
            long tiempoTrans = date2.getTime() - date1.getTime();
            TimeUnit unidad = TimeUnit.DAYS;
            TimeUnit unidad2 = TimeUnit.DAYS;
            long tiempoTrans2 = (date2.getTime() - date1.getTime());
            long diasFaltantes = unidad.convert(tiempoTrans, TimeUnit.MILLISECONDS);
            diasf = Integer.parseInt("" + diasFaltantes);
            System.out.println("dias "+diasf);
        } catch (Exception e) {
            System.out.println("erro en calcular dias: " + e.getMessage());
        }
        return diasf;
    }

    public int buscarTamañoArray() {
        int tamaño = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT COUNT(Producto) AS tamaño FROM db_Perecederos WHERE Estado = 'Util' OR Estado = 'Por Vencer'");
            rs = ps.executeQuery();
            while (rs.next()) {
                tamaño = (rs.getInt("tamaño"));
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error buscarTamaño Array: " + e.getMessage());
        }
        return tamaño;
    }

    public void AsinarId_fecha() {
        int tamaño = buscarTamañoArray();
        id = new int[tamaño];
        FechaEvento = new String[tamaño];
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT id_perecedero, FechaVencimiento FROM db_Perecederos WHERE Estado = 'Util' OR Estado = 'Por Vencer' ORDER BY id_perecedero ASC");
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                id[i] = rs.getInt("id_perecedero");
                FechaEvento[i] = rs.getString("FechaVencimiento");
                i++;
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error AsinarId_fecha: " + e.getMessage());
        }
    }

    public void DiasAutomaticos(String FechaAhora) {
        AsinarId_fecha();
        String fecha = "";
        String Estado = "";
        int dias = 0;
        try {
            for (int i = 0; i < FechaEvento.length; i++) {
                fecha = FechaEvento[i];
                if (DiasRestantes(fecha, FechaAhora) < 0) {
                    dias = 0;
                } else if (DiasRestantes(fecha, FechaAhora) >= 0) {
                    dias = DiasRestantes(fecha, FechaAhora);
                }
                Estado = EstadoDias(fecha, FechaAhora);
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("UPDATE db_Perecederos SET DiasPendientes = ?, Estado = ? WHERE id_perecedero = " + id[i]);
                ps.setInt(1, dias);
                ps.setString(2, Estado);
                ps.executeUpdate();
                ps.close();
            }

        } catch (Exception e) {
            System.err.println("error DiasAutomaticos: " + e.getMessage());
        }

    }

    public String EstadoDias(String fecha, String FechaAhora) {
        String texto = "";

        int dias = DiasRestantes(fecha, FechaAhora);
        if (dias < 0) {
            texto = "Agotado";
        } else if (dias == 0) {
            texto = "Vencido";
        } else if (dias > 0 && dias <= 3) {
            texto = "Por Vencer";
        } else if (dias > 3 && dias <= 10) {
            texto = "Util";
        }
        return texto;
    }

    public void ModificarPerecedero() {
        int Cantidad = Integer.parseInt(cr.txt_cantidad.getText());
        int id = Integer.parseInt(cr.txt_idPere.getText());
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE db_Perecederos SET Cantidad = ? WHERE id_perecedero = ?");
            ps.setInt(1, Cantidad);
            ps.setInt(2, id);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void EliminarPerecedero() {
        try {
            String nombre = cr.txt_Producto.getText();
            int id = Integer.parseInt(cr.txt_idPere.getText());
            if (cr.cbx_tipoVenta.getSelectedItem().toString().equals("FLORERIA")) {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("DELETE db_Perecederos WHERE id_perecedero = ? AND Producto = ?");
                ps.setInt(1, id);
                ps.setString(2, nombre);
                ps.executeUpdate();
                ps.close();
            } else {

            }
        } catch (Exception e) {
            System.out.println("error en Eliminar Perecedero: " + e.getMessage());
        }
    }

    /////////// ESTA PARTE ES PARA LA BUSQUEDA DE PRODUCTOS PRECIONANDO EL BOTON DE GENERAR VENTA EN VENTAS PARA MODIFICAR 
    /// EN EL FRAME DE PERECEDEROS.
    public int buscarTamañoProductoPerecedero() {
        String nombre = vn.txt_Producto.getText();
        int id = 0;
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT COUNT(Producto) AS Tamaño, "
                    + "SUM(Cantidad) AS Cantidad FROM db_Perecederos WHERE Producto = ? AND Estado = 'Util' OR Estado = 'Por Vencer'");
            ps.setString(1, nombre);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt("Tamaño");
                cantTotal = rs.getInt("Cantidad");
            }
        } catch (Exception e) {
            System.out.println("error en buscarTamañoProductoPerecedero: " + e.getMessage());
        }
        return id;
    }

    public void meterIdArray() {
        int tamaño = buscarTamañoProductoPerecedero();
        idVentasPerecederos = new int[tamaño];
        String nombre = vn.txt_Producto.getText();
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT id_perecedero FROM db_Perecederos WHERE Producto = ? AND Estado = 'Util' OR Estado = 'Por Vencer' ORDER BY id_perecedero ASC");
            ps.setString(1, nombre);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                idVentasPerecederos[i] = rs.getInt("id_perecedero");
                i++;
            }
        } catch (Exception e) {
            System.out.println("error en meterIdArray: " + e.getMessage());
        }
    }

    public void CalcularCantidad(String signo) {
        meterIdArray();
        int idVenta = Integer.parseInt(vn.txt_idVenta.getText());
        int cant = 0;
        int id = 0;
        int cantidad = Integer.parseInt(vn.txt_cantidad.getText());
        try {
            int num = idVentasPerecederos[id];
            cant = buscarCantidad(num);
            canti = canti + cant;
            if (idVentasPerecederos.length > 1) {
                if (cant == 0) {
                } else if (cantidad == canti) {
                    idPerecedero = idPerecedero + "" + idVentasPerecederos[id] + ",";
                    cantPere = cantPere + "" + cantidad + ",";
                    StockPerecederos(cant, cant, id, signo, idVentasPerecederos);
                } else if (cantidad > canti) {
                    idPerecedero = idPerecedero + "" + idVentasPerecederos[id] + ",";
                    cantPere = cantPere + "" + cant + ",";
                    StockPerecederos(cant, cant, id, signo, idVentasPerecederos);
                    CalcularCantidad(signo);
                } else if (cantidad < cant) {
                    idPerecedero = idPerecedero + "" + idVentasPerecederos[id] + ",";
                    int total = canti - cantidad;
                    int resul = cant - total;
                    cantPere = cantPere + "" + cantidad + ",";
                    StockPerecederos(cant, resul, id, signo, idVentasPerecederos);
                    canti = 0;
                }
            } else if (idVentasPerecederos.length == 1) {
                int total = canti - cantidad;
                if (cantidad == canti || cantidad == cant) {
                    idPerecedero = idPerecedero + "" + idVentasPerecederos[id] + ",";
                    cantPere = cantPere + "" + cantidad + ",";
                    StockPerecederos(cant, cant, id, signo, idVentasPerecederos);
                    canti = 0;
                } else if (total < cant) {
                    idPerecedero = idPerecedero + "" + idVentasPerecederos[id] + ",";
                    int resul = cant - total;
                    cantPere = cantPere + "" + resul + ",";
                    StockPerecederos(cant, resul, id, signo, idVentasPerecederos);
                    canti = 0;
                }
            }
            ModificarIdperecederoVentas(idPerecedero, cantPere, idVenta);
        } catch (Exception e) {
            System.out.println("error en CalcularCantidad: " + e.getMessage());
        }
    }

    public void StockPerecederos(int cantidadSDtock, int cantResta, int index, String signo, int idPerecedero[]) {
        String Estado = "";
        int resultado = 0;
        if (signo.equalsIgnoreCase("-")) {
            resultado = cantidadSDtock - cantResta;

            if (resultado == 0 || resultado < 0) {
                Estado = "Agotado";
            } else {
                Estado = "Util";
            }
        } else if (signo.equalsIgnoreCase("+")) {
            resultado = cantidadSDtock + cantResta;

            if (resultado == 0 || resultado < 0) {
                Estado = "Agotado";
            } else if (resultado > 0) {
                Estado = "Util";
            }
        }

        if (vn.cbx_tipoVenta.getSelectedItem().toString().equals("FLORERIA")) {
            try {
                Connection conectar = Conexion.establecerConnection();
                ResultSet rs;
                PreparedStatement ps = conectar.prepareStatement("UPDATE db_Perecederos SET Cantidad = Cantidad " + signo + " ?, Estado = ? WHERE id_perecedero = ?");
                ps.setInt(1, cantResta);
                ps.setString(2, Estado);
                ps.setInt(3, idPerecedero[index]);
                ps.executeUpdate();
                ps.close();
            } catch (Exception e) {
                System.out.println("error en restarStockPerecederos: " + e.getMessage());
            }

        } else {
            //NO REALIZA NADA
        }
    }

    public void SepararCadena() {
        int idVenta = Integer.parseInt(vn.txt_idVenta.getText());
        int cantidadperecedero = 0;
        int cantidadVentas = Integer.parseInt(vn.txt_cantidad.getText());
        int cant = 0;
        int id = 0, pereVenta = 0;
        String cadena = "", idPere = "", cantQuitar = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT IdPerecedero, cantQuitar FROM db_Ventas WHERE id_ventas = ?");
            ps.setInt(1, idVenta);
            rs = ps.executeQuery();
            while (rs.next()) {
                idPere = rs.getString("IdPerecedero");
                cantQuitar = rs.getString("cantQuitar");
            }
            ps.close();
            String idPereVenta[] = String.valueOf(idPere).split(",");
            String CantidadPere[] = String.valueOf(cantQuitar).split(",");
            TransformarArray(CantidadPere, idPereVenta);
            for (int i = (CantidadPere.length - 1); i >= 0; i--) {
                id = Integer.parseInt(idPereVenta[i]);
                cantidadperecedero = Integer.parseInt(CantidadPere[i]);
                if (cantidadVentas < cantidadperecedero) {
                    cant = buscarCantidad(id);
//                    idPerecedero = idPerecedero + ""+idPerecederoVenta[pereVenta]+",";
                    cantPere = cantPere + "" + cantidad + ",";
                    StockPerecederos(cant, cantidadperecedero, i, "+", idPerecederoVenta);
                    break;
                } else if (cantidadVentas > cantidadperecedero) {
                    cant = buscarCantidad(id);
                    StockPerecederos(cant, cantidadperecedero, i, "+", idPerecederoVenta);
                } else if (cantidadVentas == cantidadperecedero) {
                    break;
                }
                pereVenta++;
            }

        } catch (Exception e) {
            System.out.println("error en SepararCadenaPerecederos: " + e.getMessage());
        }
    }

    public int buscarCantidad(int id) {
        int cant = 0;
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Cantidad FROM db_Perecederos WHERE id_perecedero = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                cant = rs.getInt("Cantidad");
            }
            ps.close();
        } catch (Exception e) {
            System.out.println("buscarCantidad :" + e.getMessage());
        }
        return cant;
    }

    public void TransformarArray(String array[], String id[]) {
        cantidad = new int[array.length];
        idPerecederoVenta = new int[id.length];
        try {
            for (int i = 0; i < array.length; i++) {
                cantidad[i] = Integer.parseInt(array[i]);
                idPerecederoVenta[i] = Integer.parseInt(id[i]);
            }
        } catch (Exception e) {
            System.out.println("error en TransformarArray :" + e.getMessage());
        }
    }

    public void ModificarIdperecederoVentas(String idPerecederoCant, String cant, int id_Venta) {
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("UPDATE db_Ventas SET IdPerecedero = ?, cantQuitar = ? WHERE id_ventas = ?");
            ps.setString(1, idPerecederoCant);
            ps.setString(2, cant);
            ps.setInt(3, id_Venta);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            System.out.println("error en ModificarPerecederos: " + e.getMessage());
        }
    }

    public void QuitarPercederoProducto() {
        int tamaño = buscarTamañoPerecederoVencido();
        int idPerecederoVencido[] = new int[tamaño];
        String fecha = "", FechaHoy = pe.txt_fecha.getText();
        int valor = 0;
        String NombrePerecederoVencido[] = new String[tamaño];
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT id_perecedero, Producto, StockVencido, FechaRegistro FROM db_Perecederos WHERE Estado = 'Vencido'");
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                idPerecederoVencido[i] = rs.getInt("id_perecedero");
                NombrePerecederoVencido[i] = rs.getString("Producto");
                valor = rs.getInt("StockVencido");
                fecha = rs.getString("FechaRegistro");
                i++;
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("error en Meter id Perecedero: " + e.getMessage());
        }
        String estado = EstadoDias(fecha, FechaHoy);
        if (valor == 0) {
            for (int i = 0; i < tamaño; i++) {
                int CantidadPere = buscarCantidad(idPerecederoVencido[i]);
                try {
                    Connection conectar = Conexion.establecerConnection();
                    ResultSet rs;
                    PreparedStatement ps = conectar.prepareStatement("UPDATE db_Floreria_Productos set Cantidad = Cantidad - ? WHERE Producto = ?");
                    ps.setInt(1, CantidadPere);
                    ps.setString(2, NombrePerecederoVencido[i]);
                    ps.executeUpdate();
                    ps.close();
                } catch (Exception e) {
                    System.out.println("error en Cambiar estado Producto: " + e.getMessage());
                }
                try {
                    Connection conectar = Conexion.establecerConnection();
                    ResultSet rs;
                    PreparedStatement ps = conectar.prepareStatement("UPDATE db_Perecederos set Cantidad = 0, StockVencido = ?, Estado = ? WHERE Estado = 'Vencido'");
                    ps.setInt(1, CantidadPere);
                    ps.setString(2, estado);
                    ps.executeUpdate();
                    ps.close();
                } catch (Exception e) {
                    System.out.println("error en Cambiar estado Pere: " + e.getMessage());
                }
            }
        }else if(valor > 0 || estado.equals("Agotado")){
            for (int i = 0; i < tamaño; i++) {
                int CantidadPere = buscarCantidad(idPerecederoVencido[i]);
            try {
                    Connection conectar = Conexion.establecerConnection();
                    ResultSet rs;
                    PreparedStatement ps = conectar.prepareStatement("UPDATE db_Perecederos set  Estado = ? WHERE Estado = 'Vencido'");
                    ps.setString(1, estado);
                    ps.executeUpdate();
                    ps.close();
                } catch (Exception e) {
                    System.out.println("error en Cambiar estado Pere: " + e.getMessage());
                }
            }
        }

    }

    public int buscarTamañoPerecederoVencido() {
        int id = 0;
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT COUNT(Producto) AS Tamaño FROM db_Perecederos WHERE Estado = 'Vencido'");
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt("Tamaño");
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("error en buscarTamañoProductoPerecedero: " + e.getMessage());
        }
        return id;
    }

    public void buscarProductoStock() {
        String nombre = pe.lb_id.getText();
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Cantidad FROM db_Floreria_Productos WHERE Producto = ?");
            ps.setString(1, nombre);
            rs = ps.executeQuery();
            while (rs.next()) {
                pe.Lb_Stock.setText("" + rs.getInt("Cantidad"));
            }
            ps.close();
        } catch (Exception e) {
            System.out.println("error en Cambiar estado Perecedero: " + e.getMessage());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pe.tb_Perecederos) {
            try {
                String nombre = "", idPere = "";
                int fila = pe.tb_Perecederos.getSelectedRow();
                int id = Integer.parseInt(pe.tb_Perecederos.getValueAt(fila, 0).toString());
                String Pro = pe.tb_Perecederos.getValueAt(fila, 1).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id_perecedero, Producto FROM db_Perecederos WHERE id_perecedero = ? AND Producto = ?");
                ps.setInt(1, id);
                ps.setString(2, Pro);
                rs = ps.executeQuery();
                while (rs.next()) {
                    nombre = (rs.getString("Producto"));
                    idPere = rs.getString("id_perecedero");
                    
                }
                rs.close();
                pe.lb_idProducto.setText(idPere);
                pe.lb_id.setText(nombre);
                buscarProductoStock();
            } catch (Exception er) {
                System.err.println("Error en tabla prod mouse: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
