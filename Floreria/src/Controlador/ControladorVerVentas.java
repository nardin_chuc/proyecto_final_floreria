/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.AgregarProducto;
import Interfaz.VerVentas;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorVerVentas implements ActionListener, KeyListener, MouseListener{
    
     VerVentas vr;
    String Valor, dato;

    public ControladorVerVentas(VerVentas vr) {
        this.vr = vr;
        this.vr.ckB_Hoy.addActionListener(this);
        this.vr.txt_idCompra.addKeyListener(this);
        this.vr.txt_fecha.addKeyListener(this);
        this.vr.txt_Mes.addKeyListener(this);
        this.vr.txt_usuario.addKeyListener(this);
        this.vr.txt_AÑO.addKeyListener(this);
        this.vr.tb_compra.addMouseListener(this);
        this.vr.btn_imprimir.addActionListener(this);
        this.vr.btn_imprimir.addMouseListener(this);
        String año =  vr.txt_AÑO.getText();
        String fecha = vr.lb_Fecha.getText();
        vr.btn_imprimir.setVisible(false);
        cargarTabla(fecha, "",año);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vr.ckB_Hoy){
            String año = vr.txt_AÑO.getText();
            if(vr.ckB_Hoy.isSelected()) {
                String fecha = vr.lb_Fecha.getText();
                cargarTabla(fecha, "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
            } else {
                cargarTabla("", "", año);
                vr.txt_idCompra.requestFocus(true);
            }
        }else if(e.getSource() == vr.btn_imprimir){
            abrirDocumento(vr.lb_año.getText(), vr.lb_nota.getText());
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        String año;
        if(e.getSource() == vr.txt_idCompra){
          String id = vr.txt_idCompra.getText();
          año = vr.txt_AÑO.getText();
          cargarTabla("",id,año);
          vr.ckB_Hoy.setSelected(false);
        }else if(e.getSource() == vr.txt_fecha){
          Valor = vr.txt_fecha.getText();
            año = vr.txt_AÑO.getText();
            cargarTabla(Valor, "",año);
        }else if(e.getSource() == vr.txt_Mes){
            String mes = vr.txt_Mes.getText();
           año = vr.txt_AÑO.getText();
          cargarTabla(mes, "", año);
        }else if(e.getSource() == vr.txt_usuario){
          Valor = vr.txt_usuario.getText();
            año = vr.txt_AÑO.getText();
            cargarTabla(Valor, "",año);
        }else if(e.getSource() == vr.txt_AÑO){
          año = vr.txt_AÑO.getText();
            String id = vr.txt_idCompra.getText();
            cargarTabla("", "",año);
        }
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vr.tb_compra){
            try {
                int fila = vr.tb_compra.getSelectedRow();
                int id = Integer.parseInt(vr.tb_compra.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                
                ps = con.prepareStatement("SELECT id_ventas, AÑO, MES FROM db_Ventas WHERE id_ventas = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    vr.lb_año.setText(rs.getString("AÑO"));
                    vr.lb_nota.setText("Venta No._"+rs.getString("id_ventas"));
                    vr.lb_Mes.setText(rs.getString("MES"));
                }
                vr.btn_imprimir.setVisible(true);
                rs.close();
                
                
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
     }

     public void mouseEntered(MouseEvent e) {
        if(e.getSource() == vr.btn_imprimir){
            vr.btn_imprimir.setBackground(Color.red);
        }else if(e.getSource() == vr.tb_compra){
            vr.lb_selc.setVisible(true);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() == vr.btn_imprimir){
            vr.btn_imprimir.setBackground(Color.white);
        }else if(e.getSource() == vr.tb_compra){
            vr.lb_selc.setVisible(false);
        }
     }
    
    
    public void abrirDocumento(String año, String nombre){
         String mes = vr.lb_Mes.getText();
           try{ 
                String Año = vr.txt_AÑO.getText();
              
                    String ruta = System.getProperty("user.home");
                    String url = ruta+"\\OneDrive\\Escritorio\\Floreria_y_Limpieza\\ReciboVentas_"+año + "\\" +mes+"\\"+ nombre + ".pdf";
                    ProcessBuilder P = new ProcessBuilder();
                    P.command("cmd.exe", "/c", url);
                    P.start();
                
                    
            
        } catch (IOException ex) {
            Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se encontro el Archivo");
        }
    }

    
    
    public void cargarTabla(String dato,String id, String año) {
       int id_Compra;
       String mes = vr.txt_Mes.getText();
       String usuario = vr.txt_usuario.getText();
       
       if(id.isEmpty()){
           id_Compra=0;
       }else{
           id_Compra = Integer.parseInt(id);
       }
       
       DefaultTableModel modeloTabla = (DefaultTableModel) vr.tb_compra.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {85, 170, 100, 150, 130, 90, 170,150,220};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            vr.tb_compra.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_ventas, Producto, Cantidad, Precio_Venta, "
                    + "Total_Venta, Estado, Fecha, Usuario, Nombre_Cliente FROM db_Ventas WHERE AÑO = "+año+" ORDER BY id_ventas ASC, Estado ASC";
            
            String sqlFecha = "SELECT id_ventas, Producto, Cantidad, Precio_Venta, "
                    + "Total_Venta, Estado, Fecha, Usuario, Nombre_Cliente FROM db_Ventas WHERE id_ventas LIKE '%"+dato+"%' OR Fecha LIKE '%"+dato+"%' "
                    + "OR Usuario LIKE '%"+dato+"%' ORDER BY id_ventas ASC, Estado ASC";
            
            String sqlAño = "SELECT id_ventas, Producto, Cantidad, Precio_Venta, "
                    + "Total_Venta, Estado, Fecha, Usuario, Nombre_Cliente FROM db_Ventas WHERE AÑO LIKE '%"+año+"%' "
                    + "AND MES LIKE '%"+dato+"%' ORDER BY id_ventas ASC, Estado ASC";
            
            String sqlID = "SELECT id_ventas, Producto, Cantidad, Precio_Venta, "
                    + "Total_Venta, Estado, Fecha, Usuario, Nombre_Cliente FROM db_Ventas WHERE id_ventas LIKE '%"+id_Compra+"%' AND AÑO LIKE '%"+año+"%' ORDER BY id_ventas ASC, Estado ASC";
            
            if ("".equalsIgnoreCase(año)) {
               limpiarTable();

            } else if (!("".equalsIgnoreCase(año)) && !("".equalsIgnoreCase(mes))) {
                ps = con.prepareStatement(sqlAño);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!("".equalsIgnoreCase(dato)) && !("".equalsIgnoreCase(año))) {
                ps = con.prepareStatement(sqlFecha);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!("".equalsIgnoreCase(id))) {
                ps = con.prepareStatement(sqlID);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
            else if (!("".equalsIgnoreCase(año))) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
            
        } catch (Exception e) {
            System.err.println("Error en CARGARtabla: " + e.toString());
        }
    }
    
    
    public void limpiarTable() {
        DefaultTableModel modeloTabla = (DefaultTableModel) vr.tb_compra.getModel();
        modeloTabla.setRowCount(0);
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            modeloTabla.removeRow(i);
            i = i - 1;
        }
    }

    
    
    
}
