/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.AgregarProducto;
import Interfaz.Bienvenida;
import Interfaz.Notificaciones;
import Interfaz.Perecederos;
import Interfaz.VerPedidos;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author CCNAR
 */
public class ControladorNotificaciones implements WindowListener{

    public Notificaciones nt;
    int tamañoLimpieza = 0, flor = 0, tamañoPedHoy = 0, TamañoPreparacion = 0, tamañoPereVen = 0, TamañoPerePorVencer = 0;
    String Pedidos = "";
    String Perecederos = "";
    
    boolean bandera = true;
Bienvenida bn = new Bienvenida();

    public ControladorNotificaciones(Notificaciones nt) {
        this.nt = nt;
        buscarStockProductos();
        buscarPedidos();
        buscarPerecederos();
       // this.nt.addWindowListener(this);
    }
    public ControladorNotificaciones() {
        
    }

    public String getPedidos() {
        return Pedidos;
    }

    public void setPedidos(String Pedidos) {
        this.Pedidos = Pedidos;
    }

    public String getPerecederos() {
        return Perecederos;
    }

    public void setPerecederos(String Perecederos) {
        this.Perecederos = Perecederos;
    }
    
    
    
    
    public boolean estadoFrame(){
        System.out.println("bander = "+bandera);
        if(bandera == true){
            System.out.println("hola");
            return bandera;
        }else{
            System.out.println("hola2");
            return bandera;
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
        if(e.getSource() == nt){
             bn.setBandera(true);
             System.out.println("Not Bnadera "+bn.getBandera());
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
   }

    @Override
    public void windowClosed(WindowEvent e) {
         if(e.getSource() == nt){
             bn.setBandera(false);
             System.out.println("Not Bnadera "+bn.getBandera());
        }
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
        
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
      
    }
   
    
    
    public void buscarStockProductos(){
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT COUNT(Producto) AS tamaño FROM db_Floreria_Productos WHERE Cantidad = 0");
            rs = ps.executeQuery();
            while (rs.next()) {
                flor = (rs.getInt("tamaño"));
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error buscarProducto Floreria : " + e.getMessage());
        }
        
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT COUNT(Producto) AS tamaño FROM db_ProductosLimpieza WHERE Cantidad = 0");
            rs = ps.executeQuery();
            while (rs.next()) {
                tamañoLimpieza = (rs.getInt("tamaño"));
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error buscarProducto Limpieza : " + e.getMessage());
        }
        if (tamañoLimpieza > 0 && flor == 0) {
            nt.btn_0Stock.setBackground(Color.red);
            nt.LB_Stokc_Limpieza.setText(tamañoLimpieza + " Producto(s) en 0 en Limpieza");
            nt.LB_Stokc_floreria.setText(flor+" Producto(s) en 0 en Floreria");
            nt.JP_Limpieza_Color.setBackground(Color.GREEN);
            nt.JP_Floreria_Color.setBackground(Color.white);
            nt.btn_0Stock.setForeground(Color.white);

        }if (tamañoLimpieza == 0 && flor > 0) {
            nt.btn_0Stock.setBackground(Color.red);
            nt.LB_Stokc_Limpieza.setText(tamañoLimpieza + " Producto(s) en 0 en Limpieza");
            nt.LB_Stokc_floreria.setText(flor+" Producto(s) en 0 en Floreria");
            nt.JP_Limpieza_Color.setBackground(Color.white);
            nt.JP_Floreria_Color.setBackground(Color.orange);
            nt.btn_0Stock.setForeground(Color.white);

        } if (tamañoLimpieza > 0 && flor > 0) {
            nt.btn_0Stock.setBackground(Color.red);
            nt.LB_Stokc_Limpieza.setText(tamañoLimpieza + " Producto(s) en 0 en Limpieza");
            nt.LB_Stokc_floreria.setText(flor+" Producto(s) en 0 en Floreria");
            nt.JP_Limpieza_Color.setBackground(Color.green);
            nt.JP_Floreria_Color.setBackground(Color.orange);
            nt.btn_0Stock.setForeground(Color.white);

        }else if (tamañoLimpieza == 0 && flor == 0) {
            nt.btn_0Stock.setVisible(true);
            nt.LB_Stokc_Limpieza.setText(tamañoLimpieza + " Producto(s) en 0 en Limpieza");
            nt.LB_Stokc_floreria.setText(flor+" Producto(s) en 0 en Floreria");
            nt.JP_Limpieza_Color.setBackground(Color.white);
            nt.JP_Floreria_Color.setBackground(Color.white);
            nt.btn_0Stock.setForeground(Color.black);
            nt.btn_0Stock.setBackground(Color.white);
        }
        
    }
    
    
    
    public void buscarPedidos() {
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT COUNT(DiasPendEstado) AS tamaño FROM db_Pedidos WHERE DiasPendEstado = 'HOY'");
            rs = ps.executeQuery();
            while (rs.next()) {
                tamañoPedHoy = (rs.getInt("tamaño"));
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error buscarNotificacionPedido Floreria : " + e.getMessage());
        }

        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT COUNT(DiasPendEstado) AS tamaño FROM db_Pedidos WHERE DiasPendEstado = 'Preparación'");
            rs = ps.executeQuery();
            while (rs.next()) {
                TamañoPreparacion = (rs.getInt("tamaño"));
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error buscarProducto Limpieza : " + e.getMessage());
        }

        if (tamañoPedHoy > 0 && TamañoPreparacion == 0) {
            nt.btn_pedidos.setBackground(Color.red);
            nt.LB_pedHoy.setText(tamañoPedHoy + " Pendiente(s) para Hoy");
            nt.LB_Pedidos_preparar.setText(TamañoPreparacion+" Pendiente(s) para Preparar");
            nt.jp_Hoy_Color.setBackground(Color.green);
            nt.JP_Preparar_Color.setBackground(Color.white);
            nt.btn_pedidos.setForeground(Color.white);
            Pedidos = tamañoPedHoy + " Pendiente(s) para Hoy\n";

        }if (tamañoPedHoy == 0 && TamañoPreparacion > 0) {
            nt.btn_pedidos.setBackground(Color.red);
            nt.LB_pedHoy.setText(tamañoPedHoy + " Pendiente(s) para Hoy");
            nt.LB_Pedidos_preparar.setText(TamañoPreparacion+" Pendiente(s) para Preparar");
            nt.jp_Hoy_Color.setBackground(Color.white);
            nt.JP_Preparar_Color.setBackground(Color.orange);
            nt.btn_pedidos.setForeground(Color.white);
            Pedidos = TamañoPreparacion+" Pendiente(s) para Preparar\n";

        } if (tamañoPedHoy > 0 && TamañoPreparacion > 0) {
            nt.btn_pedidos.setBackground(Color.red);
            nt.LB_pedHoy.setText(tamañoPedHoy + " Pendiente(s) para Hoy");
            nt.LB_Pedidos_preparar.setText(TamañoPreparacion+" Pendiente(s) para Preparar");
            nt.jp_Hoy_Color.setBackground(Color.green);
            nt.JP_Preparar_Color.setBackground(Color.orange);
            nt.btn_pedidos.setForeground(Color.white);
            Pedidos = tamañoPedHoy + " Pendiente(s) para Hoy\n";
            Pedidos = Pedidos + TamañoPreparacion+" Pendiente(s) para Preparar\n";

        }else if (tamañoPedHoy == 0 && TamañoPreparacion == 0) {
            nt.btn_pedidos.setVisible(true);
            nt.LB_pedHoy.setText(tamañoPedHoy + " Pendiente(s) para Hoy");
            nt.LB_Pedidos_preparar.setText(TamañoPreparacion+" Pendiente(s) para Preparar");
            nt.LB_pedHoy.setBackground(Color.white);
            nt.LB_Pedidos_preparar.setBackground(Color.white);
            nt.btn_pedidos.setForeground(Color.black);
            nt.btn_pedidos.setBackground(Color.white);
            Pedidos = "";
        }

    }
    
    
    public void buscarPerecederos() {
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT COUNT(Producto) AS tamaño FROM db_Perecederos WHERE Estado = 'Vencido'");
            rs = ps.executeQuery();
            while (rs.next()) {
                tamañoPereVen = (rs.getInt("tamaño"));
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error buscarNotificacionPedido Floreria : " + e.getMessage());
        }

        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT COUNT(Producto) AS tamaño FROM db_Perecederos WHERE Estado = 'Por Vencer'");
            rs = ps.executeQuery();
            while (rs.next()) {
                TamañoPerePorVencer = (rs.getInt("tamaño"));
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error buscarProducto Limpieza : " + e.getMessage());
        }

        if (tamañoPereVen > 0 && TamañoPerePorVencer == 0) {
            nt.btn_Pere.setBackground(Color.red);
            nt.LB_PereVencido.setText(tamañoPereVen + " Perecedero(s) para Hoy");
            nt.LB_PerePorVencer.setText(TamañoPerePorVencer+" Pendiente(s) para Preparar");
            nt.JP_Vencido_Color.setBackground(Color.green);
            nt.JP_PorVencer_Color.setBackground(Color.white);
            Perecederos = tamañoPereVen + " Perecedero(s) para Hoy\n";
            nt.btn_Pere.setForeground(Color.white);

        }if (tamañoPereVen == 0 && TamañoPerePorVencer > 0) {
            nt.btn_Pere.setBackground(Color.red);
            nt.LB_PereVencido.setText(tamañoPereVen + " Perecedero(s) Vencido(s)");
            nt.LB_PerePorVencer.setText(TamañoPerePorVencer+" Perecedero(s) por Vencer");
            nt.JP_Vencido_Color.setBackground(Color.white);
            nt.JP_PorVencer_Color.setBackground(Color.orange);
            Perecederos = TamañoPerePorVencer+" Perecedero(s) por Vencer\n";
            nt.btn_Pere.setForeground(Color.white);

        } if (tamañoPereVen > 0 && TamañoPerePorVencer > 0) {
            nt.btn_Pere.setBackground(Color.red);
            nt.LB_PereVencido.setText(tamañoPereVen + " Perecedero(s) Vencido(s)");
            nt.LB_PerePorVencer.setText(TamañoPerePorVencer+" Perecedero(s) por Vencer");
            nt.JP_Vencido_Color.setBackground(Color.green);
            nt.JP_PorVencer_Color.setBackground(Color.orange);
            Perecederos = tamañoPereVen + " Perecedero(s) para Hoy\n";
            Perecederos = Perecederos + TamañoPerePorVencer+" Perecedero(s) por Vencer\n";
            nt.btn_Pere.setForeground(Color.white);

        }else if (tamañoPereVen == 0 && TamañoPerePorVencer == 0) {
            nt.btn_Pere.setVisible(true);
            nt.LB_PereVencido.setText(tamañoPereVen + " Perecedero(s) Vencido(s)");
            nt.LB_PerePorVencer.setText(TamañoPerePorVencer+" Perecedero(s) por Vencer");
            nt.JP_Vencido_Color.setBackground(Color.white);
            nt.JP_PorVencer_Color.setBackground(Color.white);
            nt.btn_Pere.setForeground(Color.black);
            nt.btn_Pere.setBackground(Color.white);
            Perecederos = "";
        }

    }
    
    public void BuscarNotificaciones(){
        buscarStockProductos();
        buscarPedidos();
        buscarPerecederos();
    }
    
    
    public String EnviarMsg() {
        String msg = "";
        int floreria = tamañoLimpieza + flor;
        int pedidos = tamañoPedHoy + TamañoPreparacion;
        int pere = tamañoPereVen + TamañoPerePorVencer;
        if (floreria > 0 || pedidos >0 || pere>0){
        int total = tamañoPereVen + TamañoPerePorVencer;
        msg ="\n"+ floreria + " Producto(s) en 0 de Stock \n"+pedidos + " Pedido(s) Pendiente(s) \n"+total + " Perecedero(s) \n";
        }
        return msg;
    }

}
