/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.AgregarProducto;
import Interfaz.AgregarUsuario;
import Interfaz.IngresarCargo;
import Interfaz.IngresarProveedor;
import Modelos.HttpJavaAltiria;
import Modelos.Tabla;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorAgrUser implements ActionListener, MouseListener, KeyListener {

    AgregarUsuario agr;

    public ControladorAgrUser(AgregarUsuario agr) {
        this.agr = agr;
        this.agr.btn_Eliminar.addActionListener(this);
        this.agr.btn_IngresarPro.addActionListener(this);
        this.agr.btn_modificar.addActionListener(this);
        this.agr.tb_user.addMouseListener(this);
        this.agr.cbx_Cargo.addMouseListener(this);
        this.agr.btn_nuevoCli.addActionListener(this);
        this.agr.btn_Eliminar.addMouseListener(this);
        this.agr.btn_IngresarPro.addMouseListener(this);
        this.agr.btn_modificar.addMouseListener(this);
        this.agr.TxtNombre.requestFocus(true);
        this.agr.btn_nuevo.addActionListener(this);
        this.agr.JmenuActivar.addActionListener(this);
        this.agr.JmenuDesactivar.addActionListener(this);
        this.agr.TxtNombre.addKeyListener(this);
        this.agr.txt_Apellido.addKeyListener(this);
        this.agr.txt_Usuario.addKeyListener(this);
        this.agr.txt_id.addKeyListener(this);
        cargarTabla();
        ActualizarId();
        LLenarCBX();
        Botones(false, true,false);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == agr.btn_IngresarPro) {
            if (camposVacios() == true) {
                agr.TxtNombre.requestFocus(true);
            } else {
                String user = agr.txt_Usuario.getText();
                if (verificarProducto(user) == true) {
                    JOptionPane.showMessageDialog(null, "El Usuario se encuentra en la Base de Datos");
                    agr.TxtNombre.requestFocus(true);
                } else {
                    if (verificarContraseñas() == true) {
                        Ingresar();
                        Limpiar();
                        Botones(false, false,true);
                    } else {
                        agr.TxtContraseña.requestFocus(true);
                    }
                }
            }
        } else if (e.getSource() == agr.btn_modificar) {
            if (camposVacios() == true) {
                agr.TxtNombre.requestFocus(true);
            } else {
                String user = agr.TxtNombre.getText();
                if (verificarContraseñas() == true) {
                    Modificar();
                    Botones(false, false,true);
                } else {
                    agr.TxtContraseña.requestFocus(true);
                }

            }

        } else if (e.getSource() == agr.btn_Eliminar) {
            if (camposVacios() == true) {
                agr.tb_user.requestFocus(true);
            } else {
                EliminarRegistro();
                Botones(false, false,true);
            }

        } else if (e.getSource() == agr.btn_nuevo) {
            ActualizarId();
            Limpiar();
            Botones(false, true, false);
        } else if (e.getSource() == agr.btn_nuevoCli) {
            IngresarCargo cg = new IngresarCargo();
            cg.setVisible(true);
        }else if (e.getSource() == agr.JmenuDesactivar){
            if(agr.txt_id.getText().isEmpty()){
                JOptionPane.showMessageDialog(null, "Seleccione una fila");
            }else{
                Limpiar();
                DesactivarActivar("Inactivo");
                Botones(false, false,true);
            }
            
        }else if(e.getSource() == agr.JmenuActivar){
            Limpiar();
            DesactivarActivar("Activo");
            Botones(false, false,true);
        }

    }

    public boolean camposVacios() {
        if (agr.txt_id.getText().isEmpty() || agr.TxtNombre.getText().isEmpty() || agr.txt_Apellido.getText().isEmpty()
                || agr.txt_Usuario.getText().isEmpty() || agr.TxtContraseña.getText().isEmpty() || agr.TxtContraNueva.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos Vacios ", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    public boolean verificarProducto(String user) {
        try {
            String prodAux = "";
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Usuario FROM db_Usuarios WHERE Usuario = ?");
            ps.setString(1, user);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodAux = (rs.getString("Usuario"));
            }
            rs.close();
            if (prodAux.isEmpty()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            System.out.println("error Verificar prod " + e.getMessage());
        }
        return false;
    }

    public boolean verificarContraseñas() {
        String contra = agr.TxtContraseña.getText().toString();
        String contraNueva = agr.TxtContraNueva.getText();
        System.out.println("contra " + contra);
        System.out.println("contraN " + contraNueva);
        if (contra.equals(contraNueva)) {
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Contraseñas Diferentes ", "Verificar contraseñas", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    public void Ingresar() {
        int id = Integer.parseInt(agr.txt_id.getText());
        String Nombre = agr.TxtNombre.getText();
        String Apellido = agr.txt_Apellido.getText();
        String usuario = agr.txt_Usuario.getText();
        String contra = agr.TxtContraseña.getText().toString();
        String telefono = agr.txt_Domicilio.getText();
        String cargo = agr.cbx_Cargo.getSelectedItem().toString();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO db_Usuarios "
                    + "(id_Usuario, Nombre, Apellido, Usuario, Contraseña, Telefono, Cargo) VALUES (?,?,?,?,?,?,?)");
            ps.setInt(1, id);
            ps.setString(2, Nombre);
            ps.setString(3, Apellido);
            ps.setString(4, usuario);
            ps.setString(5, contra);
            ps.setString(6, telefono);
            ps.setString(7, cargo);
            ps.executeUpdate();
            ps.close();
            cargarTabla();
            HttpJavaAltiria msj = new HttpJavaAltiria();
            String men = "Bienvenido a la Floreria 'Los Chuc' \n Para Ingresar al Sistema debes ingresar lo siguiente: \n "
                    + "Usuario: " + usuario + "\n Contraseña: " + contra + "\n Cargo: " + cargo + "\n Gracias.";
            System.out.println("mensaje " + men);
            msj.EnviarSMS("+52" + telefono, men);
            JOptionPane.showMessageDialog(null, "Se Ingreso al Usuario correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void Modificar() {
        int id = Integer.parseInt(agr.txt_id.getText());
        String Nombre = agr.TxtNombre.getText();
        String Apellido = agr.txt_Apellido.getText();
        String usuario = agr.txt_Usuario.getText();
        String contra = agr.TxtContraseña.getText().toString();
        String telefono = agr.txt_Domicilio.getText();
        String cargo = agr.cbx_Cargo.getSelectedItem().toString();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE db_Usuarios SET"
                    + " Nombre = ?, Apellido = ?, Usuario = ?, Contraseña = ?, Telefono = ?, Cargo = ? WHERE id_Usuario = ?");
            ps.setString(1, Nombre);
            ps.setString(2, Apellido);
            ps.setString(3, usuario);
            ps.setString(4, contra);
            ps.setString(5, telefono);
            ps.setString(6, cargo);
            ps.setInt(7, id);
            ps.executeUpdate();
            ps.close();
            cargarTabla();
            HttpJavaAltiria msj = new HttpJavaAltiria();
            String men = "Bienvenido a la Floreria 'Los Chuc' \n Para Ingresar al Sistema debes ingresar lo siguiente: \n "
                    + "Usuario: " + usuario + "\n Contraseña: " + contra + "\n Cargo: " + cargo + "\n Gracias por unirte a nuestro equipo";
            System.out.println("mensaje " + men);
            msj.EnviarSMS("+52" + telefono, men);
            JOptionPane.showMessageDialog(null, "Se Ingreso al Usuario correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void DesactivarActivar(String estado) {
        int id = Integer.parseInt(agr.txt_id.getText());
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE db_Usuarios SET Estado = ? WHERE id_Usuario = ?");
                ps.setString(1, estado);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTabla();
                ps.close();
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        

    }

    public void EliminarRegistro() {
        int id = Integer.parseInt(agr.txt_id.getText());
        String Nombre = agr.TxtNombre.getText();
        String Usuario = agr.txt_Usuario.getText();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("DELETE db_Usuarios WHERE id_Usuario = ? AND Nombre = ? AND Usuario = ?");
            ps.setInt(1, id);
            ps.setString(2, Nombre);
            ps.setString(3, Usuario);
            ps.executeUpdate();
            cargarTabla();
            ps.close();
            JOptionPane.showMessageDialog(null, "Se Elimino el registro seleccionado correctamente");
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }
    }

    public void ActualizarId() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id_Usuario) AS id_Maximo FROM db_Usuarios");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }rs.close();
            agr.txt_id.setText("" + idAux);
        } catch (Exception e) {
        }
    }

    public void Botones(boolean ver, boolean ver2,boolean ver3) {
        agr.btn_Eliminar.setVisible(ver);
        agr.btn_modificar.setVisible(ver);
        agr.btn_IngresarPro.setVisible(ver2);
        agr.btn_nuevo.setVisible(ver3);
    }

    public void LLenarCBX() {
        try {
            agr.cbx_Cargo.removeAllItems();
            String estado = "Activo";
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Cargo FROM Cargo WHERE Estado = ?");
            ps.setString(1, estado);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                String Proveedor = rs.getString("Cargo");
                agr.cbx_Cargo.addItem(Proveedor);
                i++;
            }rs.close();

        } catch (Exception er) {
            System.err.println("Error en cbx: " + er.toString());
        }

    }

    public void cargarTabla() {
        String nombre = agr.TxtNombre.getText();
        String apellido = agr.txt_Apellido.getText();
        String user = agr.txt_Usuario.getText();
        Tabla tableColor = new Tabla();
         int id = 0;
        if(agr.txt_id.getText().isEmpty()){
             id = 0;
        }else{
            id = Integer.parseInt(agr.txt_id.getText());
        }
         agr.tb_user.setDefaultRenderer(agr.tb_user.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) agr.tb_user.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 120, 150, 150, 100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            agr.tb_user.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_Usuario, Nombre, Apellido, Usuario, Telefono, Estado FROM db_Usuarios ORDER BY id_Usuario ASC, Estado ASC";
            String sqlProv = "SELECT id_Usuario, Nombre, Apellido, Usuario, Telefono, Estado FROM db_Usuarios WHERE Nombre LIKE '%" + nombre + "%' "
                    + "and Apellido LIKE '%" + apellido+"%' and Usuario LIKE '%" + user+"%'";
            String sqlId = "SELECT id_Usuario, Nombre, Apellido, Usuario, Telefono, Estado FROM db_Usuarios WHERE id_Usuario = "+id;
            if (nombre.isEmpty() && id == 0) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!(nombre.isEmpty())) {
                ps = con.prepareStatement(sqlProv);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!(id == 0)) {
                ps = con.prepareStatement(sqlId);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
        } catch (Exception e) {
            System.err.println("Error en Cargartabla User: " + e.toString());
        }
    }

    //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == agr.tb_user) {
            try {
                int fila = agr.tb_user.getSelectedRow();
                int id = Integer.parseInt(agr.tb_user.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id_Usuario, Nombre, Apellido, Usuario, Contraseña, Cargo, Telefono, Estado FROM db_Usuarios WHERE id_Usuario = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    agr.txt_id.setText("" + rs.getInt("id_Usuario"));
                    agr.TxtNombre.setText(rs.getString("Nombre"));
                    agr.txt_Apellido.setText(rs.getString("Apellido"));
                    agr.txt_Usuario.setText(rs.getString("Usuario"));
                    agr.TxtContraseña.setText(rs.getString("Contraseña"));
                    agr.TxtContraNueva.setText(rs.getString("Contraseña"));
                    agr.cbx_Cargo.setSelectedItem(rs.getString("Cargo"));
                    agr.txt_Domicilio.setText(rs.getString("Telefono"));
                    agr.txt_estado.setText(rs.getString("Estado"));

                }rs.close();con.close();
                Botones(true, false,true);
                 if (agr.txt_estado.getText().equals("Inactivo")) {
                    agr.JmenuActivar.setVisible(true);
                    agr.JmenuDesactivar.setVisible(false);
                } else if (agr.txt_estado.getText().equals("Activo")) {
                    agr.JmenuActivar.setVisible(false);
                    agr.JmenuDesactivar.setVisible(true);
                }  
            } catch (Exception er) {
                System.err.println("Error en tabla user: " + er.toString());
            }
        } else if (e.getSource() == agr.cbx_Cargo) {
            LLenarCBX();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_nuevoCli) {
            agr.btn_nuevoCli.setBackground(Color.black);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
         if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_nuevoCli) {
            agr.btn_nuevoCli.setBackground(Color.red);
        }

    }

    public void Limpiar() {
        agr.TxtNombre.setText("");
        agr.txt_Apellido.setText("");
        agr.txt_Usuario.setText("");
        agr.TxtContraseña.setText("");
        agr.TxtContraNueva.setText("");
        agr.txt_Domicilio.setText("");
        agr.TxtNombre.requestFocus(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == agr.TxtNombre){
            cargarTabla();
        }else if(e.getSource() == agr.txt_Apellido){
            cargarTabla();
        }else if(e.getSource() == agr.txt_Usuario || e.getSource() == agr.txt_id){
            cargarTabla();
        }
    }

}
