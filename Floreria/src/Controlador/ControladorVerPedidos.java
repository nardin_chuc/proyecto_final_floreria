/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.AgregarProducto;
import Interfaz.VerPedidos;
import Interfaz.VerVentas;
import Modelos.ColorLinea;
import Modelos.TbPedidos;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;

/**
 *
 * @author CCNAR
 */
public class ControladorVerPedidos implements ActionListener, KeyListener, MouseListener{
    
     VerPedidos vr;
    String Valor, dato;

    public ControladorVerPedidos(VerPedidos vr) {
        this.vr = vr;
        this.vr.ckB_Hoy.addActionListener(this);
        this.vr.ckB_Activo.addActionListener(this);
        this.vr.ckB_Finalizado.addActionListener(this);
        this.vr.ckB_Pendientes.addActionListener(this);
        this.vr.ckB_Preparacion.addActionListener(this);
        this.vr.ckB_Todos.addActionListener(this);
        this.vr.txt_idCompra.addKeyListener(this);
        this.vr.txt_fecha.addKeyListener(this);
        this.vr.txt_Mes.addKeyListener(this);
        this.vr.txt_usuario.addKeyListener(this);
        this.vr.txt_AÑO.addKeyListener(this);
        this.vr.txt_cliente.addKeyListener(this);
        this.vr.tb_compra.addMouseListener(this);
//        this.vr.btn_imprimir.addActionListener(this);
        String año =  vr.txt_AÑO.getText();
        String fecha = vr.lb_Fecha.getText();
        cargarTabla("Hoy", "",año);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vr.ckB_Hoy){
            String año = vr.txt_AÑO.getText();
            if(vr.ckB_Hoy.isSelected()) {
                String fecha = vr.lb_Fecha.getText();
                cargarTabla(fecha,"", año);
                vr.ckB_Activo.setSelected(false);
                vr.ckB_Finalizado.setSelected(false);
                vr.ckB_Pendientes.setSelected(false);
                vr.ckB_Preparacion.setSelected(false);
                vr.ckB_Todos.setSelected(false);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            } else{
                cargarTabla("","", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }
        }else if(e.getSource() == vr.ckB_Activo){
            String año = vr.txt_AÑO.getText();
            if(vr.ckB_Activo.isSelected()){
                cargarTabla("Registro Activo", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.ckB_Hoy.setSelected(false);
                vr.ckB_Finalizado.setSelected(false);
                vr.ckB_Pendientes.setSelected(false);
                vr.ckB_Preparacion.setSelected(false);
                vr.ckB_Todos.setSelected(false);
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }else{
                cargarTabla("", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }
        }else if(e.getSource() == vr.ckB_Finalizado){
            String año = vr.txt_AÑO.getText();
            if(vr.ckB_Finalizado.isSelected()){
                cargarTabla("Terminado", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.ckB_Activo.setSelected(false);
                vr.ckB_Hoy.setSelected(false);
                vr.ckB_Pendientes.setSelected(false);
                vr.ckB_Preparacion.setSelected(false);
                vr.ckB_Todos.setSelected(false);
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }else{
                cargarTabla("", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }
        }else if(e.getSource() == vr.ckB_Pendientes){
            String año = vr.txt_AÑO.getText();
            if(vr.ckB_Pendientes.isSelected()){
                cargarTabla("Pendiente", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.ckB_Activo.setSelected(false);
                vr.ckB_Finalizado.setSelected(false);
                vr.ckB_Hoy.setSelected(false);
                vr.ckB_Preparacion.setSelected(false);
                vr.ckB_Todos.setSelected(false);
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }else{
                cargarTabla("", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }
        }else if(e.getSource() == vr.ckB_Preparacion){
            String año = vr.txt_AÑO.getText();
            if(vr.ckB_Preparacion.isSelected()){
                cargarTabla("Preparación", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.ckB_Activo.setSelected(false);
                vr.ckB_Finalizado.setSelected(false);
                vr.ckB_Pendientes.setSelected(false);
                vr.ckB_Hoy.setSelected(false);
                vr.ckB_Todos.setSelected(false);
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }else{
                cargarTabla("", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }
        }else if(e.getSource() == vr.ckB_Todos){
            String año = vr.txt_AÑO.getText();
            if(vr.ckB_Todos.isSelected()){
                cargarTabla("", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.ckB_Activo.setSelected(false);
                vr.ckB_Finalizado.setSelected(false);
                vr.ckB_Pendientes.setSelected(false);
                vr.ckB_Preparacion.setSelected(false);
                vr.ckB_Hoy.setSelected(false);
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }else{
                cargarTabla("", "", año);
                vr.txt_Mes.setText("");
                vr.txt_fecha.setText("");
                vr.txt_idCompra.setText("");
                vr.txt_usuario.setText("");
                vr.textArea_descrip.setText("");
                verDatosBusqueda(false);
            }
//        }else if(e.getSource() == vr.btn_imprimir){
//            abrirDocumento(vr.lb_año.getText(), vr.lb_nota.getText());
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        String año;
        if(e.getSource() == vr.txt_idCompra){
          String id = vr.txt_idCompra.getText();
          año = vr.txt_AÑO.getText();
          cargarTabla("",id,año);
          vr.ckB_Hoy.setSelected(false);
          vr.textArea_descrip.setText("");
          verDatosBusqueda(false);
        }else if(e.getSource() == vr.txt_fecha){
          Valor = vr.txt_fecha.getText();
            año = vr.txt_AÑO.getText();
            cargarTabla(Valor, "",año);
            vr.ckB_Hoy.setSelected(false);
            vr.textArea_descrip.setText("");
            verDatosBusqueda(false);
        }else if(e.getSource() == vr.txt_Mes){
            String mes = vr.txt_Mes.getText();
           año = vr.txt_AÑO.getText();
          cargarTabla(mes, "", año);
          vr.ckB_Hoy.setSelected(false);
          vr.textArea_descrip.setText("");
          verDatosBusqueda(false);
        }else if(e.getSource() == vr.txt_usuario){
          Valor = vr.txt_usuario.getText();
            año = vr.txt_AÑO.getText();
            cargarTabla(Valor, "",año);
            vr.ckB_Hoy.setSelected(false);
            vr.textArea_descrip.setText("");
            verDatosBusqueda(false);
        }else if(e.getSource() == vr.txt_AÑO){
          año = vr.txt_AÑO.getText();
            String id = vr.txt_idCompra.getText();
            cargarTabla("", "",año);
            vr.textArea_descrip.setText("");
            vr.ckB_Hoy.setSelected(false);
            verDatosBusqueda(false);
        }else if(e.getSource() == vr.txt_cliente){
          año = vr.txt_AÑO.getText();
          Valor = vr.txt_cliente.getText();
          verDatosBusqueda(false);
            String id = vr.txt_idCompra.getText();
            cargarTabla(Valor, "",año);
            vr.textArea_descrip.setText("");
            vr.ckB_Hoy.setSelected(false);
            verDatosBusqueda(false);
        }
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vr.tb_compra){
            try {
                ColorLinea li = new ColorLinea();
                int fila = vr.tb_compra.getSelectedRow();
                int id = Integer.parseInt(vr.tb_compra.getValueAt(fila, 0).toString());
//                if(vr.tb_compra.isRowSelected(fila)){
//                    vr.tb_compra.setBackground(Color.red);
//                    System.out.println("hola");
//                    System.out.println("fila "+fila);
//                }
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                
                ps = con.prepareStatement("SELECT id_pedido, Nombre, Telefono, Descripcion FROM db_pedidos WHERE id_pedido = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    vr.txt_id.setText(rs.getString("id_pedido"));
                    vr.txt_id1.setText(rs.getString("Nombre"));
                    vr.txt_telefono.setText(rs.getString("Telefono"));
                    vr.textArea_descrip.setText(rs.getString("Descripcion"));
                }
                rs.close();
                verDatosBusqueda(true);
                
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
           
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
     }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == vr.tb_compra) {
            vr.lb_selc.setVisible(true);
            vr.lb_selc.setText("Haga Click en una fila para Visualizar");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == vr.tb_compra) {
            vr.lb_selc.setVisible(false);
        }
    }
    public void abrirDocumento(String año, String nombre){
           try{ 
                String Año = vr.txt_AÑO.getText();
              
                    String ruta = System.getProperty("user.home");
                    String url = ruta+"\\OneDrive\\Escritorio\\Floreria_y_Limpieza\\ReciboVentas_"+año + "\\" + nombre + ".pdf";
                    ProcessBuilder P = new ProcessBuilder();
                    P.command("cmd.exe", "/c", url);
                    P.start();
                
                    
            
        } catch (IOException ex) {
            Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se encontro el Archivo");
        }
    }

    
    
    public void cargarTabla(String dato,String id, String año) {
       
       int id_Compra;
       String mes = vr.txt_Mes.getText();
       String usuario = vr.txt_usuario.getText();
       
       if(id.isEmpty()){
           id_Compra=0;
       }else{
           id_Compra = Integer.parseInt(id);
       }
       TbPedidos tableColor = new TbPedidos();
       vr.tb_compra.setDefaultRenderer(vr.tb_compra.getColumnClass(0), tableColor);
       
        
       DefaultTableModel modeloTabla = (DefaultTableModel) vr.tb_compra.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {95, 165, 165, 150, 150, 170, 150, 130, 150,160,120};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            vr.tb_compra.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
           
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_pedido, Nombre, Telefono, FechaRegistro, "
                    + "Saldo, Descripcion, FechaEvento, HoraEvento,  "
                    + "DiasFaltantes, DiasPendEstado, Usuario FROM db_Pedidos WHERE AÑO = "+año+" ORDER BY DiasPendEstado ASC, DiasFaltantes ASC, id_pedido ASC";
            
            String sqlFecha = "SELECT id_pedido, Nombre, Telefono, FechaRegistro, "
                    + "Saldo, Descripcion, FechaEvento, HoraEvento,  "
                    + "DiasFaltantes, DiasPendEstado, Usuario FROM db_Pedidos  WHERE id_pedido LIKE '%"+dato+"%' OR FechaEvento LIKE '%"+dato+"%' "
                    + "OR Usuario LIKE '%"+dato+"%' OR DiasPendEstado LIKE '%"+dato+"%' OR Nombre LIKE '%"+dato+"%' ORDER BY DiasPendEstado ASC, DiasFaltantes ASC, id_pedido ASC";
            
            String sqlAño = "SELECT id_pedido, Nombre, Telefono, FechaRegistro, "
                    + "Saldo, Descripcion, FechaEvento, HoraEvento,  "
                    + "DiasFaltantes, DiasPendEstado, Usuario FROM db_Pedidos  WHERE AÑO LIKE '%"+año+"%' "
                    + "AND MES LIKE '%"+dato+"%' ORDER BY DiasPendEstado ASC, id_pedido ASC";
            
            String sqlID = "SELECT id_pedido, Nombre, Telefono, FechaRegistro, "
                    + "Saldo, Descripcion, FechaEvento, HoraEvento,  "
                    + "DiasFaltantes, DiasPendEstado, Usuario FROM db_Pedidos WHERE id_pedido LIKE '%"+id_Compra+"%' AND AÑO LIKE '%"+año+"%' ORDER BY DiasPendEstado ASC, DiasFaltantes ASC, id_pedido ASC";
            
            if ("".equalsIgnoreCase(año)) {
               limpiarTable();

            } else if (!("".equalsIgnoreCase(año)) && !("".equalsIgnoreCase(mes))) {
                ps = con.prepareStatement(sqlAño);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!("".equalsIgnoreCase(dato)) && !("".equalsIgnoreCase(año))) {
                ps = con.prepareStatement(sqlFecha);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!("".equalsIgnoreCase(id))) {
                ps = con.prepareStatement(sqlID);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
            else if (!("".equalsIgnoreCase(año))) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
            
        } catch (Exception e) {
            System.err.println("Error en CARGARtabla: " + e.toString());
        }
    }
    
    public void verDatosBusqueda(boolean ver){
        vr.jLabel2.setVisible(ver);
         vr.jLabel6.setVisible(ver);
        vr.txt_id.setVisible(ver);
        vr.txt_id1.setVisible(ver);
        vr.jLabel7.setVisible(ver); 
        vr.txt_telefono.setVisible(ver);
        vr.jLabel5.setVisible(ver); 
        vr.textArea_descrip.setVisible(ver);
    }
    
    
    public void limpiarTable() {
        DefaultTableModel modeloTabla = (DefaultTableModel) vr.tb_compra.getModel();
        modeloTabla.setRowCount(0);
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            modeloTabla.removeRow(i);
            i = i - 1;
        }
    }

    
    
    
}
