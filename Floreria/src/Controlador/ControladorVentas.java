/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.Compras;
import Interfaz.IngresarCliente;
import Interfaz.Ventas;
import Modelos.TbProducto;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Rectangle;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;

/**
 *
 * @author CCNAR
 */
public class ControladorVentas implements ActionListener, MouseListener, KeyListener {

    Ventas agr;
    ControladorPerecederos pe;

    public ControladorVentas(Ventas agr) {
        this.agr = agr;
        pe = new ControladorPerecederos(this.agr);
        this.agr.btn_Eliminar.addActionListener(this);
        this.agr.btn_IngresarPro.addActionListener(this);
        this.agr.btn_modificar.addActionListener(this);
        this.agr.tb_Ventas.addMouseListener(this);
        this.agr.tb_Productos.addMouseListener(this);
        this.agr.txt_idProducto.requestFocus(true);
        this.agr.btn_Eliminar.addMouseListener(this);
        this.agr.btn_IngresarPro.addMouseListener(this);
        this.agr.btn_IngresarPro.addKeyListener(this);
        this.agr.btn_modificar.addMouseListener(this);
        this.agr.txt_idProducto.addKeyListener(this);
        this.agr.txt_idVenta.addKeyListener(this);
        this.agr.txt_proveedor.addKeyListener(this);
        this.agr.txt_Producto.addKeyListener(this);
        this.agr.btn_nuevo.addActionListener(this);
        this.agr.btn_pagar.addActionListener(this);
        this.agr.btn_finalizarVenta.addActionListener(this);
        this.agr.cbx_tipoVenta.addActionListener(this);
        this.agr.btn_nuevoCli.addActionListener(this);
        this.agr.btn_imprimir.addActionListener(this);
        this.agr.cbx_cliente.addMouseListener(this);
        this.agr.txt_cantidad.addKeyListener(this);
        ActualizarId();
        cargarTabla();
        cargarProducto("", "");
        Botones(false, false, false, false);
        sumaTotal();
        LLenarCBX();
        agr.btn_imprimir.setVisible(false);
        AutoCompleteDecorator.decorate(this.agr.cbx_cliente);
        agr.txt_idProducto.setEditable(true);
        agr.txt_cantidad.setEditable(true);
    }

    public ControladorVentas() {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == agr.btn_IngresarPro) {
            if (camposVacios() == true) {
                agr.txt_idProducto.requestFocus(true);
            } else {
                int idproducto = Integer.parseInt(agr.txt_idProducto.getText());
                if (verificarProducto(idproducto) == true) {
                    int Cantidad = Integer.parseInt(agr.txt_cantidad.getText());
                    int stock = Integer.parseInt(agr.Lb_Stock.getText());
                    if (Cantidad == 0 || Cantidad < 0) {
                        agr.txt_cantidad.setText("");
                        agr.txt_cantidad.requestFocus(true);
                    } else if (Cantidad < stock || Cantidad == stock) {
                        Ingresar();
                        pe.CalcularCantidad("-");
                        Botones(false, false, false, false);
                        cargarProducto("", "");
                        sumaTotal();
                        Limpiar();
                    } else if (Cantidad > stock) {
                        JOptionPane.showMessageDialog(null, "Stock Insuficiente", "Error en Stock", JOptionPane.ERROR_MESSAGE);
                        agr.txt_cantidad.setText("");
                        agr.txt_cantidad.requestFocus(true);
                    }

                } else {
                    agr.txt_idProducto.requestFocus(true);
                    JOptionPane.showMessageDialog(null, "El id del Producto no se encuentra en la Base de Datos", "Verificar ID_Producto", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (e.getSource() == agr.btn_nuevo) {
            Limpiar();
            ActualizarId();
            cargarTabla();
            Botones(false, false, false, false);
            buscarUser(agr.txt_user.getText());
            agr.txt_idProducto.setEditable(true);
            agr.txt_cantidad.setEditable(true);

        } else if (e.getSource() == agr.btn_finalizarVenta) {

            if (agr.txt_cambio.getText().isEmpty()) {
                agr.txt_pago.requestFocus();
                JOptionPane.showMessageDialog(null, "Dijite el monto de Pago Correcto");
            } else {
                Float costo = Float.parseFloat(agr.txt_total.getText());
                Float Pago = Float.parseFloat(agr.txt_pago.getText());
                if (costo < Pago || costo == Pago) {
                    JOptionPane.showMessageDialog(null, "Se Realizo Correctamente");
                    finalizarVenta();
                    generarReciboPdf();
                    ActualizarId();
                    cargarTabla();
                    agr.TxtSubToal.setText("");
                    agr.TxtTOTAL.setText("");
                    agr.txt_pago.setText("");
                    agr.txt_cambio.setText("");
                    agr.PagarVenta.setVisible(false);
                } else {
                    JOptionPane.showMessageDialog(null, "Dijite el monto de Pago Correctamente");
                    agr.txt_pago.requestFocus();
                }

            }

        } else if (e.getSource() == agr.cbx_tipoVenta) {
            cargarProducto("", "");
        } else if (e.getSource() == agr.btn_nuevoCli) {
            IngresarCliente cli = new IngresarCliente();
            cli.setVisible(true);
        } else if (e.getSource() == agr.btn_imprimir) {
            if (VerivicarEstadoPagado() == true) {
                JOptionPane.showMessageDialog(null, "Genere el pago correspondiente");
            } else {
                CrearCapetaAuto();
                generarReciboPdf();
            }
        }

    }

    public void AccionarModificacion(Ventas vn) {
        ;
        if (camposVacios() == true) {
            vn.txt_idProducto.requestFocus(true);
        } else {
            int Cantidad = Integer.parseInt(vn.txt_cantidad.getText());
            int stock = Integer.parseInt(vn.Lb_Stock.getText());
            int cantAux = Integer.parseInt(vn.txt_Cant.getText());
            if (Cantidad == 0 || Cantidad < 0) {
                vn.txt_cantidad.setText("");
                vn.txt_cantidad.requestFocus(true);
            } else if (Cantidad < cantAux || Cantidad == stock || Cantidad == Integer.parseInt(vn.txt_Cant.getText())) {
                Modificar();
                pe.SepararCadena();
                pe.CalcularCantidad("-");
                cargarTabla();
                cargarProducto("", "");
                Limpiar();
                sumaTotal();
                Botones(false, false, false, true);
            } else if (Cantidad > stock) {
                JOptionPane.showMessageDialog(null, "Stock Insuficiente", "Error en Stock", JOptionPane.ERROR_MESSAGE);
                vn.txt_cantidad.setText("");
                vn.txt_cantidad.requestFocus(true);
            }
        }
    }

    public String verificacionTabla() {
        String tipoVenta = agr.cbx_tipoVenta.getSelectedItem().toString();
        String tablaSQL = "";
        if (tipoVenta.equals("FLORERIA")) {
            tablaSQL = "db_Floreria_Productos";
        } else if (tipoVenta.equals("PRODUCTOS DE LIMPIEZA")) {
            tablaSQL = "db_ProductosLimpieza";
        }
        return tablaSQL;
    }

    public boolean camposVacios() {
        if (agr.txt_idProducto.getText().isEmpty() || agr.txt_Producto.getText().isEmpty() || agr.txt_cantidad.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos Vacios ", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    public boolean verificarProducto(int idProd) {
        try {
            String tabla = verificacionTabla();
            String prodAux = "";
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto FROM " + tabla + " WHERE id = ?");
            ps.setInt(1, idProd);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodAux = (rs.getString("Producto"));
            }
            rs.close();
            if (prodAux.isEmpty()) {
                agr.txt_Producto.setText(prodAux);
                return false;
            } else {
                agr.txt_Producto.setText(prodAux);
                return true;
            }
        } catch (Exception e) {
            System.out.println("error Verificar prod " + e.getMessage());
        }
        return false;
    }

    public void Ingresar() {
        int idVentas = Integer.parseInt(agr.txt_idVenta.getText());
        String Año = agr.txt_AÑO.getText();
        String MES = agr.txt_Mes.getText();
        String FECHA = agr.txt_fecha.getText();
        int idProducto = Integer.parseInt(agr.txt_idProducto.getText());
        String Producto = agr.txt_Producto.getText();
        float precio_venta = Float.parseFloat(agr.txt_precio.getText());
        String cliente = agr.cbx_cliente.getSelectedItem().toString();
        int cantidad = Integer.parseInt(agr.txt_cantidad.getText());
        String user = agr.txt_user.getText();
        int idUsuario = buscarUser(user);
        float subtotal = cantidad * precio_venta;
        float total = subtotal + ((subtotal * 16) / 100);
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_Ventas ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?");
            ps.setInt(1, idVentas);
            ps.setString(2, Año);
            ps.setString(3, MES);
            ps.setString(4, FECHA);
            ps.setString(5, Producto);
            ps.setInt(6, idProducto);
            ps.setInt(7, cantidad);
            ps.setString(8, user);
            ps.setInt(9, idUsuario);
            ps.setString(10, cliente);
            ps.setFloat(11, precio_venta);
            ps.setFloat(12, subtotal);
            ps.setFloat(13, total);
            ps.executeUpdate();
            ps.close();
            cargarTabla();
            String tabla = verificacionTabla();
            Connection conectar2 = Conexion.establecerConnection();
            PreparedStatement ps2 = conectar.prepareStatement("UPDATE " + tabla + " SET Cantidad = Cantidad - ? WHERE Producto = ?");
            ps2.setInt(1, cantidad);
            ps2.setString(2, Producto);
            ps2.executeQuery();
            ps2.close();

            JOptionPane.showMessageDialog(null, "Se Ingreso la venta correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public boolean VerivicarEstadoPagado() {
        try {
            int id = Integer.parseInt(agr.txt_idVenta.getText());
            String Producto = "";
            ResultSet rs;
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto FROM db_Ventas WHERE id_ventas = ? AND Estado = 'Pagado'");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto = rs.getString("Producto");
            }
            rs.close();

            if (Producto.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
        }
        return true;
    }

    public void Modificar() {
        int idVentas = Integer.parseInt(agr.txt_idVenta.getText());
        String Año = agr.txt_AÑO.getText();
        String MES = agr.txt_Mes.getText();
        String FECHA = agr.txt_fecha.getText();
        int idProducto = Integer.parseInt(agr.txt_idProducto.getText());
        String Producto = agr.txt_Producto.getText();
        float precio_venta = Float.parseFloat(agr.txt_precio.getText());
        String cliente = agr.cbx_cliente.getSelectedItem().toString();
        int cantidad = Integer.parseInt(agr.txt_cantidad.getText());
        int cantidaAux = Integer.parseInt(agr.txt_Cant.getText());
        String user = agr.txt_user.getText();
        int idUsuario = buscarUser(user);
        float subtotal = cantidad * precio_venta;
        float total = subtotal + ((subtotal * 16) / 100);
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_ModificarVentas ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?");
            ps.setInt(1, idVentas);
            ps.setString(2, Año);
            ps.setString(3, MES);
            ps.setString(4, FECHA);
            ps.setString(5, Producto);
            ps.setInt(6, idProducto);
            ps.setInt(7, cantidad);
            ps.setInt(8, cantidaAux);
            ps.setString(9, user);
            ps.setInt(10, idUsuario);
            ps.setString(11, cliente);
            ps.setFloat(12, precio_venta);
            ps.setFloat(13, subtotal);
            ps.setFloat(14, total);
            ps.executeUpdate();
            ps.close();
            actualizarCantidadStock(cantidaAux, Producto, "+");
            actualizarCantidadStock(cantidad, Producto, "-");
            cargarTabla();

            JOptionPane.showMessageDialog(null, "Se Modifico la venta correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void EliminarRegistro(Ventas vn) {
        if (agr.txt_op.getText().equals("SI")) {
            int idProducto = Integer.parseInt(vn.txt_idProducto.getText());
            int idVenta = Integer.parseInt(vn.txt_idVenta.getText());
            int Cantidad = Integer.parseInt(vn.txt_cantidad.getText());
            String Producto = vn.txt_Producto.getText();
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_EliminarVenta ?,?,?");
                ps.setString(1, Producto);
                ps.setInt(2, Cantidad);
                ps.setInt(3, idVenta);
                ps.executeUpdate();
                ps.close();
                actualizarCantidadStock(Cantidad, Producto, "+");
                cargarTabla();
                cargarProducto(vn.txt_idProducto.getText(), Producto);
                Limpiar();
                JOptionPane.showMessageDialog(null, "Se Elimino el registro seleccionado correctamente");
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        } else {

        }

    }

    public void finalizarVenta() {
        try {
            String estado = "Pagado";
            Float pagado = Float.parseFloat(agr.txt_pago.getText());
            int id = Integer.parseInt(agr.txt_idVenta.getText());
            ResultSet rs;
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE db_Ventas SET Pago_Usuario = ?, Estado = ? WHERE id_ventas = ?");
            ps.setDouble(1, pagado);
            ps.setString(2, estado);
            ps.setInt(3, id);
            rs = ps.executeQuery();
            rs.close();
        } catch (Exception e) {
            System.out.println("error en finalizar venta: " + e.getMessage());
        }
    }

    public void actualizarCantidadStock(int cantidad, String Producto, String signo) {
        try {
            String tabla = buscarProductoTabla(Producto, "db_Floreria_Productos");
            ResultSet rs;
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + tabla + " SET Cantidad = Cantidad " + signo + " ? WHERE Producto = ?");
            ps.setInt(1, cantidad);
            ps.setString(2, Producto);
            rs = ps.executeQuery();
            rs.close();
        } catch (Exception e) {
            System.out.println("error en actualizar stock " + e.getMessage());
        }
    }

    public String buscarProductoTabla(String Producto, String table) {
        String tablaEncotrada = "";
        String Prov = "";
        try {
            String prodAux = "";
            ResultSet rs;
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto, Proveedor FROM " + table + " WHERE Producto = ?");
            ps.setString(1, Producto);
            rs = ps.executeQuery();

            while (rs.next()) {
                prodAux = rs.getString("Producto");
                Prov = rs.getString("Proveedor");
            }
            if (prodAux.isEmpty()) {
                tablaEncotrada = buscarProductoTabla(Producto, "db_ProductosLimpieza");
                //  agr.txt_proveedor.setText(Prov);
            } else {
                agr.txt_proveedor.setText(Prov);
                return table;
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("error buscar Producto tabla " + e.getMessage());
        }
        return tablaEncotrada;
    }

    public void buscarCBX(String tabla) {

        try {
            if (tabla.equals("db_Floreria_Productos")) {
                agr.cbx_tipoVenta.setSelectedItem("FLORERIA");
                cargarTabla();
            } else if (tabla.equals("db_ProductosLimpieza")) {
                agr.cbx_tipoVenta.setSelectedItem("PRODUCTOS DE LIMPIEZA");
                cargarTabla();
            }

        } catch (Exception e) {
            System.out.println("precio " + e.getMessage());
        }
    }

    public void sumaTotal() {
        float subtotal = 0;
        float total = 0;
        int idVenta;
        if (agr.txt_idVenta.getText().isEmpty()) {
            idVenta = 0;
        } else {
            idVenta = Integer.parseInt(agr.txt_idVenta.getText());
        }

        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT SUM(Subtotal) AS Subtotal, SUM(Total_Venta) AS total FROM db_Ventas WHERE id_ventas = ?");
            ps.setInt(1, idVenta);
            rs = ps.executeQuery();
            while (rs.next()) {
                subtotal = (rs.getInt("Subtotal"));
                total = (rs.getInt("total"));
            }
            agr.TxtSubToal.setText("" + subtotal);
            agr.TxtTOTAL.setText("" + total);
            rs.close();
        } catch (Exception e) {
            System.out.println("error en Buscar User " + e.getMessage());
        }
    }

    public int buscarUser(String user) {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT id_Usuario  FROM db_Usuarios WHERE Usuario = ?");
            ps.setString(1, user);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Usuario"));
            }
            rs.close();
            return idAux;
        } catch (Exception e) {
            System.out.println("error en Buscar User " + idAux);
        }
        return idAux;
    }

    public void ActualizarIdVn(Ventas vn) {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id_ventas) AS id_Maximo FROM db_Ventas");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            rs.close();
            vn.txt_idVenta.setText("" + idAux);
            cargarTabla();
        } catch (Exception e) {
        }
    }

    public void ActualizarId() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id_ventas) AS id_Maximo FROM db_Ventas");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            rs.close();
            agr.txt_idVenta.setText("" + idAux);
        } catch (Exception e) {
        }
    }

    public void Botones(boolean ver, boolean ver2, boolean ver3, boolean ver4) {
        agr.btn_Eliminar.setVisible(ver);
        agr.btn_modificar.setVisible(ver3);
        agr.btn_IngresarPro.setEnabled(ver2);
        agr.btn_nuevo.setVisible(ver4);
    }

    public void LLenarCBX() {
        try {

            agr.cbx_cliente.removeAllItems();
            PreparedStatement ps;
            ResultSet rs;
            String Estado = "Activo";
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Nombre, Apellido  FROM db_Cliente WHERE Estado = ? ORDER BY id_Cliente");
            ps.setString(1, Estado);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                String nom = rs.getString("Nombre");
                String apellido = rs.getString("Apellido");
                String NomCompleto = nom + " " + apellido;
                agr.cbx_cliente.addItem(NomCompleto);
                i++;
            }
            rs.close();

        } catch (Exception er) {
            System.err.println("Error en cbx Cliente: " + er.toString());
        }

    }

    public void cargarProducto(String idProd, String info) {
        String tabla = verificacionTabla();
        TbProducto tableColor = new TbProducto();
        agr.tb_Productos.setDefaultRenderer(agr.tb_Productos.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) agr.tb_Productos.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;

        int id = 0;
        if (idProd.isEmpty()) {
            id = 0;
        } else {
            id = Integer.parseInt(idProd);
        }

        int columnas;
        int[] ancho = {90, 150, 90, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            agr.tb_Productos.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sqlID = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM " + tabla + " WHERE id = ? AND Estado = 'Activo'";
            String vacio = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM " + tabla + " WHERE Estado = 'Activo' ORDER BY id";
            String dato = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM " + tabla + " WHERE Producto LIKE '%" + info + "%' AND Estado = 'Activo'"
                    + "OR Proveedor LIKE '%" + info + "%'";

            if ("".equalsIgnoreCase(idProd) && "".equalsIgnoreCase(info)) {
                ps = con.prepareStatement(vacio);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!("".equalsIgnoreCase(info))) {
                ps = con.prepareStatement(dato);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!("".equalsIgnoreCase(idProd)) && ("".equalsIgnoreCase(info))) {
                ps = con.prepareStatement(sqlID);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
        } catch (Exception e) {
            System.err.println("Error en tabla CargarProd: " + e.toString());
        }
    }

    public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) agr.tb_Ventas.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int id = 0;
        if (agr.txt_idVenta.getText().isEmpty()) {
            id = 0;
        } else {
            id = Integer.parseInt(agr.txt_idVenta.getText());
        }
        int columnas;
        int[] ancho = {90, 150, 90, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            agr.tb_Ventas.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_ventas, Producto, Cantidad, Precio_Venta, Subtotal Estado FROM db_Ventas WHERE id_ventas = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("Error en tabla CargarVentasprod: " + e.toString());
        }
    }

    //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == agr.tb_Ventas) {
            try {
                String Nombretabla = "";
                String precioAux = "";
                String provAux = "";
                String proAux = "";
                String idPro = "";
                String Cantidad = "";
                String user = "";
                String Cliente = "";
                String FechaSQL = "";
                int fila = agr.tb_Ventas.getSelectedRow();
                int id = Integer.parseInt(agr.tb_Ventas.getValueAt(fila, 0).toString());
                String Pro = agr.tb_Ventas.getValueAt(fila, 1).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT  Producto, id_Producto, Cantidad, Usuario, Precio_Venta, Nombre_Cliente,Fecha FROM db_Ventas WHERE id_ventas = ? AND Producto = ?");
                ps.setInt(1, id);
                ps.setString(2, Pro);
                rs = ps.executeQuery();
                while (rs.next()) {
                    idPro = ("" + rs.getInt("id_Producto"));
                    proAux = (rs.getString("Producto"));
                    Cantidad = "" + rs.getInt("Cantidad");
                    agr.txt_Cant.setText("" + rs.getInt("Cantidad"));
                    user = (rs.getString("Usuario"));
                    precioAux = rs.getString("Precio_Venta");
                    Cliente = (rs.getString("Nombre_Cliente"));
                    FechaSQL = rs.getString("Fecha");
                }
                Nombretabla = buscarProductoTabla(proAux, "db_Floreria_Productos");
                Botones(verifecha(FechaSQL), false, verifecha(FechaSQL), true);
                buscarCBX(Nombretabla);
                cargarProducto(idPro, proAux);
                agr.btn_imprimir.setEnabled(true);
                agr.btn_imprimir.setVisible(true);
                agr.txt_Producto.setText(proAux);
                agr.txt_idProducto.setText(idPro);
                Stock(agr.txt_idProducto.getText());
                agr.txt_cantidad.setText(Cantidad);
                agr.txt_user.setText(user);
                agr.txt_precio.setText(precioAux);
                agr.cbx_cliente.setSelectedItem(Cliente);
                rs.close();
            } catch (Exception er) {
                System.err.println("Error en tabla prod mouse: " + er.toString());
            }
        } else if (e.getSource() == agr.tb_Productos) {
            String tabla = verificacionTabla();
            try {
                int fila = agr.tb_Productos.getSelectedRow();
                int id = Integer.parseInt(agr.tb_Productos.getValueAt(fila, 0).toString());
                String Pro = agr.tb_Productos.getValueAt(fila, 1).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Producto,  Precio_Compra, Proveedor FROM " + tabla + " WHERE id = ? AND Producto = ?");
                ps.setInt(1, id);
                ps.setString(2, Pro);
                rs = ps.executeQuery();
                while (rs.next()) {
                    agr.txt_idProducto.setText("" + rs.getInt("id"));
                    agr.txt_Producto.setText(rs.getString("Producto"));
                    agr.txt_precio.setText("" + rs.getFloat("Precio_Compra"));
                    agr.txt_proveedor.setText(rs.getString("Proveedor"));

                }
                Botones(false, true, false, true);
                agr.txt_cantidad.requestFocus(true);
                Stock(agr.txt_idProducto.getText());
                rs.close();
            } catch (Exception er) {
                System.err.println("Error en tabla prod mouse: " + er.toString());
            }
        } else if (e.getSource() == agr.cbx_cliente) {
            LLenarCBX();
        }
    }

    private boolean verifecha(String fecha) {
        LocalDateTime dia = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        String hoy = dia.format(formato);
        System.out.println();
        if (fecha.equals(hoy)) {
            agr.txt_idProducto.setEditable(true);
            agr.txt_cantidad.setEditable(true);
            return true;
        } else {
            agr.txt_idProducto.setEditable(false);
            agr.txt_cantidad.setEditable(false);
            return false;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.black);
        } else if (e.getSource() == agr.tb_Ventas) {
            agr.lb_selc.setVisible(true);
            agr.lb_selc.setText("Haga Click en una fila para Modificar");
        } else if (e.getSource() == agr.tb_Productos) {
            agr.lb_selc1.setVisible(true);
            agr.lb_selc1.setText("Haga Click para Seleccionar Producto");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.red);
        } else if (e.getSource() == agr.tb_Ventas) {
            agr.lb_selc.setVisible(false);
        } else if (e.getSource() == agr.tb_Productos) {
            agr.lb_selc1.setVisible(false);
        }

    }

    public void Stock(String id) {
        int stock = 0;
        int idP = 0;

        if (id.isEmpty()) {
            idP = 0;
        } else {
            idP = Integer.parseInt(agr.txt_idProducto.getText());
        }
        String tabla = verificacionTabla();

        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Cantidad FROM " + tabla + " WHERE id = ?");
            ps.setInt(1, idP);
            rs = ps.executeQuery();
            while (rs.next()) {
                stock = (rs.getInt("Cantidad"));
            }
            agr.Lb_Stock.setText("" + stock);
            rs.close();
        } catch (Exception e) {
            System.out.println("error buscarProducto " + e.getMessage());
        }
    }

    public void Limpiar() {
        agr.txt_Producto.setText("");
        agr.txt_precio.setText("");
        agr.txt_idProducto.setText("");
        agr.txt_cantidad.setText("");
        agr.Lb_Stock.setText("");
        agr.txt_proveedor.setText("");
        agr.txt_idProducto.requestFocus(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == agr.txt_idProducto) {
                if (agr.txt_Producto.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Producto No Encontrado", "ID no Encontrado", JOptionPane.ERROR_MESSAGE);
                    agr.txt_idProducto.requestFocus(true);
                } else {
                    agr.txt_cantidad.requestFocus(true);
                    Botones(false, true, false, false);
                }
            } else if (e.getSource() == agr.btn_IngresarPro) {
                if (camposVacios() == true) {
                    agr.txt_idProducto.requestFocus(true);
                } else {
                    int idproducto = Integer.parseInt(agr.txt_idProducto.getText());
                    if (verificarProducto(idproducto) == true) {
                        int Cantidad = Integer.parseInt(agr.txt_cantidad.getText());
                        int stock = Integer.parseInt(agr.Lb_Stock.getText());
                        if (Cantidad == 0 || Cantidad < 0) {
                            agr.txt_cantidad.setText("");
                            agr.txt_cantidad.requestFocus(true);
                        } else if (Cantidad < stock || Cantidad == stock) {
                            pe.CalcularCantidad("-");
                            Ingresar();
                            Botones(false, true, true, true);
                            cargarProducto("", "");
                            sumaTotal();
                            Limpiar();
                        } else if (Cantidad > stock) {
                            JOptionPane.showMessageDialog(null, "Stock Insuficiente", "Error en Stock", JOptionPane.ERROR_MESSAGE);
                            agr.txt_cantidad.setText("");
                            agr.txt_cantidad.requestFocus(true);
                        }

                    } else {
                        agr.txt_idProducto.requestFocus(true);
                        JOptionPane.showMessageDialog(null, "El id del Producto no se encuentra en la Base de Datos", "Verificar ID_Producto", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } else if (agr.txt_cantidad.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Digite alguna Cantidad");
                agr.txt_cantidad.requestFocus(true);
            } else {
                agr.btn_IngresarPro.requestFocus(true);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == agr.txt_idProducto) {
            if (agr.txt_idProducto.getText().isEmpty()) {
                Limpiar();
                cargarProducto("", "");
            } else {
                String id = agr.txt_idProducto.getText();
                cargarProducto(id, "");
                buscarProducto(id);
                Stock(id);
                Botones(false, true, false, false);
            }

        } else if (e.getSource() == agr.txt_idVenta) {
            cargarTabla();
            sumaTotal();
            Botones(false, false, false, true);
            agr.btn_nuevo.setVisible(true);
            if (agr.txt_idVenta.getText().isEmpty()) {
                agr.btn_imprimir.setEnabled(false);
            } else {
                agr.btn_imprimir.setVisible(true);
                agr.btn_imprimir.setEnabled(true);
            }

        } else if (e.getSource() == agr.txt_Producto) {
            String prod = agr.txt_Producto.getText();
            cargarProducto("", prod);
        } else if (e.getSource() == agr.txt_proveedor) {
            String prov = agr.txt_proveedor.getText();
            cargarProducto("", prov);
        }
    }

    public void buscarProducto(String id) {
        int idpro = 0;
        if (id.isEmpty()) {
            idpro = 0;
        } else {
            idpro = Integer.parseInt(id);
        }
        String tabla = verificacionTabla();
        try {
            String prodAux = "";
            float precio = 0;
            String prov = "";
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto, Precio_Venta, Proveedor FROM " + tabla + " WHERE id = ? AND Estado = 'Activo'");
            ps.setInt(1, idpro);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodAux = (rs.getString("Producto"));
                precio = rs.getFloat("Precio_Venta");
                prov = rs.getString("Proveedor");
            }
            agr.txt_Producto.setText(prodAux);
            agr.txt_precio.setText("" + precio);
            agr.txt_proveedor.setText(prov);
            rs.close();
        } catch (Exception e) {
            System.out.println("error buscarProducto " + e.getMessage());
        }
    }

    public void AbrirPdf(int id) {
        String nombre = "Venta No._" + id;
        String mes = agr.txt_Mes.getText();
        try {
            String año = agr.txt_AÑO.getText();

            if (año.equals(agr.txt_AÑO.getText())) {
                String ruta = System.getProperty("user.home");
                String url = ruta + "\\OneDrive\\Escritorio\\Floreria_y_Limpieza\\ReciboVentas_" + año + "\\" + mes + "\\" + nombre + ".pdf";
                ProcessBuilder P = new ProcessBuilder();
                P.command("cmd.exe", "/c", url);
                P.start();
            }

        } catch (IOException ex) {
            Logger.getLogger(Compras.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se encontro el Archivo");
        }
    }

    public void CrearCapetaAuto() {
        String mes = agr.txt_Mes.getText();
        String año = agr.txt_AÑO.getText();
        String ruta = System.getProperty("user.home");
        String url = ruta + "\\OneDrive\\Escritorio\\Floreria_y_Limpieza\\ReciboVentas_" + año + "\\" + mes;
        File f = new File(url);
        if (f.mkdirs()) {
            // JOptionPane.showMessageDialog(null,"Se creo Carpeta");

        } else {
//            JOptionPane.showMessageDialog(null, "No se creo la carpeta correctamente");
//            System.out.println("No se Creo la carpeta");
        }
    }

    public void generarReciboPdf() {
        //      Rectangle tamaño = new Rectangle(200,400);
//            Document documento = new Document(tamaño;
        Document documento = new Document();
        String mes = agr.txt_Mes.getText();
        PreparedStatement ps;
        ResultSet rs;
        ResultSet rs2;
        Date day = new Date();
        DateFormat formateadorFechaMedia = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fecha = formateadorFechaMedia.format(day);
        String año = agr.txt_AÑO.getText();
        int id = Integer.parseInt(agr.txt_idVenta.getText());
        try {
            String nombre = "Venta No._" + id;
            String ruta = System.getProperty("user.home");
            PdfWriter.getInstance(documento, new FileOutputStream(ruta + "/OneDrive/Escritorio/Floreria_y_Limpieza/ReciboVentas_" + año + "/" + mes + "/" + nombre.trim() + ".pdf"));

            com.itextpdf.text.Image header = com.itextpdf.text.Image.getInstance(ruta + "\\OneDrive\\Documentos\\NetBeansProjects\\ProyectoFinal_floreria\\Floreria\\src\\Imag\\logo_floreria.png");
//            header.scaleToFit(200, 200);
            header.scaleToFit(450, 200);
            header.setAlignment(Chunk.ALIGN_CENTER);

            Paragraph parrafo = new Paragraph();
            parrafo.setAlignment(Paragraph.ALIGN_CENTER);
            parrafo.add("Venta No. " + id + "\n\n");
            parrafo.setFont(FontFactory.getFont("Comic Sans MS", 14, Font.BOLD, BaseColor.DARK_GRAY));

            documento.open();
            documento.add(header);
            documento.add(parrafo);

            int[] medidaCeldas = {200, 210, 200, 180, 230};
            PdfPTable tablaNum = new PdfPTable(5);
            Font fnP1 = new Font();
          //  fnP1.setSize(20);
            tablaNum.getDefaultCell().setBorder(0);
            tablaNum.getDefaultCell().setBackgroundColor(BaseColor.GREEN);
            tablaNum.addCell(new Paragraph("Cod.Prod", fnP1));
            tablaNum.addCell(new Paragraph("Descrip.", fnP1));
            tablaNum.addCell(new Paragraph("Cant.", fnP1));
            tablaNum.addCell(new Paragraph("P.Uni.", fnP1));
            tablaNum.addCell(new Paragraph("Importe", fnP1));
            tablaNum.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

            String cliente = "";
            String User = "";
            String fechaSQL = "";
            Float PagoCliente = 0f;

            try {
                String estado = "Activo";
                Connection cn = Conexion.establecerConnection();
                PreparedStatement pst = cn.prepareStatement(
                        "SELECT id_Producto, Producto,  Cantidad, Precio_Venta, Fecha, Nombre_Cliente, Usuario, Pago_Usuario FROM db_Ventas WHERE id_ventas = ? ");
                pst.setInt(1, id);
                rs = pst.executeQuery();
                Font fn = new Font();
             //   fn.setSize(20);
                if (rs.next()) {
                    do {

                        tablaNum.addCell(new Paragraph("" + rs.getInt(1), fn));
                        tablaNum.addCell(new Paragraph(rs.getString(2), fn));
                        tablaNum.addCell(new Paragraph("" + rs.getFloat(3), fn));
                        tablaNum.addCell(new Paragraph("" + rs.getInt(4), fn));
                        int importe1 = Integer.parseInt(rs.getString(3));
                        Float importe2 = Float.parseFloat(rs.getString(4));
                        Float importeTotal = importe1 * importe2;
                        tablaNum.addCell(new Paragraph("" + importeTotal, fn));
                        fechaSQL = rs.getString("Fecha");
                        cliente = rs.getString("Nombre_Cliente");
                        User = rs.getString("Usuario");
                        PagoCliente = rs.getFloat("Pago_Usuario");

                    } while (rs.next());
                    // tablaNum.setWidthPercentage(100);
                    documento.add(tablaNum);
                }
                rs.close();

            } catch (SQLException e) {
                System.err.print("Error en obtener en los Productos. " + e);
            }

            try {
                float cambio = 0f;
                float total = 0f;
                Paragraph parrafo2 = new Paragraph();
                parrafo2.setAlignment(Paragraph.ALIGN_CENTER);
                parrafo2.add("\n");

                PdfPTable tablaTotal = new PdfPTable(5);
                tablaTotal.getDefaultCell().setBorder(0);

                Connection cn2 = Conexion.establecerConnection();
                PreparedStatement pst2 = cn2.prepareStatement(
                        "SELECT SUM(Cantidad) AS CanTotal, SUM(Subtotal) AS ImporteTotal FROM db_Ventas WHERE id_ventas = ?");
                pst2.setInt(1, id);
                rs2 = pst2.executeQuery();
                Font fn = new Font();
               // fn.setSize(8);
                if (rs2.next()) {
                    do {
                        tablaTotal.addCell("     ");
                        tablaTotal.addCell("     ");
                        tablaTotal.addCell(new Paragraph("Total Arti. \n" + "     " + rs2.getInt("CanTotal"), fn));
                                             tablaTotal.addCell("     ");
                        tablaTotal.addCell(new Paragraph("Total a Pagar: \n" + "    $ " + rs2.getFloat("ImporteTotal"), fn));
                        total = rs2.getFloat("ImporteTotal")   ;
                    } while (rs2.next());
             //       fn.setSize(5);
                    Paragraph parrafo3 = new Paragraph();
                    parrafo3.setAlignment(Paragraph.ALIGN_CENTER);

                    parrafo3.add("\n\n Razón Social: " + cliente + "\n Usted pagó con: " + PagoCliente + "\n Su Cambio: " + (PagoCliente-total) + "\n Cajero: " + User
                            + "\n Fecha: " + fechaSQL);
                    documento.add(parrafo2);
                    // tablaTotal.setWidthPercentage(150);
                    documento.add(tablaTotal);
                    documento.add(parrafo3);
                }
                rs2.close();

            } catch (Exception e) {
                System.out.println("error tabla 2 " + e.getMessage());
            }

            documento.close();
            //    JOptionPane.showMessageDialog(null, "Reporte creado correctamente.");
            AbrirPdf(id);

        } catch (DocumentException | IOException e) {
            System.err.println("Error en PDF o ruta de imagen" + e);
            JOptionPane.showMessageDialog(null, "Error al generar PDF, contacte al administrador " + e.getMessage());
        }
    }

}
