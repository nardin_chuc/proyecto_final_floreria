/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.IngresarCliente;
import Interfaz.IngresarProveedor;
import Modelos.Tabla;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorIngrClientes implements ActionListener, MouseListener, KeyListener{
    
    IngresarCliente pes;

    public ControladorIngrClientes(IngresarCliente pes) {
        this.pes = pes;
        ActualizarId();
        this.pes.btn_Ingresar.addActionListener(this);
        this.pes.btn_Modificar.addActionListener(this);
        this.pes.btn_Nuevo.addActionListener(this);
        this.pes.tb_Permisionario.addMouseListener(this);
        this.pes.btn_Eliminar.addActionListener(this);
        this.pes.txt_Nombre.requestFocus(true);
        this.pes.tb_Permisionario.addMouseListener(this);
        this.pes.JmenuActivar.addActionListener(this);
        this.pes.JmenuDesactivar.addActionListener(this);
        this.pes.txt_Nombre.addKeyListener(this);
        this.pes.txt_apellido.addKeyListener(this);
        this.pes.txt_id.addKeyListener(this);
        cargarTabla();
        Botones(false, true, false);
       
    }

        @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == pes.btn_Ingresar){ //////******* BTN INGRESAR
            if(CamposVacios() == true){
            }else{
                if(VericarCliente("El Cliente se encuentra en la Base de Datos") == true){
                Ingresar();
                ActualizarId();
                Botones(false, true, true);
                Limpiar();
                }else{
                    
                }
            }
        }else if(e.getSource() == pes.btn_Modificar){//////////******* BTN MODIFICAR
            if(CamposVacios() == true){
                
            }else{
                Modificar();
            }
        }else if(e.getSource() == pes.JmenuActivar){/////////******** BTN ELIMINAR
            if(CamposVacios() == true){
            }else{
                Desactivar("Activo");
                ActualizarId();
                Botones(false, true, true);
                Limpiar();
            }
        }else if(e.getSource() == pes.JmenuDesactivar){ ///////////////////// ******* BTN CANCELAR
            if(CamposVacios() == true){
            }else{
                Desactivar("Inactivo");
                ActualizarId();
                Botones(false, true, true);
                Limpiar();
            }
            
        }else if(e.getSource() == pes.btn_Nuevo){
            Limpiar();
            ActualizarId();
            Botones(false, true, true);
        }else if(e.getSource() == pes.btn_Eliminar){
            if(CamposVacios() == true){
            }else{
            Eliminar();
            Limpiar();
            }
        }

    }
    
    public boolean CamposVacios() {
        if (pes.txt_Nombre.getText().isEmpty() || pes.txt_id.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Digite los Campos vacios", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }
    
    
    
     public boolean VericarCliente(String textosino){
        String cliente = pes.txt_Nombre.getText();
        String apellido = pes.txt_apellido.getText();
        String nombreSql="";
        String Cliente = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Nombre FROM db_Cliente WHERE Nombre = ? AND Apellido = ?");
            ps.setString(1, cliente);
            ps.setString(2, apellido);
            rs = ps.executeQuery();
            while (rs.next()) { 
                     Cliente = (rs.getString("Nombre"));
                     
                 }
            if (Cliente.isEmpty()) {
                return true;
            }else{
                JOptionPane.showMessageDialog(null, textosino);
                return false;
            }
        
        } catch (Exception e) {
           System.out.println(" Error: Verificar "+e.getMessage());
        }
        return false;
    }
     
     
     public void Ingresar() {
        int id = Integer.parseInt(pes.txt_id.getText());
        String Nombre = pes.txt_Nombre.getText();
        String apellido = pes.txt_apellido.getText();
        String telefono = pes.txt_telefono.getText();
        String Estado = "Activo";
        try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("INSERT INTO db_Cliente (id_Cliente, Nombre, Apellido, Telefono) VALUES (?,?,?,?)");
        ps.setInt(1, id);
        ps.setString(2, Nombre);
        ps.setString(3, apellido);
        ps.setString(4, telefono);
        ps.executeUpdate();
        cargarTabla();
        ps.close();
            JOptionPane.showMessageDialog(null, "Se Ingreso al cliente correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar "+e.getMessage());
        }
    }
     
     
     public void Modificar() {
        int id = Integer.parseInt(pes.txt_id.getText());
        String Nombre = pes.txt_Nombre.getText();
        String apellido = pes.txt_apellido.getText();
        String telefono = pes.txt_telefono.getText();
       try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("UPDATE db_Cliente SET Nombre = ?, Apellido = ?, Telefono = ? WHERE id_Cliente = ?");
        ps.setString(1, Nombre);
        ps.setString(2, apellido);
        ps.setString(3, telefono);
        ps.setInt(4, id);
        ps.executeUpdate();
        cargarTabla();
        JOptionPane.showMessageDialog(null, "Se Modifico al Cliente correctamente");
        ps.close();
        } catch (Exception e) {
            System.out.println("Error modifi: "+e.getMessage());
        }
    }
     
     
     public void Eliminar(){
         int id = Integer.parseInt(pes.txt_id.getText());
       try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("DELETE db_Cliente WHERE id_Cliente = ?");
        ps.setInt(1, id);
        ps.executeUpdate();
        cargarTabla();
        JOptionPane.showMessageDialog(null, "Se Elimino al Cliente correctamente");
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
        }
     }
     
     
     
     public void Desactivar(String Estado) {
        int id = Integer.parseInt(pes.txt_id.getText());

            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE db_Cliente SET Estado = ? WHERE id_Cliente = ?");
                ps.setString(1, Estado);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTabla();
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
    }
     
     
     public void cargarTabla() {
        String nombre = pes.txt_Nombre.getText();
        String apellido = pes.txt_apellido.getText();
        Tabla tableColor = new Tabla();
        int id = 0;
        if(pes.txt_id.getText().isEmpty()){
             id = 0;
        }else{
            id = Integer.parseInt(pes.txt_id.getText());
        }
         pes.tb_Permisionario.setDefaultRenderer(pes.tb_Permisionario.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT * FROM db_Cliente ORDER BY id_Cliente ASC";
            String sqlProv = "SELECT * FROM db_Cliente WHERE Nombre LIKE '%" + nombre + "%' and Apellido LIKE '%" + apellido+"%'" ;
            String sqlId = "SELECT * FROM db_Cliente WHERE id_Cliente = "+id;
            if (nombre.isEmpty() && id == 0) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!(nombre.isEmpty())) {
                ps = con.prepareStatement(sqlProv);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!(id == 0)) {
                ps = con.prepareStatement(sqlId);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
        } catch (Exception e) {
            System.err.println("Error en CARGARtabla: " + e.toString());
        }
    }
     
     
     
     public void ActualizarId(){
           int idAux = 0;
        try {
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT MAX(id_Cliente) AS id_Maximo FROM db_Cliente");
                 rs = ps.executeQuery();
                 while (rs.next()) {
                     idAux = (rs.getInt("id_Maximo")+1);                 
                 }
                 pes.txt_id.setText(""+idAux);
           } catch (Exception e) {
               System.err.println("error ActFolio: "+e.getMessage());
           }
       }
     
     
     public void Limpiar() {
        pes.txt_Nombre.setText("");
        pes.txt_apellido.setText("");
        pes.txt_telefono.setText("");
        pes.txt_estado.setText("");
        pes.txt_Nombre.requestFocus(true);
    }
     
     
     public void Botones(boolean ver, boolean ver2, boolean ver1){
        pes.btn_Eliminar.setVisible(ver);
        pes.btn_Modificar.setVisible(ver);
        pes.btn_Nuevo.setVisible(ver1);
        pes.btn_Modificar.setEnabled(ver);
        pes.btn_Ingresar.setEnabled(ver2);
        pes.btn_Nuevo.setEnabled(ver1);
        
    }
     
     
     
     //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
        try {
                int fila = pes.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(pes.tb_Permisionario.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id_Cliente, Nombre, Apellido, Telefono, Estado FROM db_Cliente WHERE id_Cliente = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pes.txt_id.setText(""+rs.getInt("id_Cliente"));
                    pes.txt_Nombre.setText(rs.getString("Nombre"));
                    pes.txt_apellido.setText(rs.getString("Apellido"));
                    pes.txt_telefono.setText(rs.getString("Telefono"));
                    pes.txt_estado.setText(rs.getString("Estado"));  
                }
                Botones(true, true, true);
                if (pes.txt_estado.getText().equals("Inactivo")) {
                    pes.JmenuActivar.setVisible(true);
                    pes.JmenuDesactivar.setVisible(false);
                } else if (pes.txt_estado.getText().equals("Activo")) {
                    pes.JmenuActivar.setVisible(false);
                    pes.JmenuDesactivar.setVisible(true);
                }                 
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            pes.lb_selc.setVisible(true);
            pes.lb_selc.setText("Haga Click en una fila para Modificar");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
         if (e.getSource() == pes.tb_Permisionario) {
            pes.lb_selc.setVisible(false);
        }
    }
    
        public void keyTyped(KeyEvent e) {

    }

    //*****EVENTO DEL TECLADO AL PONERSE EN EL TXT_ID, PARA PODER REALIZAR CIERTAS ACCIONES
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            
        }
    }

    //********** REALIZA UNA COSULTA EN LA BASE DE DATOS PARA LOCALIZAR LOS DATOS DEL PERMISIONARIO
    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == pes.txt_Nombre){
            cargarTabla();
        } else if(e.getSource() == pes.txt_apellido){
            cargarTabla();
        }else if (e.getSource() == pes.txt_id){
            cargarTabla();
        }
    }
    
}
