/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.Bienvenida;
import Interfaz.Compras;
import Interfaz.Ventas;
import Modelos.TbProducto;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
//import Interfaz.Verificacion;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorCompras implements ActionListener, MouseListener, KeyListener {

    Compras agr;
    ControladorPerecederos pe;

    public ControladorCompras(Compras agr) {
        this.agr = agr;
        pe = new ControladorPerecederos(this.agr);
        this.agr.btn_Eliminar.addActionListener(this);
        this.agr.btn_IngresarPro.addActionListener(this);
        this.agr.btn_modificar.addActionListener(this);
        this.agr.tb_Productos.addMouseListener(this);
        this.agr.tb_Compra.addMouseListener(this);
        this.agr.txt_idProducto.requestFocus(true);
        this.agr.btn_Eliminar.addMouseListener(this);
        this.agr.btn_IngresarPro.addMouseListener(this);
        this.agr.btn_modificar.addMouseListener(this);
        this.agr.txt_idProducto.addKeyListener(this);
        this.agr.txt_idVenta.addKeyListener(this);
        this.agr.btn_nuevo.addActionListener(this);
        this.agr.txt_Producto.addKeyListener(this);
        this.agr.txt_proveedor.addKeyListener(this);
        this.agr.btn_pagar.addActionListener(this);
        this.agr.cbx_tipoVenta.addActionListener(this);
        this.agr.btn_imprimir.addActionListener(this);
        //  colocarDatos();
        ActualizarId();
        cargarTabla("", "");
//        LLenarCBX();
        Botones(false, false, false, false);
        sumaTotal();
        agr.txt_idProducto.setEditable(true);
        agr.txt_cantidad.setEditable(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == agr.btn_IngresarPro) {
            if (camposVacios() == true) {
                agr.txt_idProducto.requestFocus(true);
            } else {
                int idproducto = Integer.parseInt(agr.txt_idProducto.getText());
                if (verificarProducto(idproducto) == true) {
                    registrarPerecederos();
                    Ingresar();
                    cargarTabla("", "");
                    cargarCompras();
                    sumaTotal();
                    Limpiar(true);
                    agr.btn_pagar.setEnabled(true);
                    agr.btn_imprimir.setVisible(false);
                } else {
                    agr.txt_idProducto.requestFocus(true);
                    JOptionPane.showMessageDialog(null, "El id del Producto no se encuentra en la Base de Datos", "Verificar ID_Producto", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (e.getSource() == agr.btn_nuevo) {
            Limpiar(true);
            ActualizarId();
            cargarCompras();
            Botones(false, true, false, false);
            buscarUser(agr.txt_user.getText());
            agr.txt_idProducto.setEditable(true);
            agr.txt_cantidad.setEditable(true);
        } else if (e.getSource() == agr.btn_pagar) {
            generarReciboPdf();
            pagar();
            ActualizarId();
            Limpiar(true);
            cargarCompras();
            sumaTotal();
        } else if (e.getSource() == agr.cbx_tipoVenta) {
            cargarTabla("", "");
            Limpiar(true);
        } else if (e.getSource() == agr.btn_imprimir) {
            int id = Integer.parseInt(agr.txt_idVenta.getText());
            if( verificarIdCompras() ==true){
                CrearCapetaAuto();
                generarReciboPdf();
            }else{
            }
        }
    }

    public void AccionarModificacion(Compras vn) {
        if (camposVacios() == true) {
            vn.txt_idProducto.requestFocus(true);
        } else {
            int Cantidad = Integer.parseInt(vn.txt_cantidad.getText());
            int stock = Integer.parseInt(vn.Lb_Stock.getText());
            if (Cantidad == 0) {
                vn.txt_cantidad.setText("");
                vn.txt_cantidad.requestFocus(true);
            } else {
                Modificar();
                cargarCompras();
                cargarTabla(vn.txt_idProducto.getText(), "");
                pe.ModificarPerecedero();
                Limpiar(true);
                sumaTotal();
            }
        }
    }

    public String verificacionTabla() {
        String tipoVenta = agr.cbx_tipoVenta.getSelectedItem().toString();
        String tablaSQL = "";
        if (tipoVenta.equals("FLORERIA")) {
            tablaSQL = "db_Floreria_Productos";
        } else if (tipoVenta.equals("PRODUCTOS DE LIMPIEZA")) {
            tablaSQL = "db_ProductosLimpieza";
        }
        return tablaSQL;
    }

    public void colocarDatos() {
        Bienvenida bv = new Bienvenida();
        String user = bv.txt_user.getText();
        String cargo = bv.txt_cargo.getText();
        System.out.println("user " + user);
        agr.txt_user.setText(user);
        agr.txt_cargo.setText(cargo);
    }

    public boolean camposVacios() {
        if (agr.txt_idProducto.getText().isEmpty() || agr.txt_Producto.getText().isEmpty() || agr.txt_cantidad.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos Vacios ", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    public boolean verificarProducto(int idProd) {
        try {
            String tabla = verificacionTabla();
            String prodAux = "";
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto FROM " + tabla + " WHERE id = ?");
            ps.setInt(1, idProd);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodAux = (rs.getString("Producto"));
            }
            if (prodAux.isEmpty()) {
                agr.txt_Producto.setText(prodAux);
                return false;
            } else {
                agr.txt_Producto.setText(prodAux);
                return true;
            }
        } catch (Exception e) {
            System.out.println("error Verificar prod " + e.getMessage());
        }
        return false;
    }

    public void Ingresar() {
        int idVentas = Integer.parseInt(agr.txt_idVenta.getText());
        String Año = agr.txt_AÑO.getText();
        String MES = agr.txt_Mes.getText();
        String FECHA = agr.txt_fecha.getText();
        int idProducto = Integer.parseInt(agr.txt_idProducto.getText());
        String Producto = agr.txt_Producto.getText();
        float precio_venta = Float.parseFloat(agr.txt_precioCompra.getText());
        int cantidad = Integer.parseInt(agr.txt_cantidad.getText());
        String user = agr.txt_user.getText();
        int idUsuario = buscarUser(user);
        String Proveedor = agr.txt_proveedor.getText();
        int idProveedor = buscarProveedor(Proveedor);
        float subtotal = cantidad * precio_venta;
        int id_pere = 0;
        float total = subtotal + ((subtotal * 16) / 100);
        if (agr.cbx_tipoVenta.getSelectedItem().toString().equals("FLORERIA")) {
            id_pere = (ActualizarIdPerecederos() - 1);
        } else {
            id_pere = 0;
        }
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_Compras ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?");
            ps.setInt(1, idVentas);
            ps.setString(2, Año);
            ps.setString(3, MES);
            ps.setString(4, FECHA);
            ps.setString(5, Producto);
            ps.setInt(6, idProducto);
            ps.setInt(7, cantidad);
            ps.setString(8, user);
            ps.setInt(9, idUsuario);
            ps.setString(10, Proveedor);
            ps.setInt(11, idProveedor);
            ps.setFloat(12, precio_venta);
            ps.setFloat(13, subtotal);
            ps.setFloat(14, id_pere);
            ps.executeUpdate();
            String tabla = verificacionTabla();
            Connection conectar2 = Conexion.establecerConnection();
            PreparedStatement ps2 = conectar2.prepareStatement("UPDATE " + tabla + " SET Cantidad = Cantidad + ? WHERE Producto = ?");
            ps2.setInt(1, cantidad);
            ps2.setString(2, Producto);
            ps2.executeQuery();
            cargarTabla("", "");
            JOptionPane.showMessageDialog(null, "Se Ingreso la Compra correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void Modificar() {
        int idVentas = Integer.parseInt(agr.txt_idVenta.getText());
        String Año = agr.txt_AÑO.getText();
        String MES = agr.txt_Mes.getText();
        String FECHA = agr.txt_fecha.getText();
        int idProducto = Integer.parseInt(agr.txt_idProducto.getText());
        String Producto = agr.txt_Producto.getText();
        float precio_venta = Float.parseFloat(agr.txt_precioCompra.getText());
        int cantidad = Integer.parseInt(agr.txt_cantidad.getText());
        int cantidaAux = Integer.parseInt(agr.txt_AuxCant.getText());
        String user = agr.txt_user.getText();
        int idUsuario = buscarUser(user);
        String Proveedor = agr.txt_proveedor.getText();
        int idProveedor = buscarProveedor(Proveedor);
        float subtotal = cantidad * precio_venta;
        float total = subtotal + ((subtotal * 16) / 100);
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_ModificarCompras ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?");
            ps.setInt(1, idVentas);
            ps.setString(2, Año);
            ps.setString(3, MES);
            ps.setString(4, FECHA);
            ps.setString(5, Producto);
            ps.setInt(6, idProducto);
            ps.setInt(7, cantidad);
            ps.setInt(8, cantidaAux);
            ps.setString(9, user);
            ps.setInt(10, idUsuario);
            ps.setString(11, Proveedor);
            ps.setInt(12, idProveedor);
            ps.setFloat(13, precio_venta);
            ps.setFloat(14, subtotal);
            ps.executeUpdate();

            actualizarCantidadStock(cantidaAux, Producto, "-");
            actualizarCantidadStock(cantidad, Producto, "+");
            cargarTabla("", "");
            JOptionPane.showMessageDialog(null, "Se Modifico la Compra correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void EliminarRegistro(Compras cm) {
        if (cm.txt_op.getText().equals("SI")) {
            pe.EliminarPerecedero();
            int idProducto = Integer.parseInt(cm.txt_idProducto.getText());
            int idVenta = Integer.parseInt(cm.txt_idVenta.getText());
            int Cantidad = Integer.parseInt(cm.txt_cantidad.getText());
            String Producto = agr.txt_Producto.getText();
            String tabla = buscarProductoTabla(Producto, "db_Floreria_Productos");
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("EXECUTE SP_EliminarCompra ?,?,?");
                ps.setString(1, Producto);
                ps.setInt(2, Cantidad);
                ps.setInt(3, idVenta);
                ps.executeUpdate();
                ps.close();
                actualizarCantidadStock(Cantidad, Producto, "-");
                Limpiar(true);
                cargarTabla("", "");
                cargarCompras();

                JOptionPane.showMessageDialog(null, "Se Elimino el registro seleccionado correctamente");
            } catch (Exception e) {
                System.out.println("error elimnar reg: " + e.getMessage());
            }
        } else {

        }

    }

    public void actualizarCantidadStock(int cantidad, String Producto, String signo) {
        try {
            String tabla = buscarProductoTabla(Producto, "db_Floreria_Productos");
            ResultSet rs;
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + tabla + " SET Cantidad = Cantidad " + signo + " ? WHERE Producto = ?");
            ps.setInt(1, cantidad);
            ps.setString(2, Producto);
            rs = ps.executeQuery();
            rs.close();
        } catch (Exception e) {
            System.out.println("error en actualizar stock " + e.getMessage());
        }
    }

    public String buscarProductoTabla(String Producto, String table) {
        String tablaEncotrada = "";
        try {
            String prodAux = "";
            ResultSet rs;
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto FROM " + table + " WHERE Producto = ?");
            ps.setString(1, Producto);
            rs = ps.executeQuery();

            while (rs.next()) {
                prodAux = rs.getString("Producto");
            }
            if (prodAux.isEmpty()) {
                tablaEncotrada = buscarProductoTabla(Producto, "db_ProductosLimpieza");

            } else {
                return table;
            }
        } catch (Exception e) {
            System.out.println("error buscar Producto tabla " + e.getMessage());
        }
        return tablaEncotrada;
    }

    public void sumaTotal() {
        float subtotal = 0;
        float total = 0;
        int idVenta;
        if (agr.txt_idVenta.getText().isEmpty()) {
            idVenta = 0;
        } else {
            idVenta = Integer.parseInt(agr.txt_idVenta.getText());
        }
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT SUM(Total_Compra) AS total FROM db_Compras WHERE id_compra = ?");
            ps.setInt(1, idVenta);
            rs = ps.executeQuery();
            while (rs.next()) {
                total = (rs.getInt("total"));
            }
            agr.TxtTOTAL.setText("" + total);
        } catch (Exception e) {
            System.out.println("error en Buscar User " + e.getMessage());
        }
    }

    public int buscarUser(String user) {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT id_Usuario  FROM db_Usuarios WHERE Usuario = ?");
            ps.setString(1, user);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Usuario"));

            }
            return idAux;
        } catch (Exception e) {
            System.out.println("error en Buscar User " + idAux);
        }
        return idAux;
    }

    public int buscarProveedor(String Prov) {
        String tabla = verificacionTabla();
        String tablaProv = "";
        if (tabla.equals("db_Floreria_Productos")) {
            tablaProv = "db_Floreria_Proveedor";
        } else if (tabla.equals("db_ProductosLimpieza")) {
            tablaProv = "db_ProductosLimpieza_Proveedor";
        }
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT id_Proveedor  FROM " + tablaProv + " WHERE Proveedor = ?");
            ps.setString(1, Prov);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Proveedor"));

            }
            return idAux;
        } catch (Exception e) {
            System.out.println("error en Buscar Proveedor " + idAux);
        }
        return idAux;
    }

    public void pagar() {
        int idVenta = Integer.parseInt(agr.txt_idVenta.getText());
        String estado = "Pagado";
        String tabla = verificacionTabla();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE db_Compras SET Estado = ? WHERE id_compra = ?");
            ps.setString(1, estado);
            ps.setInt(2, idVenta);
            ps.executeUpdate();
            cargarTabla("", "");
            cargarCompras();
            JOptionPane.showMessageDialog(null, "SE CONCLUYO LA COMPRA");
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }
    }

    public void ActualizarId() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id_compra) AS id_Maximo FROM db_Compras");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            agr.txt_idVenta.setText("" + idAux);
        } catch (Exception e) {
        }
    }

    public void Botones(boolean ver, boolean ver2, boolean ver3, boolean ver4) {
        agr.btn_Eliminar.setVisible(ver);
        agr.btn_modificar.setVisible(ver3);
        agr.btn_IngresarPro.setEnabled(ver2);
        agr.btn_nuevo.setVisible(ver4);
    }

    public void cargarTabla(String idProd, String info) {
        String tabla = verificacionTabla();
        TbProducto tableColor = new TbProducto();
        agr.tb_Productos.setDefaultRenderer(agr.tb_Productos.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) agr.tb_Productos.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;

        int id = 0;
        if (idProd.isEmpty()) {
            id = 0;
        } else {
            id = Integer.parseInt(idProd);
        }

        int columnas;
        int[] ancho = {90, 150, 90, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            agr.tb_Productos.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sqlID = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM " + tabla + " WHERE id = ? AND Estado = 'Activo'";
            String vacio = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM " + tabla + " WHERE Estado = 'Activo' ORDER BY id";
            String dato = "SELECT id, Producto, Cantidad, Precio_Compra, Proveedor FROM " + tabla + " WHERE Producto LIKE '%" + info + "%' AND Estado = 'Activo' "
                    + "OR Proveedor LIKE '%" + info + "%'";

            if ("".equalsIgnoreCase(idProd) && "".equalsIgnoreCase(info)) {
                ps = con.prepareStatement(vacio);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!("".equalsIgnoreCase(info))) {
                ps = con.prepareStatement(dato);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (!("".equalsIgnoreCase(idProd)) && ("".equalsIgnoreCase(info))) {
                ps = con.prepareStatement(sqlID);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
        } catch (Exception e) {
            System.err.println("Error en tabla CargarProd: " + e.toString());
        }
    }

    public void cargarCompras() {
        DefaultTableModel modeloTabla = (DefaultTableModel) agr.tb_Compra.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int id = 0;
        if (agr.txt_idVenta.getText().isEmpty()) {
            id = 0;
        } else {
            id = Integer.parseInt(agr.txt_idVenta.getText());
        }
        int columnas;
        int[] ancho = {90, 150, 90, 150, 150, 100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            agr.tb_Compra.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_compra, Producto, Cantidad, Precio_Compra, Total_Compra, Estado FROM db_Compras WHERE id_compra = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla CargarComprasprod: " + e.toString());
        }
    }

    //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        String tabla = verificacionTabla();
        if (e.getSource() == agr.tb_Productos) {
            try {
                int fila = agr.tb_Productos.getSelectedRow();
                int id = Integer.parseInt(agr.tb_Productos.getValueAt(fila, 0).toString());
                String Pro = agr.tb_Productos.getValueAt(fila, 1).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Producto,  Precio_Compra, Proveedor FROM " + tabla + " WHERE id = ? AND Producto = ?");
                ps.setInt(1, id);
                ps.setString(2, Pro);
                rs = ps.executeQuery();
                while (rs.next()) {
                    agr.txt_idProducto.setText("" + rs.getInt("id"));
                    agr.txt_Producto.setText(rs.getString("Producto"));
                    agr.txt_precioCompra.setText("" + rs.getFloat("Precio_Compra"));
                    agr.txt_proveedor.setText(rs.getString("Proveedor"));
//                    agr.txt_cliente.setText(rs.getString("Nombre_Cliente"));

                }
//                if( agr.txt_estado.getText().equals("Inactivo")){
//                    agr.btn_Desactivar.setText("Reingresar");
//                }else if( agr.txt_estado.getText().equals("Activo")){
//                    agr.btn_Desactivar.setText("Desactivar");
//                }                 
                Botones(false, true, false, false);
                agr.txt_cantidad.requestFocus(true);
                Stock(agr.txt_idProducto.getText());
                agr.lb_selc.setVisible(false);
                agr.btn_pagar.setEnabled(false);
                agr.btn_imprimir.setVisible(false);
            } catch (Exception er) {
                System.err.println("Error en tabla prod mouse: " + er.toString());
            }
        } else if (e.getSource() == agr.tb_Compra) {
            String idprod = "";
            String nom = "";
            String Nombretabla = "";
            String FechaSQL = "";
            try {
                int fila = agr.tb_Compra.getSelectedRow();
                int id = Integer.parseInt(agr.tb_Compra.getValueAt(fila, 0).toString());
                String precioAux = "";
                String provAux = "";
                String proAux = "";
                String idPro = "";
                String Cantidad = "";
                String Pro = agr.tb_Compra.getValueAt(fila, 1).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT  Producto, id_Producto,  Cantidad, Precio_Compra, Proveedor, id_Perecedero, Fecha FROM db_Compras WHERE id_compra = ? AND Producto = ?");
                ps.setInt(1, id);
                ps.setString(2, Pro);
                rs = ps.executeQuery();
                while (rs.next()) {
                    proAux = (rs.getString("Producto"));
                    idPro = "" + rs.getInt("id_Producto");
                    Cantidad = "" + rs.getInt("Cantidad");
                    agr.txt_AuxCant.setText("" + rs.getInt("Cantidad"));
                    precioAux = rs.getString("Precio_Compra");
                    provAux = rs.getString("Proveedor");
                    agr.txt_idPere.setText(rs.getString("id_Perecedero"));
                    this.agr.txt_precioCompra.setText(rs.getString("Precio_Compra"));
                    this.agr.txt_proveedor.setText(rs.getString("Proveedor"));
                    FechaSQL = rs.getString("Fecha");
                }

                Nombretabla = buscarProductoTabla(proAux, "db_Floreria_Productos");

                Botones(verifecha(FechaSQL), false, verifecha(FechaSQL), true);
                agr.lb_selc2.setVisible(false);

                buscarCBX(Nombretabla);
                cargarTabla(idPro, proAux);
                agr.txt_precioCompra.setText(precioAux);
                agr.txt_proveedor.setText(provAux);
                agr.txt_Producto.setText(proAux);
                agr.txt_idProducto.setText(idPro);
                Stock(agr.txt_idProducto.getText());
                agr.txt_cantidad.setText(Cantidad);
                agr.btn_imprimir.setVisible(true);
            } catch (Exception er) {
                System.err.println("Error en tabla prod mouse: " + er.toString());
            }

        }
    }

    private boolean verifecha(String fecha) {
        LocalDateTime dia = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        String hoy = dia.format(formato);
        System.out.println();
        if (fecha.equals(hoy)) {
            agr.txt_idProducto.setEditable(true);
            agr.txt_cantidad.setEditable(true);
            return true;
        } else {
            agr.txt_idProducto.setEditable(false);
            agr.txt_cantidad.setEditable(false);
            return false;
        }
    }

    public void buscarCBX(String tabla) {

        try {
            if (tabla.equals("db_Floreria_Productos")) {
                agr.cbx_tipoVenta.setSelectedItem("FLORERIA");
                cargarTabla("", "");
                agr.txt_proveedor.setText("Cd");
            } else if (tabla.equals("db_ProductosLimpieza")) {
                agr.cbx_tipoVenta.setSelectedItem("PRODUCTOS DE LIMPIEZA");
                cargarTabla("", "");
            }

        } catch (Exception e) {
            System.out.println("precio " + e.getMessage());
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.black);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.black);
        } else if (e.getSource() == agr.tb_Productos) {
            agr.lb_selc.setVisible(true);
            agr.lb_selc.setText("Haga Click en una fila para Elegir Producto");
        } else if (e.getSource() == agr.tb_Compra) {
            agr.lb_selc2.setVisible(true);
            agr.lb_selc2.setText("Haga Click en una fila para Modificar");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == agr.btn_Eliminar) {
            agr.btn_Eliminar.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_IngresarPro) {
            agr.btn_IngresarPro.setBackground(Color.red);
        } else if (e.getSource() == agr.btn_modificar) {
            agr.btn_modificar.setBackground(Color.red);
        } else if (e.getSource() == agr.tb_Productos) {
            agr.lb_selc.setVisible(false);
        } else if (e.getSource() == agr.tb_Compra) {
            agr.lb_selc2.setVisible(false);
        }

    }

    public void Stock(String id) {
        String tabla = verificacionTabla();
        int stock = 0;
        int idP = 0;

        if (id.isEmpty()) {
            idP = 0;
        } else {
            idP = Integer.parseInt(agr.txt_idProducto.getText());
        }
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Cantidad FROM " + tabla + " WHERE id = ?");
            ps.setInt(1, idP);
            rs = ps.executeQuery();
            while (rs.next()) {
                stock = (rs.getInt("Cantidad"));
            }
            agr.Lb_Stock.setText("" + stock);
        } catch (Exception e) {
            System.out.println("error buscarProducto " + e.getMessage());
        }
    }

    public void Limpiar(boolean ver) {
        agr.txt_Producto.setText("");
        agr.txt_precioCompra.setText("");
        agr.txt_idProducto.setText("");
        agr.txt_cantidad.setText("");
        agr.Lb_Stock.setText("");
        agr.txt_proveedor.setText("");
        agr.txt_idProducto.requestFocus(ver);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == agr.txt_idProducto) {
                if (agr.txt_Producto.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Producto No Encontrado", "ID no Encontrado", JOptionPane.ERROR_MESSAGE);
                    agr.txt_idProducto.requestFocus(true);
                    agr.btn_imprimir.setVisible(false);
                } else {
                    agr.txt_cantidad.requestFocus(true);
                    agr.btn_imprimir.setVisible(false);
                }
            } else if (agr.txt_cantidad.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Digite alguna Cantidad");
                agr.txt_cantidad.requestFocus(true);
            } else {
                agr.btn_IngresarPro.requestFocus(true);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == agr.txt_idProducto) {
            if (agr.txt_idProducto.getText().isEmpty()) {
                Limpiar(true);
                cargarTabla("", "");
            } else {
                String id = agr.txt_idProducto.getText();
                cargarTabla(id, "");
                cargarCompras();
                buscarProducto(id);
                Stock(id);
                Botones(false, true, false, false);
                agr.btn_imprimir.setVisible(false);
            }

        } else if (e.getSource() == agr.txt_idVenta) {
            cargarCompras();
            sumaTotal();
            Botones(false, false, false, true);
            agr.txt_Producto.setText("");
            agr.txt_precioCompra.setText("");
            agr.txt_idProducto.setText("");
            agr.txt_cantidad.setText("");
            agr.Lb_Stock.setText("");
            agr.txt_proveedor.setText("");
            if (agr.txt_idVenta.getText().isEmpty()) {
                agr.btn_imprimir.setEnabled(false);
            } else {
                agr.btn_imprimir.setEnabled(true);
                agr.btn_imprimir.setVisible(true);
            }

        } else if (e.getSource() == agr.txt_Producto) {
            String prod = agr.txt_Producto.getText();
            cargarTabla("", prod);
        } else if (e.getSource() == agr.txt_proveedor) {
            String prov = agr.txt_proveedor.getText();
            cargarTabla("", prov);
        }
    }

    public void buscarProducto(String id) {
        String tabla = verificacionTabla();
        int idpro = 0;
        if (id.isEmpty()) {
            idpro = 0;
        } else {
            idpro = Integer.parseInt(id);
        }
        try {
            String prodAux = "";
            float precio = 0;
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto, Precio_Venta FROM " + tabla + " WHERE id = ? AND Estado = 'Activo'");
            ps.setInt(1, idpro);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodAux = (rs.getString("Producto"));
                precio = rs.getFloat("Precio_Venta");
            }
            agr.txt_Producto.setText(prodAux);
            agr.txt_precioCompra.setText("" + precio);
        } catch (Exception e) {
            System.out.println("error buscarProducto " + e.getMessage());
        }
    }

    public boolean verificarIdCompras() {
        int id = Integer.parseInt(agr.txt_idVenta.getText());
        String idCompra = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Producto FROM db_Compras WHERE id_compra = ? AND Estado = 'Pagado'");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                idCompra = (rs.getString("Producto"));
            }
            if (idCompra.isEmpty()) {
                JOptionPane.showMessageDialog(null, "No se encuentra en la Base de Datos");
                return false;
            } else if (!(idCompra.isEmpty())) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("error buscarProducto " + e.getMessage());
        }return false;
    }

    public void generarReciboPdf() {
        String tabla = verificacionTabla();
        String mes = agr.txt_Mes.getText();
        Document documento = new Document();
        PreparedStatement ps;
        ResultSet rs;
        ResultSet rs2;
        Date day = new Date();
        DateFormat formateadorFechaMedia = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fecha = formateadorFechaMedia.format(day);
        String año = agr.txt_AÑO.getText();
        int id = Integer.parseInt(agr.txt_idVenta.getText());
        try {
            String nombre = "Compra No._" + id;
            String ruta = System.getProperty("user.home");
            PdfWriter.getInstance(documento, new FileOutputStream(ruta + "/OneDrive/Escritorio/Floreria_y_Limpieza/ReciboCompras_" + año + "/" + mes + "/" + nombre.trim() + ".pdf"));

            com.itextpdf.text.Image header = com.itextpdf.text.Image.getInstance(ruta + "\\OneDrive\\Documentos\\NetBeansProjects\\ProyectoFinal_floreria\\Floreria\\src\\Imag\\logo_floreria.png");
            header.scaleToFit(450, 200);
            header.setAlignment(Chunk.ALIGN_CENTER);

            Paragraph parrafo = new Paragraph();
            parrafo.setAlignment(Paragraph.ALIGN_CENTER);
            parrafo.add("Compra No. " + id + "\n\n");
            parrafo.setFont(FontFactory.getFont("Comic Sans MS", 14, Font.BOLD, BaseColor.DARK_GRAY));

            documento.open();
            documento.add(header);
            documento.add(parrafo);

            PdfPTable tablaNum = new PdfPTable(5);
            tablaNum.getDefaultCell().setBorder(0);
            tablaNum.getDefaultCell().setBackgroundColor(BaseColor.CYAN);
            tablaNum.addCell("Cod. Produ.");
            tablaNum.addCell("Descrip.");
            tablaNum.addCell("Cant.");
            tablaNum.addCell("P. Uni.");
            tablaNum.addCell("Importe");
            tablaNum.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
            String user = "";
            String provs = "";
            String fechaSQL = "";
            try {
                String estado = "Activo";
                Connection cn = Conexion.establecerConnection();
                PreparedStatement pst = cn.prepareStatement(
                        "SELECT id_Producto, Producto,  Cantidad, Precio_Compra, Proveedor, Usuario, Fecha FROM db_Compras WHERE id_compra = ? AND Estado = 'Pagado'");
                pst.setInt(1, id);
                rs = pst.executeQuery();
                if (rs.next()) {
                    do{
                        tablaNum.addCell("" + rs.getInt(1));
                    tablaNum.addCell(rs.getString(2));
                    tablaNum.addCell("" + rs.getInt(3));
                    tablaNum.addCell("" + rs.getFloat(4));
                    int importe1 = Integer.parseInt(rs.getString(3));
                    Float importe2 = Float.parseFloat(rs.getString(4));
                    Float importeTotal = importe1 * importe2;
                    tablaNum.addCell("" + importeTotal);
                    provs = provs + " - " + rs.getString("Proveedor");
                    user = rs.getString("Usuario");
                    fechaSQL = rs.getString("Fecha");
                    }while(rs.next());
                    documento.add(tablaNum);
                    rs.close();
                }
                
            } catch (SQLException e) {
                System.err.print("Error en obtener en los Productos. " + e);
            }

            try {
                Paragraph parrafo2 = new Paragraph();
                parrafo2.setAlignment(Paragraph.ALIGN_CENTER);
                parrafo2.add("\n");

                PdfPTable tablaTotal = new PdfPTable(5);
                tablaTotal.getDefaultCell().setBorder(0);

                Connection cn2 = Conexion.establecerConnection();
                PreparedStatement pst2 = cn2.prepareStatement(
                        "SELECT SUM(Cantidad) AS CanTotal, SUM(Total_Compra) AS ImporteTotal FROM db_Compras WHERE id_compra = ?");
                pst2.setInt(1, id);
                rs2 = pst2.executeQuery();

                if (rs2.next()) {
                    do {
                        tablaTotal.addCell("");
                        tablaTotal.addCell("");
                        tablaTotal.addCell("Total Arti: \n" + "     "+rs2.getInt(1));
                        tablaTotal.addCell("");
                        tablaTotal.addCell("Total a Pagar: \n" + "    $ "+rs2.getFloat(2));
                        

                    } while (rs2.next());
                    Paragraph parrafo3 = new Paragraph();
                    parrafo3.setAlignment(Paragraph.ALIGN_CENTER);
                    parrafo3.add("\n\n Proveedor: \n" + provs + "\n Usuario: " + user + "\n Fecha: " + fechaSQL);

                    documento.add(parrafo2);
                    documento.add(tablaTotal);
                    documento.add(parrafo3);

                }
            } catch (Exception e) {
                System.out.println("error tabla 2 " + e.getMessage());
            }

            documento.close();
            AbrirPdf(id);

        } catch (DocumentException | IOException e) {
            System.err.println("Error en PDF o ruta de imagen" + e);
            JOptionPane.showMessageDialog(null, "Error al generar PDF, contacte al administrador " + e.getMessage());
        }
    }

    public void AbrirPdf(int id) {
        String mes = agr.txt_Mes.getText();
        String nombre = "Compra No._" + id;
        try {
            String año = agr.txt_AÑO.getText();

            if (año.equals(agr.txt_AÑO.getText())) {
                String ruta = System.getProperty("user.home");
                String url = ruta + "\\OneDrive\\Escritorio\\Floreria_y_Limpieza\\ReciboCompras_" + año + "\\" + mes + "\\" + nombre + ".pdf";
                ProcessBuilder P = new ProcessBuilder();
                P.command("cmd.exe", "/c", url);
                P.start();
            }

        } catch (IOException ex) {
            Logger.getLogger(Compras.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se encontro el Archivo");
        }
    }

    public void CrearCapetaAuto() {
        String mes = agr.txt_Mes.getText();
        String año = agr.txt_AÑO.getText();
        String ruta = System.getProperty("user.home");
        String url = ruta + "\\OneDrive\\Escritorio\\Floreria_y_Limpieza\\ReciboCompras_" + año + "\\" + mes;
        File f = new File(url);
        if (f.mkdirs()) {
            // JOptionPane.showMessageDialog(null,"Se creo Carpeta");

        } else {
//            JOptionPane.showMessageDialog(null, "No se creo la carpeta correctamente");
//            System.out.println("No se Creo la carpeta");
        }
    }

    // ESTA PARTE NOS SERVIRA PARA EL REGISTRO DE PERECEDEROS
    public int ActualizarIdPerecederos() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id_perecedero) AS id_Maximo FROM db_Perecederos");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            rs.close();
        } catch (Exception e) {
        }
        return idAux;
    }

    public void registrarPerecederos() {
        String giro = agr.cbx_tipoVenta.getSelectedItem().toString();
        if (giro.equals("FLORERIA")) {
            int idPerece = ActualizarIdPerecederos();
            String Año = agr.txt_AÑO.getText();
            String MES = agr.txt_Mes.getText();
            String FECHA = agr.txt_fecha.getText();
            String Producto = agr.txt_Producto.getText();
            int cantidad = Integer.parseInt(agr.txt_cantidad.getText());
            String FechaVence = CalcularDiaVencimiento(FECHA);
            int DiasFaltantes = DiasRestantes(FechaVence);
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("INSERT INTO db_Perecederos (Año, MES, id_perecedero, "
                        + "Producto, Cantidad, FechaRegistro, FechaVencimiento, DiasPendientes) VALUES (?,?,?,?,?,?,?,?)");
                ps.setString(1, Año);
                ps.setString(2, MES);
                ps.setInt(3, idPerece);
                ps.setString(4, Producto);
                ps.setInt(5, cantidad);
                ps.setString(6, FECHA);
                ps.setString(7, FechaVence);
                ps.setInt(8, DiasFaltantes);
                ps.executeUpdate();
                ps.close();
//                String tabla = verificacionTabla();
//                Connection conectar2 = Conexion.establecerConnection();
//                PreparedStatement ps2 = conectar2.prepareStatement("UPDATE " + tabla + " SET Cantidad = Cantidad + ? WHERE Producto = ?");
//                ps2.setInt(1, cantidad);
//                ps2.setString(2, Producto);
//                ps2.executeQuery();
//                cargarTabla("", "");
            } catch (Exception e) {
                System.out.println("error ingresarPerecedero " + e.getMessage());
            }
        } else {

        }
    }

    public String CalcularDiaVencimiento(String fechaRegistro) {
        String Vencimiento = "";
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        LocalDate fecha = LocalDate.parse(fechaRegistro, formato);
        fecha = fecha.plusDays(10);
        Vencimiento = fecha.format(formato);
        return Vencimiento;
    }

    public int DiasRestantes(String fechaVencimiento) {           //CALCULA LOS DIAS QUE FALTAN PARA EL EVENTO
        String fecha_hoy = agr.txt_fecha.getText();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        int diasf = 0;
        try {
            Date date1 = dateFormat.parse(fecha_hoy);
            Date date2 = dateFormat.parse(fechaVencimiento);
            long tiempoTrans = date2.getTime() - date1.getTime();
            TimeUnit unidad = TimeUnit.DAYS;
            TimeUnit unidad2 = TimeUnit.DAYS;
            long tiempoTrans2 = (date2.getTime() - date1.getTime());
            long diasFaltantes = unidad.convert(tiempoTrans, TimeUnit.MILLISECONDS);
            diasf = Integer.parseInt("" + diasFaltantes);
        } catch (Exception e) {
            System.out.println("erro en calcular dias: " + e.getMessage());
        }
        return diasf;
    }

}/////// FIN DE LA CLASE CONTROLADOR COMPRAS
