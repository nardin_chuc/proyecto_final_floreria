/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.IngresarCargo;
import Modelos.Tabla;
import Modelos.TbProducto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorIngresarCargos implements ActionListener, MouseListener, KeyListener {

    IngresarCargo pes;

    public ControladorIngresarCargos(IngresarCargo pes) {
        this.pes = pes;
        ActualizarId();
        this.pes.btn_Ingresar.addActionListener(this);
        this.pes.btn_Modificar.addActionListener(this);
        this.pes.btn_Nuevo.addActionListener(this);
        this.pes.tb_Permisionario.addMouseListener(this);
        this.pes.btn_Eliminar.addActionListener(this);
        this.pes.txt_Nombre.requestFocus(true);
        this.pes.tb_Permisionario.addMouseListener(this);
        this.pes.JmenuActivar.addActionListener(this);
        this.pes.JmenuDesactivar.addActionListener(this);
        Botones(false, false, true, false);
        cargarTabla();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pes.btn_Ingresar) { //////******* BTN INGRESAR
            if (CamposVacios() == true) {
            } else {
                if (VericarCliente("El Cargo se encuentra en la Base de Datos") == true) {
                    Ingresar();
                    Botones(true, true, false, true);
                } else {

                }
            }
        } else if (e.getSource() == pes.btn_Modificar) {//////////******* BTN MODIFICAR
            if (CamposVacios() == true) {

            } else {
                Modificar();
            }
        } else if (e.getSource() == pes.btn_Nuevo) {
            Limpiar();
            ActualizarId();
            Botones(true, false, true, false);
        } else if (e.getSource() == pes.btn_Eliminar) {
            if (CamposVacios() == true) {
            } else {
                Eliminar();
                Limpiar();
            }
        } else if (e.getSource() == pes.JmenuDesactivar) {
            if (CamposVacios()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                Desactivar("Inactivo");
                cargarTabla();
                Botones(false, false, false,true);
                Limpiar();
                ActualizarId();
            }
        } else if (e.getSource() == pes.JmenuActivar) {
            if (CamposVacios()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                Desactivar("Activo");
                cargarTabla();
                Botones(false, false, false,true);
                Limpiar();
                ActualizarId();
            }
        }
    }

    public boolean CamposVacios() {
        if (pes.txt_Nombre.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Digite los Campos vacios", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    public boolean VericarCliente(String textosino) {
        String cliente = pes.txt_Nombre.getText();
        String nombreSql = "";
        String rfc = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Cargo FROM Cargo WHERE Cargo = ?");
            ps.setString(1, cliente);
            rs = ps.executeQuery();
            while (rs.next()) {
                rfc = (rs.getString("Cargo"));
            }
            if (rfc.isEmpty()) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, textosino);
                return false;
            }

        } catch (Exception e) {
            System.out.println(" Error: Verificar " + e.getMessage());
        }
        return false;
    }

    public void Ingresar() {
        int id = Integer.parseInt(pes.txt_id.getText());
        String Nombre = pes.txt_Nombre.getText();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO Cargo "
                    + " (id, Cargo) VALUES (?,?)");
            ps.setInt(1, id);
            ps.setString(2, Nombre);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Ingreso el Cargo correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    public void Modificar() {
        int id = Integer.parseInt(pes.txt_id.getText());
        String Nombre = pes.txt_Nombre.getText();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE Cargo SET Cargo = ? WHERE id = ?");
            ps.setString(1, Nombre);
            ps.setInt(2, id);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Modifico El Cargo correctamente");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void Eliminar() {
        int id = Integer.parseInt(pes.txt_id.getText());
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("DELETE Cargo WHERE id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Elimino el Cargo correctamente");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void Desactivar(String Estado) {
        int id = Integer.parseInt(pes.txt_id.getText());
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE Cargo SET Estado = ? WHERE id = ?");
                ps.setString(1, Estado);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTabla();
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        
    }

    public void cargarTabla() {
        TbProducto tableColor = new TbProducto();
        pes.tb_Permisionario.setDefaultRenderer(pes.tb_Permisionario.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id, Cargo, Estado FROM Cargo ORDER BY id ASC, Estado ASC";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en CARGARtabla Cargo: " + e.toString());
        }
    }

    public void ActualizarId() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM Cargo");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            pes.txt_id.setText("" + idAux);
        } catch (Exception e) {
            System.err.println("error ActFolio: " + e.getMessage());
        }
    }

    public void Limpiar() {
        pes.txt_id.setText("");
        pes.txt_Nombre.setText("");
        pes.txt_estado.setText("");
    }

    public void Botones(boolean ver, boolean ver1, boolean ver2, boolean ver3) {
        pes.btn_Eliminar.setVisible(ver1);
        pes.btn_Modificar.setVisible(ver1);
        pes.btn_Modificar.setEnabled(ver1);
        pes.btn_Ingresar.setEnabled(ver2);
        pes.btn_Nuevo.setVisible(ver3);

    }

    //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            try {
                int fila = pes.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(pes.tb_Permisionario.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Cargo, Estado FROM Cargo WHERE id = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pes.txt_id.setText("" + rs.getInt("id"));
                    pes.txt_Nombre.setText(rs.getString("Cargo"));
                    pes.txt_estado.setText(rs.getString("Estado"));

                }
                Botones(true, true, false, true);
                pes.lb_selc.setVisible(false);
                if (pes.txt_estado.getText().equals("Inactivo")) {
                    pes.JmenuActivar.setVisible(true);
                    pes.JmenuDesactivar.setVisible(false);
                } else if (pes.txt_estado.getText().equals("Activo")) {
                    pes.JmenuActivar.setVisible(false);
                    pes.JmenuDesactivar.setVisible(true);
                }
            } catch (Exception er) {
                System.err.println("Error en tabla Cargo: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            pes.lb_selc.setVisible(true);
            pes.lb_selc.setText("Haga Click en una fila para Modificar");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            pes.lb_selc.setVisible(false);
        }
    }

    public void keyTyped(KeyEvent e) {

    }

    //*****EVENTO DEL TECLADO AL PONERSE EN EL TXT_ID, PARA PODER REALIZAR CIERTAS ACCIONES
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {

        }
    }

    //********** REALIZA UNA COSULTA EN LA BASE DE DATOS PARA LOCALIZAR LOS DATOS DEL PERMISIONARIO
    @Override
    public void keyReleased(KeyEvent e) {

    }

}
