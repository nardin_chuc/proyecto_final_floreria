/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Interfaz.Pedidos;
import Modelos.Tabla;
import Modelos.TbPedidos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorPedidos implements ActionListener, MouseListener, KeyListener {

    Pedidos pe;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
    String fecha = "";
    int id_Cliente = 0;
    int id[];
    String FechaEvento[];

    public ControladorPedidos(Pedidos pe) {
        this.pe = pe;
        ActualizarId();
        DiasAutomaticos();
        cargarTabla();
        Botones(true, false, false);
        this.pe.btn_Ingresar.addActionListener(this);
        this.pe.btn_Modificar.addActionListener(this);
        this.pe.btn_Nuevo.addActionListener(this);
        this.pe.btn_Eliminar.addActionListener(this);
        this.pe.cbx_cliente1.addKeyListener(this);
        this.pe.cbx_cliente1.addMouseListener(this);
        this.pe.txt_Anticipo.addKeyListener(this);
        this.pe.txt_PrecioTotal.addKeyListener(this);
        this.pe.tb_Pedidos.addMouseListener(this);
        this.pe.JmenuActivar.addActionListener(this);
        this.pe.JmenuDesactivar.addActionListener(this);
    }

    
    

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pe.btn_Ingresar) {
            if (CamposVacios()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                Ingresar();
                cargarTabla();
                limpiar();
                ActualizarId();
            }
        } else if (e.getSource() == pe.btn_Modificar) {
            if (CamposVacios()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
            Modificar();
            Botones(true, true, true);
            }
        } else if (e.getSource() == pe.btn_Nuevo) {
            limpiar();
            ActualizarId();
            Botones(true, false, false);
        } else if (e.getSource() == pe.JmenuDesactivar) {
            if (CamposVacios()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
            Desactivar("Cancelado");
            cargarTabla();
            Botones(false, true, true);
            limpiar();
            ActualizarId();
            }
        } else if (e.getSource() == pe.JmenuActivar) {
            if (CamposVacios()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
            Modificar();
            cargarTabla();
            Botones(false, true, true);
            limpiar();
            ActualizarId();
            }
        }else if(e.getSource() == pe.btn_Eliminar){
            if(CamposVacios()){
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            }else{
                cargarTabla();
                Eliminar();
                limpiar();
                ActualizarId();
                pe.btn_Eliminar.setVisible(false);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pe.cbx_cliente1) {
            LLenarCBX();
        } else if (e.getSource() == pe.tb_Pedidos) {
            try {
                int fila = pe.tb_Pedidos.getSelectedRow();
                int id = Integer.parseInt(pe.tb_Pedidos.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id_pedido, Nombre, Telefono, "
                        + "PrecioTotal, Anticipo, Saldo, FechaEvento, HoraEvento, Descripcion, DiasPendEstado"
                        + " FROM db_Pedidos WHERE id_pedido = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pe.txt_id.setText("" + rs.getInt("id_pedido"));
                    pe.cbx_cliente1.setSelectedItem(rs.getString("Nombre"));
                    pe.txt_telefono.setText(rs.getString("Telefono"));
                    pe.txt_PrecioTotal.setText(rs.getString("PrecioTotal"));
                    pe.txt_Anticipo.setText(rs.getString("Anticipo"));
                    pe.txt_Saldo.setText(rs.getString("Saldo"));
                    SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yy");
                    Date fechaEvento = formato.parse((rs.getString("FechaEvento")));
                    try {
                        pe.jcalendar_Pedido.setDate(fechaEvento);
                    } catch (Exception ev) {

                    }
                    if (pe.txt_estado.getText().equals("Cancelado")) {
                        pe.JmenuActivar.setVisible(true);
                        pe.JmenuDesactivar.setVisible(false);
                    } else if(pe.txt_estado.getText().equals("Preparación") || pe.txt_estado.getText().equals("Pendiente") || pe.txt_estado.getText().equals("Hoy"))  {
                        pe.JmenuActivar.setVisible(false);
                        pe.JmenuDesactivar.setVisible(true);
                    }
                    pe.txt_hora.setText(rs.getString("HoraEvento"));
                    pe.textArea_descrip.setText(rs.getString("Descripcion"));
                    pe.txt_estado.setText(rs.getString("DiasPendEstado"));
                }
                Botones(false, true, true);
                pe.lb_selc.setVisible(false);
                
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == pe.tb_Pedidos) {
            pe.lb_selc.setVisible(true);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == pe.tb_Pedidos) {
            pe.lb_selc.setVisible(false);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == pe.cbx_cliente1) {
                int id = (pe.cbx_cliente1.getSelectedIndex() + 1);
                encontrarTelefono(id);
                pe.txt_PrecioTotal.requestFocus(true);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == pe.txt_PrecioTotal) {
            Float anticipo = 0f;
            Float saldo = 0f;
            if (pe.txt_PrecioTotal.getText().isEmpty()) {
                anticipo = 0f;
                saldo = 0f;
                pe.txt_Anticipo.setText("" + anticipo);
                pe.txt_Saldo.setText("" + saldo);
            } else {
                Float precioTotal = Float.parseFloat(pe.txt_PrecioTotal.getText());
                anticipo = (precioTotal / 2);
                saldo = precioTotal - anticipo;
                pe.txt_Saldo.setText("" + saldo);
                pe.txt_Anticipo.setText("" + anticipo);
            }

        } else if (e.getSource() == pe.txt_Anticipo) {
            Float anticipo = 0f;
            Float saldo = 0f;
            if (pe.txt_PrecioTotal.getText().isEmpty() || pe.txt_Anticipo.getText().isEmpty()) {
                anticipo = 0f;
                saldo = 0f;
                pe.txt_Saldo.setText("" + saldo);
            } else {
                Float precioTotal = Float.parseFloat(pe.txt_PrecioTotal.getText());
                anticipo = Float.parseFloat(pe.txt_Anticipo.getText());
                saldo = precioTotal - anticipo;
                pe.txt_Saldo.setText("" + saldo);
            }
        }
    }

    public void Ingresar() {
        String año = pe.txt_AÑO.getText();
        String MES = pe.txt_Mes.getText();
        String FechaRegistro = pe.txt_fecha.getText();
        int id = Integer.parseInt(pe.txt_id.getText());
        String Nombre = pe.cbx_cliente1.getSelectedItem().toString();
        String telefono = pe.txt_telefono.getText();
        Float PagoTotal = Float.parseFloat(pe.txt_PrecioTotal.getText());
        Float Anticipo = Float.parseFloat(pe.txt_Anticipo.getText());
        Float Saldo = Float.parseFloat(pe.txt_Saldo.getText());
        String hora = pe.txt_hora.getText();
        String Descripcion = pe.textArea_descrip.getText();
        String usuario = pe.txt_user.getText();
        fecha = dateFormat.format(pe.jcalendar_Pedido.getCalendar().getTime());
        int diasFaltantes = 0;
        if (DiasRestantes(fecha) < 0) {
            diasFaltantes = 0;
        } else if (DiasRestantes(fecha) >= 0) {
            diasFaltantes = DiasRestantes(fecha);
        }
        String diasEstado = EstadoDias(fecha);

        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO db_Pedidos (AÑO, MES, id_pedido, Nombre, Telefono, FechaRegistro, "
                    + "PrecioTotal, Anticipo, Saldo, FechaEvento, HoraEvento, Descripcion, Usuario, DiasFaltantes, DiasPendEstado) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, año);
            ps.setString(2, MES);
            ps.setInt(3, id);
            ps.setString(4, Nombre);
            ps.setString(5, telefono);
            ps.setString(6, FechaRegistro);
            ps.setFloat(7, PagoTotal);
            ps.setFloat(8, Anticipo);
            ps.setFloat(9, Saldo);
            ps.setString(10, fecha);
            ps.setString(11, hora);
            ps.setString(12, Descripcion);
            ps.setString(13, usuario);
            ps.setInt(14, diasFaltantes);
            ps.setString(15, diasEstado);
            ps.executeUpdate();
            cargarTabla();
            ps.close();
            JOptionPane.showMessageDialog(null, "Se Ingreso correctamente el Pedido");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    //Falta el metodo de Modificar
    public void Modificar() {
        String año = pe.txt_AÑO.getText();
        String MES = pe.txt_Mes.getText();
        String FechaRegistro = pe.txt_fecha.getText();
        int id = Integer.parseInt(pe.txt_id.getText());
        String Nombre = pe.cbx_cliente1.getSelectedItem().toString();
        String telefono = pe.txt_telefono.getText();
        Float PagoTotal = Float.parseFloat(pe.txt_PrecioTotal.getText());
        Float Anticipo = Float.parseFloat(pe.txt_Anticipo.getText());
        Float Saldo = Float.parseFloat(pe.txt_Saldo.getText());
        String hora = pe.txt_hora.getText();
        String Descripcion = pe.textArea_descrip.getText();
        String usuario = pe.txt_user.getText();
        fecha = dateFormat.format(pe.jcalendar_Pedido.getCalendar().getTime());
        int diasFaltantes = 0;
        if (DiasRestantes(fecha) < 0) {
            diasFaltantes = 0;
        } else if (DiasRestantes(fecha) >= 0) {
            diasFaltantes = DiasRestantes(fecha);
        }
        String diasEstado = EstadoDias(fecha);
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE db_Pedidos SET AÑO = ?, MES = ?, Nombre = ?, Telefono = ?, FechaRegistro = ?, "
                    + "PrecioTotal = ?, Anticipo = ?, Saldo = ?, FechaEvento = ?, HoraEvento = ?, Descripcion = ?, Usuario = ?, DiasFaltantes = ?, DiasPendEstado = ? "
                    + "WHERE id_pedido = ?");
            ps.setString(1, año);
            ps.setString(2, MES);
            ps.setString(3, Nombre);
            ps.setString(4, telefono);
            ps.setString(5, FechaRegistro);
            ps.setFloat(6, PagoTotal);
            ps.setFloat(7, Anticipo);
            ps.setFloat(8, Saldo);
            ps.setString(9, fecha);
            ps.setString(10, hora);
            ps.setString(11, Descripcion);
            ps.setString(12, usuario);
            ps.setInt(13, diasFaltantes);
            ps.setString(14, diasEstado);
            ps.setInt(15, id);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Modifico el Pedido correctamente");
            ps.close();
        } catch (Exception e) {
            System.out.println("Error modifi: " + e.getMessage());
        }
    }
    
    public void Eliminar(){
        int id = Integer.parseInt(pe.txt_id.getText());
        String nombre = pe.cbx_cliente1.getSelectedItem().toString();
        try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("DELETE db_Pedidos WHERE id_pedido = ? AND Nombre = ?");
                ps.setInt(1, id);
                ps.setString(2, nombre);
                ps.executeUpdate();
                JOptionPane.showMessageDialog(null, "Se Elimino correctamente");
                ps.close();
                pe.btn_Eliminar.setVisible(false);
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
    }

    public void Desactivar(String Estado) {
        String fecha = pe.txt_fecha.getText();
        int id = Integer.parseInt(pe.txt_id.getText());
        String cliente = pe.cbx_cliente1.getSelectedItem().toString();
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE db_Pedidos SET DiasPendEstado = ? WHERE id_pedido = ? AND Nombre = ?");
                ps.setString(1, Estado);
                ps.setInt(2, id);
                ps.setString(3, cliente);
                ps.executeUpdate();
                ps.close();
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        
    }

    public boolean CamposVacios() {
        if (pe.txt_telefono.getText().isEmpty() || pe.txt_PrecioTotal.getText().isEmpty() || pe.textArea_descrip.getText().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public void Botones(boolean ver, boolean ver2, boolean ver3) {
        pe.btn_Eliminar.setVisible(ver2);
        pe.btn_Modificar.setVisible(ver2);
        pe.btn_Ingresar.setVisible(ver);
        pe.btn_Nuevo.setVisible(ver3);

    }

    public void ActualizarId() {            //// ACTUALIZA EL ID DE CADA PEDIDO
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id_pedido) AS id_Maximo FROM db_Pedidos");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            pe.txt_id.setText("" + idAux);
            rs.close();
        } catch (Exception e) {
            System.err.println("error ActFolio: " + e.getMessage());
        }
    }

    public int DiasRestantes(String fechaevento) {           //CALCULA LOS DIAS QUE FALTAN PARA EL EVENTO
        String fecha_hoy = pe.txt_fecha.getText();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
        int diasf = 0;
        try {
            Date date1 = dateFormat.parse(fecha_hoy);
            Date date2 = dateFormat.parse(fechaevento);
            long tiempoTrans = date2.getTime() - date1.getTime();
            TimeUnit unidad = TimeUnit.DAYS;
            TimeUnit unidad2 = TimeUnit.DAYS;
            long tiempoTrans2 = (date2.getTime() - date1.getTime());
            long diasFaltantes = unidad.convert(tiempoTrans, TimeUnit.MILLISECONDS);
            diasf = Integer.parseInt("" + diasFaltantes);
        } catch (Exception e) {
            System.out.println("erro en calcular dias: " + e.getMessage());
        }
        return diasf;
    }
    
    public int buscarTamañoArray(){
        int tamaño = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT COUNT(Nombre) AS tamaño FROM db_Pedidos WHERE DiasPendEstado = 'Hoy' OR DiasPendEstado = 'Pendiente' OR DiasPendEstado = 'Registro Activo' "
                    + "OR DiasPendEstado = 'Preparación'");
            rs = ps.executeQuery();
            while (rs.next()) {
                tamaño = (rs.getInt("tamaño"));
                System.out.println("tamaño "+tamaño);
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error buscarTamaño Array: " + e.getMessage());
     }
        return tamaño;
    }
    
    public void AsinarId_fecha(){
        int tamaño = buscarTamañoArray();
        id = new int[tamaño];
        FechaEvento = new String[tamaño];
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT id_pedido, FechaEvento FROM db_Pedidos WHERE DiasPendEstado = 'Hoy' OR DiasPendEstado = 'Pendiente' OR DiasPendEstado = 'Registro Activo' "
                    + "OR DiasPendEstado = 'Preparación' ORDER BY id_pedido ASC");
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                id[i] = rs.getInt("id_pedido");
                FechaEvento[i] = rs.getString("FechaEvento");
                i++;
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("error AsinarId_fecha: " + e.getMessage());
     }
    }
    
    public void DiasAutomaticos(){
            AsinarId_fecha();
            String fecha = "";
            String Estado = "";
            int dias = 0;
           try {
            for (int i = 0; i < id.length; i++) {
                fecha = FechaEvento[i];
                if (DiasRestantes(fecha) < 0) {
                    dias = 0;
                } else if (DiasRestantes(fecha) >= 0) {
                    dias = DiasRestantes(fecha);
                }
             Estado = EstadoDias(fecha);
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("UPDATE db_Pedidos SET DiasFaltantes = ?, DiasPendEstado = ? WHERE id_pedido = "+id[i]);
            ps.setInt(1, dias);
            ps.setString(2, Estado);
            ps.executeUpdate();
            ps.close();
        }
           
        } catch (Exception e) {
            System.err.println("error DiasAutomaticos: " + e.getMessage());
     }
            
    }
    

    public String EstadoDias(String fecha) {
        String texto = "";
        int dias = DiasRestantes(fecha);
        if (dias < 0) {
            texto = "Terminado";
        } else if (dias == 0) {
            texto = "Hoy";
        } else if (dias > 0 && dias <= 3) {
            texto = "Preparación";
        } else if (dias > 3 && dias <= 10) {
            texto = "Pendiente";
        } else if (dias >= 11) {
            texto = "Registro Activo";
        }
        return texto;
    }

    public void encontrarTelefono(int id) {
        String nombre = pe.cbx_cliente1.getSelectedItem().toString();

        String telefono = "";
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Telefono FROM db_Cliente WHERE id_Cliente = " + id);
            rs = ps.executeQuery();
            while (rs.next()) {
                telefono = (rs.getString("Telefono"));
            }
            pe.txt_telefono.setText(telefono);
            rs.close();
        } catch (Exception e) {
        }
    }

    public void cargarTabla() {
        TbPedidos tableColor = new TbPedidos();
        pe.tb_Pedidos.setDefaultRenderer(pe.tb_Pedidos.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) pe.tb_Pedidos.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {80, 225, 175, 175, 130, 130, 300, 175, 110, 125, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pe.tb_Pedidos.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_pedido, Nombre, Telefono, FechaRegistro, PrecioTotal, "
                    + "Saldo, Descripcion, FechaEvento, HoraEvento, DiasFaltantes, DiasPendEstado"
                    + " FROM db_Pedidos WHERE DiasPendEstado != 'Terminado' ORDER BY DiasFaltantes ASC";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en CARGARtabla: " + e.toString());
        }
    }

    public void LLenarCBX() {
        try {

            pe.cbx_cliente1.removeAllItems();
            PreparedStatement ps;
            ResultSet rs;
            String Estado = "Activo";
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Nombre, Apellido  FROM db_Cliente WHERE Estado = ? ORDER BY id_Cliente");
            ps.setString(1, Estado);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                String nom = rs.getString("Nombre");
                String apellido = rs.getString("Apellido");
                String NomCompleto = nom + " " + apellido;
                pe.cbx_cliente1.addItem(NomCompleto);
                i++;
            }
            rs.close();

        } catch (Exception er) {
            System.err.println("Error en cbx Cliente: " + er.toString());
        }

    }

    //lIMPIA LOS ESPACIOS ESCRITOS
    public void limpiar() {
        pe.txt_telefono.setText("");
        pe.txt_PrecioTotal.setText("");
        pe.txt_Saldo.setText("");
        pe.txt_hora.setText("");
        pe.txt_Anticipo.setText("");
        pe.cbx_cliente1.requestFocus();
        pe.textArea_descrip.setText("");
        pe.jcalendar_Pedido.setDate(null);
    }

} ///// fin de la clase

