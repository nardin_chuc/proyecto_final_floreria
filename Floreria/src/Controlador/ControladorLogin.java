package Controlador;

import Conexion.Conexion;
import Interfaz.Bienvenida;
import Interfaz.Login;
import Interfaz.Notificaciones;
import Interfaz.Pedidos;
import Modelos.HiloNotificaciones;
import Modelos.HttpJavaAltiria;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ControladorLogin implements ActionListener, MouseListener, KeyListener {

    Login lg;
    
    //public Notificaciones not = new Notificaciones();
   // ControladorNotificaciones nt = new ControladorNotificaciones(not);
    String Cargo = "";
    String user2 = "";
    int id[];
    String FechaEvento[];
    HttpJavaAltiria msj = new HttpJavaAltiria();
   // HiloNotificaciones h1 = new HiloNotificaciones();

    public ControladorLogin(Login lg) {
        this.lg = lg;
//        DiasAutomaticos();
        this.lg.btn_Cancelar.addActionListener(this);
        this.lg.btn_Ingresar.addActionListener(this);
        this.lg.txt_Contra.addKeyListener(this);
        this.lg.btn_Ingresar.addKeyListener(this);
        this.lg.btn_Cancelar.addMouseListener(this);
        this.lg.btn_Ingresar.addMouseListener(this);
    }

    public ControladorLogin() {

    }
    
    

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == lg.btn_Cancelar) {
            int op = JOptionPane.showConfirmDialog(null, "¿ Desea Salir ?", "Salir", JOptionPane.INFORMATION_MESSAGE);
            if (op == 1) {
                lg.txt_user.requestFocus(true);
            } else {
                JOptionPane.showMessageDialog(null, "Saliendo");
                System.exit(0);
            }
        } else if (e.getSource() == lg.btn_Ingresar) {
            if (lg.txt_Contra.getText().isEmpty() || lg.txt_user.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios", "Verificar Campos", JOptionPane.ERROR_MESSAGE);
            } else {
                if (verificarUser() == true) {
                    JOptionPane.showMessageDialog(null, "Usuario y/o Contraseña Incorrectas", "Error al Ingresar", JOptionPane.ERROR_MESSAGE);
                    limpiar();
                    lg.txt_user.requestFocus(true);
                } else {
                    ColocarCargo(Cargo, user2);
                }
            }

        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == lg.btn_Cancelar) {
            lg.btn_Cancelar.setBackground(Color.red);
        } else if (e.getSource() == lg.btn_Ingresar) {
            lg.btn_Ingresar.setBackground(Color.red);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == lg.btn_Cancelar) {
            lg.btn_Cancelar.setBackground(Color.white);
        } else if (e.getSource() == lg.btn_Ingresar) {
            lg.btn_Ingresar.setBackground(Color.white);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == lg.txt_Contra) {
                if (lg.txt_Contra.getText().toString().isEmpty() || lg.txt_user.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Campos Vacios", "Verificar Campos", JOptionPane.ERROR_MESSAGE);
                } else {
                    if (verificarUser() == true) {
                        JOptionPane.showMessageDialog(null, "Usuario y/o Contraseña Incorrectas o se encuentra Inactivo", "Error al Ingresar", JOptionPane.ERROR_MESSAGE);
                        limpiar();
                        lg.txt_user.requestFocus(true);
                    } else {
                        ColocarCargo(Cargo, user2);
                    }
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public boolean verificarUser() {
        try {
            String user = lg.txt_user.getText();
            String contra = lg.txt_Contra.getText().toString();

            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Usuario, Cargo FROM db_Usuarios WHERE Usuario = ? AND Contraseña = ? AND Estado = 'Activo'");
            ps.setString(1, user);
            ps.setString(2, contra);
            rs = ps.executeQuery();
            while (rs.next()) {
                user2 = (rs.getString("Usuario"));
                Cargo = (rs.getString("Cargo"));
            }
            rs.close();
            if (user2.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("error Verificar login " + e.getMessage());
        }
        return false;
    }

    public void ColocarCargo(String cargo, String user) {
        Bienvenida bn = new Bienvenida();
        bn.txt_user.setText(user);
        bn.txt_cargo.setText(cargo);
        if ((cargo.equals("Vendedor"))) {
            bn.jMenu1.setVisible(false);
            bn.jMenu5.setVisible(false);
            bn.jMenu5.setVisible(false);
            bn.setVisible(true);
           // h1.EjecutarNotificaciones();
            lg.dispose();
            
        } else if (cargo.equals("Almacenista")) {
            bn.jMenu4.setVisible(false);
            bn.jMenu2.setVisible(false);
            bn.jMenu6.setVisible(false);
            bn.jMenu1.setVisible(true);
            bn.AgregarUser.setVisible(false);
            bn.jMenuItem2.setVisible(false);
            bn.setVisible(true);
           // h1.EjecutarNotificaciones();
            lg.dispose();
        } else if (cargo.equals("Administrador")) {
            bn.jMenu1.setVisible(true);
            bn.setVisible(true);
            //h1.EjecutarNotificaciones();
            lg.dispose();
        }

    }

    public void limpiar() {
        lg.txt_Contra.setText("");
        lg.txt_user.setText("");
    }

//    public int DiasRestantes(String fechaevento) {           //CALCULA LOS DIAS QUE FALTAN PARA EL EVENTO
//        String fecha_hoy = lg.txt_fecha.getText();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
//        int diasf = 0;
//        try {
//            Date date1 = dateFormat.parse(fecha_hoy);
//            Date date2 = dateFormat.parse(fechaevento);
//            long tiempoTrans = date2.getTime() - date1.getTime();
//            TimeUnit unidad = TimeUnit.DAYS;
//            TimeUnit unidad2 = TimeUnit.DAYS;
//            long tiempoTrans2 = (date2.getTime() - date1.getTime());
//            long diasFaltantes = unidad.convert(tiempoTrans, TimeUnit.MILLISECONDS);
//            diasf = Integer.parseInt("" + diasFaltantes);
//
//        } catch (Exception e) {
//            System.out.println("erro en calcular dias: " + e.getMessage());
//        }
//        return diasf;
//    }

//    public int buscarTamañoArray() {
//        int tamaño = 0;
//        try {
//            PreparedStatement ps;
//            ResultSet rs;
//            Connection con = Conexion.establecerConnection();
//            ps = con.prepareStatement("SELECT COUNT(Nombre) AS tamaño FROM db_Pedidos WHERE DiasPendEstado = 'Hoy' OR DiasPendEstado = 'Pendiente' OR DiasPendEstado = 'Registro Activo' "
//                    + "OR DiasPendEstado = 'Preparación'");
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                tamaño = (rs.getInt("tamaño"));
//            }
//            rs.close();
//        } catch (Exception e) {
//            System.err.println("error buscarTamaño Array: " + e.getMessage());
//        }
//        return tamaño;
//    }
//
//    public void AsinarId_fecha() {
//        int tamaño = buscarTamañoArray();
//        id = new int[tamaño];
//        FechaEvento = new String[tamaño];
//        try {
//            PreparedStatement ps;
//            ResultSet rs;
//            Connection con = Conexion.establecerConnection();
//            ps = con.prepareStatement("SELECT id_pedido, FechaEvento FROM db_Pedidos WHERE DiasPendEstado = 'Hoy' OR DiasPendEstado = 'Pendiente' OR DiasPendEstado = 'Registro Activo' "
//                    + "OR DiasPendEstado = 'Preparación' ORDER BY id_pedido ASC");
//            rs = ps.executeQuery();
//            int i = 0;
//            while (rs.next()) {
//                id[i] = rs.getInt("id_pedido");
//                FechaEvento[i] = rs.getString("FechaEvento");
//                i++;
//            }
//            rs.close();
//        } catch (Exception e) {
//            System.err.println("error AsinarId_fecha: " + e.getMessage());
//        }
//    }

//    public void DiasAutomaticos() {
//        AsinarId_fecha();
//        String fecha = "";
//        String Estado = "";
//        int dias = 0;
//        try {
//            for (int i = 0; i < FechaEvento.length; i++) {
//                fecha = FechaEvento[i];
//                if (DiasRestantes(fecha) < 0) {
//                    dias = 0;
//                } else if (DiasRestantes(fecha) >= 0) {
//                    dias = DiasRestantes(fecha);
//                }
//                Estado = EstadoDias(fecha);
//                PreparedStatement ps;
//                ResultSet rs;
//                Connection con = Conexion.establecerConnection();
//                ps = con.prepareStatement("UPDATE db_Pedidos SET DiasFaltantes = ?, DiasPendEstado = ? WHERE id_pedido = " + id[i]);
//                ps.setInt(1, dias);
//                ps.setString(2, Estado);
//                ps.executeUpdate();
//                ps.close();
//            }
//
//        } catch (Exception e) {
//            System.err.println("error DiasAutomaticos: " + e.getMessage());
//        }
//
//    }

//    public String EstadoDias(String fecha) {
//        String texto = "";
//
//        int dias = DiasRestantes(fecha);
//        if (dias < 0) {
//            texto = "Terminado";
//        } else if (dias == 0) {
//            texto = "Hoy";
//        } else if (dias > 0 && dias <=3) {
//            texto = "Preparación";
//        } else if (dias > 3 && dias <= 10) {
//            texto = "Pendiente";
//        } else if (dias >= 11) {
//            texto = "Registro Activo";
//        }
//        return texto;
//    }

//    public String buscarDescripcionPedido() {
//        String Descripcion = "";
//        String Nombre = "";
//        String HoraEvento = "";
//        String Saldo = "";
//        String dias = "";
//        String Mesaje = "";
//        String estado = "";
//        String MesajeTotal = "";
//        Connection conectar = Conexion.establecerConnection();
//        ResultSet rs;
//        PreparedStatement ps;
//        try {
////            ps = conectar.prepareStatement("SELECT Nombre, Descripcion, HoraEvento, DiasFaltantes,  Saldo, DiasPendEstado FROM db_Pedidos WHERE DiasPendEstado = 'Hoy' OR DiasPendEstado = 'Preparación'");
//            ps = conectar.prepareStatement("SELECT Nombre, Descripcion, HoraEvento, DiasFaltantes,  Saldo, DiasPendEstado FROM db_Pedidos WHERE DiasPendEstado = 'Hoy'");
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                Nombre = (rs.getString("Nombre"));
//                Descripcion = (rs.getString("Descripcion"));
//                HoraEvento = (rs.getString("HoraEvento"));
//                dias = ("" + rs.getInt("DiasFaltantes"));
//                Saldo = ("" + rs.getFloat("Saldo"));
//                estado = ("" + rs.getString("DiasPendEstado"));
//                Mesaje = estado + " \n"
//                        + "Nombre: " + Nombre + "\n"
//                        + "Descripción: " + Descripcion + "\n"
//                        + "Hora del Evento: " + HoraEvento + "\n"
////                        + "Dias Faltantes: "+dias+" dia(s) \n"
//                        + "Saldo Deudor: " + Saldo + "\n\n";
//                MesajeTotal = MesajeTotal + Mesaje;
//            }
//            rs.close();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return MesajeTotal;
//    }

    public String BuscarTelefono() {
        String telefono = "";
        Connection conectar = Conexion.establecerConnection();
        ResultSet rs;
        PreparedStatement ps;
        try {
            ps = conectar.prepareStatement("SELECT Telefono FROM db_Usuarios WHERE Cargo = ? AND Estado = 'Activo'");
            ps.setString(1, "Administrador");
            rs = ps.executeQuery();
            while (rs.next()) {
                telefono = (rs.getString("Telefono"));
            }
            System.out.println("tefe "+telefono);
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
        }

        return telefono;
    }
    
    
   /* public String EnviarMensaje(){
        String telefono = BuscarTelefono();
        String mensaje ="";
        mensaje = "-- Floreria 'Los Chuc' -- Buen Dia Administrador se Presentan los siguientes Pendientes: ";
        mensaje = mensaje + nt.EnviarMsg();
        return mensaje;
    }*/

}
