USE [db_Floreria_PF]
GO
/****** Object:  Table [dbo].[Cargo]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cargo](
	[Cargo] [varchar](50) NULL,
	[id] [int] NOT NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_Cargo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_Cliente]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_Cliente](
	[id_Cliente] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_db_Cliente] PRIMARY KEY CLUSTERED 
(
	[id_Cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_Compras]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_Compras](
	[id_compra] [int] NOT NULL,
	[AÑO] [varchar](10) NULL,
	[MES] [varchar](10) NULL,
	[Fecha] [varchar](10) NULL,
	[Producto] [varchar](50) NULL,
	[id_Producto] [int] NULL,
	[Proveedor] [varchar](50) NULL,
	[id_Proveedor] [int] NULL,
	[Usuario] [varchar](50) NULL,
	[id_user] [int] NULL,
	[Cantidad] [int] NULL,
	[Precio_Compra] [float] NULL,
	[Total_Compra] [float] NULL,
	[id_Perecedero] [int] NULL,
	[Estado] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_Floreria_Productos]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_Floreria_Productos](
	[id] [int] NOT NULL,
	[Producto] [varchar](50) NULL,
	[Precio_Compra] [float] NULL,
	[Precio_Venta] [float] NULL,
	[Cantidad] [int] NULL,
	[id_Proveedor] [int] NULL,
	[Proveedor] [varchar](50) NULL,
	[Ubicacion] [varchar](50) NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_db_Floreria_Productos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_Floreria_Proveedor]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_Floreria_Proveedor](
	[id_Proveedor] [int] NOT NULL,
	[Proveedor] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_db_Proveedor] PRIMARY KEY CLUSTERED 
(
	[id_Proveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_Pedidos]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_Pedidos](
	[AÑO] [varchar](50) NULL,
	[MES] [varchar](50) NULL,
	[id_pedido] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[FechaRegistro] [varchar](50) NULL,
	[PrecioTotal] [float] NULL,
	[Anticipo] [float] NULL,
	[Saldo] [float] NULL,
	[FechaEvento] [varchar](50) NULL,
	[HoraEvento] [varchar](50) NULL,
	[Descripcion] [varchar](250) NULL,
	[Usuario] [varchar](50) NULL,
	[DiasFaltantes] [int] NULL,
	[DiasPendEstado] [varchar](50) NULL,
	[FechaMensaje] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_Perecederos]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_Perecederos](
	[Año] [varchar](50) NULL,
	[MES] [varchar](50) NULL,
	[id_perecedero] [int] NOT NULL,
	[Producto] [varchar](50) NULL,
	[Cantidad] [int] NULL,
	[FechaRegistro] [varchar](50) NULL,
	[FechaVencimiento] [varchar](50) NULL,
	[DiasPendientes] [varchar](50) NULL,
	[Estado] [varchar](50) NULL,
	[StockVencido] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_ProductosLimpieza]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_ProductosLimpieza](
	[id] [int] NOT NULL,
	[Producto] [varchar](50) NULL,
	[Precio_Compra] [float] NULL,
	[Precio_Venta] [float] NULL,
	[Cantidad] [int] NULL,
	[id_Proveedor] [int] NULL,
	[Proveedor] [varchar](50) NULL,
	[Ubicacion] [varchar](50) NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_ProductosLimpieza_Proveedor]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_ProductosLimpieza_Proveedor](
	[id_Proveedor] [int] NOT NULL,
	[Proveedor] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_db_ProductosLimpieza_Proveedor] PRIMARY KEY CLUSTERED 
(
	[id_Proveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_Usuarios]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_Usuarios](
	[id_Usuario] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[Usuario] [varchar](20) NULL,
	[Contraseña] [varchar](20) NULL,
	[Telefono] [varchar](10) NULL,
	[Cargo] [varchar](20) NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_db_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[db_Ventas]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[db_Ventas](
	[id_ventas] [int] NOT NULL,
	[AÑO] [varchar](10) NULL,
	[MES] [varchar](10) NULL,
	[Fecha] [varchar](20) NULL,
	[Producto] [varchar](50) NULL,
	[id_Producto] [int] NULL,
	[Cantidad] [int] NULL,
	[Usuario] [varchar](50) NULL,
	[id_Usuario] [int] NULL,
	[Nombre_Cliente] [varchar](50) NULL,
	[Precio_Venta] [float] NULL,
	[Subtotal] [float] NULL,
	[Total_Venta] [float] NULL,
	[Pago_Usuario] [float] NULL,
	[Estado] [varchar](50) NULL,
	[IdPerecedero] [varchar](50) NULL,
	[cantQuitar] [varchar](50) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Cargo] ADD  CONSTRAINT [DF_Cargo_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Cliente] ADD  CONSTRAINT [DF_db_Cliente_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Compras] ADD  CONSTRAINT [DF_db_Compras_Cantidad]  DEFAULT ((0)) FOR [Cantidad]
GO
ALTER TABLE [dbo].[db_Compras] ADD  CONSTRAINT [DF_db_Compras_Precio_Compra]  DEFAULT ((0.0)) FOR [Precio_Compra]
GO
ALTER TABLE [dbo].[db_Compras] ADD  CONSTRAINT [DF_db_Compras_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Floreria_Productos] ADD  CONSTRAINT [DF_db_Floreria_Productos_Cantidad]  DEFAULT ((0)) FOR [Cantidad]
GO
ALTER TABLE [dbo].[db_Floreria_Productos] ADD  CONSTRAINT [DF_db_Floreria_Productos_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Floreria_Proveedor] ADD  CONSTRAINT [DF_db_Proveedor_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Perecederos] ADD  CONSTRAINT [DF_db_Perecederos_Estado]  DEFAULT ('Util') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Perecederos] ADD  CONSTRAINT [DF_db_Perecederos_StockVencido]  DEFAULT ((0)) FOR [StockVencido]
GO
ALTER TABLE [dbo].[db_ProductosLimpieza] ADD  CONSTRAINT [DF_Productos_Cantidad]  DEFAULT ((0)) FOR [Cantidad]
GO
ALTER TABLE [dbo].[db_ProductosLimpieza] ADD  CONSTRAINT [DF_Productos_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_ProductosLimpieza_Proveedor] ADD  CONSTRAINT [DF_db_ProductosLimpieza_Proveedor_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Usuarios] ADD  CONSTRAINT [DF_db_Usuarios_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Ventas] ADD  CONSTRAINT [DF_db_Ventas_Cantidad]  DEFAULT ((0)) FOR [Cantidad]
GO
ALTER TABLE [dbo].[db_Ventas] ADD  CONSTRAINT [DF_db_Ventas_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[db_Ventas]  WITH NOCHECK ADD  CONSTRAINT [FK_db_Ventas_db_Usuarios] FOREIGN KEY([id_Usuario])
REFERENCES [dbo].[db_Usuarios] ([id_Usuario])
GO
ALTER TABLE [dbo].[db_Ventas] NOCHECK CONSTRAINT [FK_db_Ventas_db_Usuarios]
GO
/****** Object:  StoredProcedure [dbo].[SP_Compras]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Compras](
@idCompras int,
@Año varchar(10),
@Mes varchar(10),
@Fecha varchar(10),
@Producto varchar(50),
@idProducto int,
@Cantidad int,
@usuario varchar(50),
@idUsuario int,
@Proveedor varchar(50),
@idProveedor int,
@Precio_compra float,
@Total_Compra float,
@id_pere int
)
AS
BEGIN
INSERT INTO db_Compras(id_compra, AÑO, MES, Fecha, Producto, id_Producto, Cantidad, Usuario, id_user, Proveedor, id_Proveedor, Precio_Compra, Total_Compra, id_Perecedero) VALUES
(@idCompras, @Año,@Mes, @Fecha, @Producto,@idProducto, @Cantidad, @usuario,@idUsuario, @Proveedor, @idProveedor, @Precio_compra, @Total_Compra, @id_pere)

END
GO
/****** Object:  StoredProcedure [dbo].[SP_EliminarCompra]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_EliminarCompra](
@Producto varchar(50),
@Cantidad int,
@idVenta int
)
AS
BEGIN

DELETE db_Compras WHERE Producto = @Producto AND  id_compra = @idVenta AND Cantidad = @Cantidad

END
GO
/****** Object:  StoredProcedure [dbo].[SP_EliminarVenta]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_EliminarVenta](
@Producto varchar(50),
@Cantidad int,
@idVenta int
)
AS
BEGIN
DELETE db_Ventas WHERE Producto = @Producto AND  id_ventas = @idVenta AND Cantidad = @Cantidad

END
GO
/****** Object:  StoredProcedure [dbo].[SP_ModificarCompras]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_ModificarCompras](
@idCompras int,
@Año varchar(10),
@Mes varchar(10),
@Fecha varchar(10),
@Producto varchar(50),
@idProducto int,
@Cantidad int,
@Cantidad2 int,
@usuario varchar(50),
@idUsuario int,
@prov varchar(50),
@idprov int ,
@Precio_Compra float,
@Total_Compra float
)
AS
BEGIN
UPDATE  db_Compras SET AÑO = @Año, MES = @Mes, Fecha = @Fecha, Producto = @Producto, id_Producto = @idProducto, 
Cantidad = @Cantidad, Usuario = @usuario, id_user = @idUsuario, Proveedor = @prov, id_Proveedor = @idprov, 
Precio_Compra = @Precio_Compra, Total_Compra = @Total_Compra WHERE id_compra = @idCompras AND Producto = @Producto

END
GO
/****** Object:  StoredProcedure [dbo].[SP_ModificarVentas]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[SP_ModificarVentas](
@idVentas int,
@Año varchar(10),
@Mes varchar(10),
@Fecha varchar(10),
@Producto varchar(50),
@idProducto int,
@Cantidad int,
@Cantidad2 int,
@usuario varchar(50),
@idUsuario int,
@Nombre_Cliente varchar(50),
@Precio_Venta float,
@Subtotal float,
@Total_Venta float
)
AS
BEGIN
UPDATE  db_Ventas SET AÑO = @Año, MES = @Mes, Fecha = @Fecha, Producto = @Producto, id_Producto = @idProducto, 
Cantidad = @Cantidad, Usuario = @usuario, id_Usuario = @idUsuario, Nombre_Cliente = @Nombre_Cliente, 
Precio_Venta = @Precio_Venta, Subtotal = @Subtotal, Total_Venta = @Total_Venta WHERE id_ventas = @idVentas AND Producto = @Producto

UPDATE db_Productos SET Cantidad =  Cantidad - @Cantidad + @Cantidad2 WHERE Producto =  @Producto
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Ventas]    Script Date: 26/11/2022 07:14:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Ventas](
@idVentas int,
@Año varchar(10),
@Mes varchar(10),
@Fecha varchar(10),
@Producto varchar(50),
@idProducto int,
@Cantidad int,
@usuario varchar(50),
@idUsuario int,
@Nombre_Cliente varchar(50),
@Precio_Venta float,
@Subtotal float,
@Total_Venta float
)
AS
BEGIN
INSERT INTO db_Ventas (id_ventas, AÑO, MES, Fecha, Producto, id_Producto, Cantidad, Usuario, id_Usuario, Nombre_Cliente, Precio_Venta, Subtotal, Total_Venta) VALUES
(@idVentas, @Año,@Mes, @Fecha, @Producto,@idProducto, @Cantidad, @usuario,@idUsuario, @Nombre_Cliente, @Precio_Venta, @Subtotal, @Total_Venta)

END
GO
