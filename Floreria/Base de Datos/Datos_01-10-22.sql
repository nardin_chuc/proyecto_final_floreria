USE [db_Floreria_PF]
GO
INSERT [dbo].[db_Usuarios] ([id_Usuario], [Nombre], [Apellido], [Usuario], [Contraseña], [Telefono], [Cargo], [Estado]) VALUES (1, N'Alex', N'chuc', N'Alex', N'1234', N'9811327293', N'Administrador', N'Activo')
INSERT [dbo].[db_Usuarios] ([id_Usuario], [Nombre], [Apellido], [Usuario], [Contraseña], [Telefono], [Cargo], [Estado]) VALUES (2, N'Oscar ', N'Ruiz Velazquez', N'oscar', N'12345', N'981254002', N'Almacenista', N'Activo')
GO
INSERT [dbo].[db_Ventas] ([id_ventas], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Cantidad], [Usuario], [id_Usuario], [Nombre_Cliente], [Precio_Venta], [Subtotal], [Total_Venta], [Pago_Usuario], [Estado]) VALUES (1, N'2022', N'OCTUBRE', N'30-09-22', N'rosa', 1, 1, N'Alex', 1, N'nardin  chuc', 15, 15, 17.399999618530273, 500, N'Pagado')
INSERT [dbo].[db_Ventas] ([id_ventas], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Cantidad], [Usuario], [id_Usuario], [Nombre_Cliente], [Precio_Venta], [Subtotal], [Total_Venta], [Pago_Usuario], [Estado]) VALUES (2, N'2022', N'OCTUBRE', N'01-10-22', N'rosa', 1, 119, N'Alex', 1, N'Publico en General', 15, 1785, 2070.60009765625, 2000, N'Pagado')
GO
INSERT [dbo].[Cargo] ([Cargo], [id], [Estado]) VALUES (N'Adminstrador', 1, N'Activo')
INSERT [dbo].[Cargo] ([Cargo], [id], [Estado]) VALUES (N'Vendedor', 2, N'Activo')
INSERT [dbo].[Cargo] ([Cargo], [id], [Estado]) VALUES (N'Almacenista', 3, N'Activo')
GO
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (1, N'Publico en', N'General', N'', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (2, N'Juan', N'Diaz Leon', N'982225533', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (3, N'Oscar ', N'Ruiz', N'9855522', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (4, N'josue solis', N'', N'9821100376', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (5, N'nardin ', N'chuc', N'98211111111', N'Activo')
GO
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (1, N'2022', N'SEPTIEMBRE', N'20-09-22', N'rosa', 1, N'Flores Chac', 3, N'Alex', 1, 20, 15, 300, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (2, N'2022', N'SEPTIEMBRE', N'20-09-22', N'Franela roja', 2, N'Carlos Chuc', 2, N'Alex', 1, 25, 12, 300, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (1, N'2022', N'SEPTIEMBRE', N'20-09-22', N'Mechudo', 1, N'Dipral', 1, N'Alex', 1, 8, 25, 200, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (2, N'2022', N'OCTOBER', N'01-10-22', N'rosa', 1, N'Flores Chac', 0, N'Alex', 1, 20, 15, 300, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (2, N'2022', N'SEPTIEMBRE', N'30-09-22', N'Rosas Blancas', 2, N'Floreria lopez', 1, N'Alex', 1, 25, 10, 250, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (2, N'2022', N'SEPTIEMBRE', N'30-09-22', N'Mechudo', 1, N'Dipral', 1, N'Alex', 1, 2, 25, 50, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (3, N'2022', N'SEPTIEMBRE', N'30-09-22', N'rosa', 1, N'Flores Chac', 0, N'Alex', 1, 2, 15, 30, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (4, N'2022', N'SEPTIEMBRE', N'30-09-22', N'rosa', 1, N'Flores Chac', 0, N'', 0, 10, 15, 150, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [Estado]) VALUES (4, N'2022', N'OCTOBER', N'01-10-22', N'Mechudo', 1, N'Dipral', 1, N'Alex', 1, 4, 25, 100, N'Pagado')
GO
INSERT [dbo].[db_Floreria_Productos] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (1, N'rosa', 15, 20, 5, 1, N'Flores Chac', N'a5', N'Activo')
INSERT [dbo].[db_Floreria_Productos] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (2, N'Rosas Blancas', 10, 25, 25, 1, N'Floreria lopez', N'', N'Activo')
GO
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (1, N'Floreria lopez', N'985521547', N'Activo')
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (2, N'Floreria Juan', N'981122350', N'Activo')
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (4, N'Floreria josue', N'98521566', N'Activo')
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (5, N'Floreria Perez', N'982111111', N'Activo')
GO
INSERT [dbo].[db_ProductosLimpieza] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (1, N'Mechudo', 25, 30, 17, 1, N'Dipral', N'e20', N'Activo')
INSERT [dbo].[db_ProductosLimpieza] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (2, N'Franela roja', 12, 18, 123, 2, N'Carlos Chuc', N'b1', N'Activo')
GO
INSERT [dbo].[db_ProductosLimpieza_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (1, N'Dipral', N'98862215', N'Activo')
INSERT [dbo].[db_ProductosLimpieza_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (2, N'Carlos Chuc', N'982215828', N'Activo')
INSERT [dbo].[db_ProductosLimpieza_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (3, N'Josue', N'98224633', N'Activo')
GO
