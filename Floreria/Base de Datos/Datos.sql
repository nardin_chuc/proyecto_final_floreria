USE [db_Floreria_PF]
GO
INSERT [dbo].[db_Usuarios] ([id_Usuario], [Nombre], [Apellido], [Usuario], [Contraseña], [Telefono], [Cargo], [Estado]) VALUES (1, N'Alex', N'chuc', N'Alex', N'1234', N'9811327293', N'Administrador', N'Activo')
INSERT [dbo].[db_Usuarios] ([id_Usuario], [Nombre], [Apellido], [Usuario], [Contraseña], [Telefono], [Cargo], [Estado]) VALUES (2, N'Oscar ', N'Ruiz Velazquez', N'oscar', N'12345', N'981254002', N'Almacenista', N'Activo')
INSERT [dbo].[db_Usuarios] ([id_Usuario], [Nombre], [Apellido], [Usuario], [Contraseña], [Telefono], [Cargo], [Estado]) VALUES (3, N'Josue', N'May', N'josue', N'1793', N'9821100376', N'Almacenista', N'Activo')
INSERT [dbo].[db_Usuarios] ([id_Usuario], [Nombre], [Apellido], [Usuario], [Contraseña], [Telefono], [Cargo], [Estado]) VALUES (4, N'Martin', N'Manzanilla', N'Martin', N'1793', N'9811095619', N'Vendedor', N'Activo')
INSERT [dbo].[db_Usuarios] ([id_Usuario], [Nombre], [Apellido], [Usuario], [Contraseña], [Telefono], [Cargo], [Estado]) VALUES (5, N'Luis', N'Rodriguez', N'Luis', N'123456', N'9811772992', N'Vendedor', N'Activo')
INSERT [dbo].[db_Usuarios] ([id_Usuario], [Nombre], [Apellido], [Usuario], [Contraseña], [Telefono], [Cargo], [Estado]) VALUES (6, N'Armando', N'Xool', N'Armando', N'2001', N'4462338661', N'Almacenista', N'Activo')
GO
INSERT [dbo].[db_Ventas] ([id_ventas], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Cantidad], [Usuario], [id_Usuario], [Nombre_Cliente], [Precio_Venta], [Subtotal], [Total_Venta], [Pago_Usuario], [Estado], [IdPerecedero], [cantQuitar]) VALUES (1, N'2022', N'Noviembre', N'14-11-22', N'rosa', 1, 1, N'Alex', 1, N'Publico en General', 15, 15, 17.399999618530273, 40, N'Pagado', N'1,', N'[I@345062dd,1,')
INSERT [dbo].[db_Ventas] ([id_ventas], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Cantidad], [Usuario], [id_Usuario], [Nombre_Cliente], [Precio_Venta], [Subtotal], [Total_Venta], [Pago_Usuario], [Estado], [IdPerecedero], [cantQuitar]) VALUES (2, N'2022', N'Noviembre', N'14-11-22', N'rosa', 1, 1, N'Alex', 1, N'Publico en General', 15, 15, 17.399999618530273, 20, N'Pagado', N'1,', N'1,')
INSERT [dbo].[db_Ventas] ([id_ventas], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Cantidad], [Usuario], [id_Usuario], [Nombre_Cliente], [Precio_Venta], [Subtotal], [Total_Venta], [Pago_Usuario], [Estado], [IdPerecedero], [cantQuitar]) VALUES (3, N'2022', N'Noviembre', N'14-11-22', N'rosa', 1, 2, N'Alex', 1, N'Publico en General', 15, 30, 34.799999237060547, 50, N'Pagado', N'1,', N'2,')
INSERT [dbo].[db_Ventas] ([id_ventas], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Cantidad], [Usuario], [id_Usuario], [Nombre_Cliente], [Precio_Venta], [Subtotal], [Total_Venta], [Pago_Usuario], [Estado], [IdPerecedero], [cantQuitar]) VALUES (4, N'2022', N'Noviembre', N'14-11-22', N'Rosas Blancas', 2, 1, N'Alex', 1, N'Publico en General', 10, 10, 11.600000381469727, NULL, N'Activo', NULL, NULL)
INSERT [dbo].[db_Ventas] ([id_ventas], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Cantidad], [Usuario], [id_Usuario], [Nombre_Cliente], [Precio_Venta], [Subtotal], [Total_Venta], [Pago_Usuario], [Estado], [IdPerecedero], [cantQuitar]) VALUES (5, N'2022', N'Noviembre', N'14-11-22', N'Romero', 4, 5, N'Alex', 1, N'Publico en General', 35, 175, 203, 200, N'Pagado', N'2,', N'5,')
GO
INSERT [dbo].[Cargo] ([Cargo], [id], [Estado]) VALUES (N'Adminstrador', 1, N'Activo')
INSERT [dbo].[Cargo] ([Cargo], [id], [Estado]) VALUES (N'Vendedor', 2, N'Activo')
INSERT [dbo].[Cargo] ([Cargo], [id], [Estado]) VALUES (N'Almacenista', 3, N'Activo')
GO
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (1, N'Publico en', N'General', N'S/N', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (2, N'Juan', N'Diaz Leon', N'9822255335', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (3, N'Oscar ', N'Ruiz', N'9855521253', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (4, N'josue solis', N'may', N'9821100376', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (5, N'nardin ', N'chuc', N'9821111111', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (6, N'josue ', N'perez', N'9821112622', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (7, N'luzma', N'hernandez', N'9811111111', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (8, N'Maria', N'Caamal', N'9811824060', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (9, N'Angelina', N'Alpuche', N'9815263654', N'Activo')
INSERT [dbo].[db_Cliente] ([id_Cliente], [Nombre], [Apellido], [Telefono], [Estado]) VALUES (10, N'Maria', N'Sosa', N'9812534152', N'Activo')
GO
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [id_Perecedero], [Estado]) VALUES (1, N'2022', N'Noviembre', N'14-11-22', N'rosa', 1, N'Flores Chac', 0, N'Alex', 1, 10, 15, 150, 1, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [id_Perecedero], [Estado]) VALUES (1, N'2022', N'Noviembre', N'14-11-22', N'Mechudo', 1, N'Dipral', 1, N'Alex', 1, 3, 25, 75, 0, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [id_Perecedero], [Estado]) VALUES (1, N'2022', N'Noviembre', N'14-11-22', N'Franela roja', 2, N'Carlos Chuc', 2, N'Armando', 6, 10, 12, 120, 0, N'Pagado')
INSERT [dbo].[db_Compras] ([id_compra], [AÑO], [MES], [Fecha], [Producto], [id_Producto], [Proveedor], [id_Proveedor], [Usuario], [id_user], [Cantidad], [Precio_Compra], [Total_Compra], [id_Perecedero], [Estado]) VALUES (2, N'2022', N'Noviembre', N'14-11-22', N'Romero', 4, N'Floreria josue', 4, N'Alex', 1, 10, 35, 350, 2, N'Pagado')
GO
INSERT [dbo].[db_Floreria_Productos] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (1, N'rosa', 15, 20, 6, 1, N'Flores Chac', N'a5', N'Activo')
INSERT [dbo].[db_Floreria_Productos] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (2, N'Rosas Blancas', 10, 25, 7, 1, N'Floreria lopez', N'', N'Activo')
INSERT [dbo].[db_Floreria_Productos] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (3, N'Clavelin', 20, 25, 13, 1, N'Floreria lopez', N'a41', N'Activo')
INSERT [dbo].[db_Floreria_Productos] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (4, N'Romero', 26, 35, 5, 3, N'Floreria josue', N'Estante A ', N'Activo')
GO
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (1, N'Floreria lopez', N'9855215452', N'Activo')
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (2, N'Floreria Juan', N'981122350', N'Activo')
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (3, N'Floreria Gomez', N'9812253641', N'Activo')
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (4, N'Floreria josue', N'98521566', N'Activo')
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (5, N'Floreria Perez', N'982111111', N'Activo')
INSERT [dbo].[db_Floreria_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (6, N'Floreria Chapanecos', N'9821100376', N'Activo')
GO
INSERT [dbo].[db_Pedidos] ([AÑO], [MES], [id_pedido], [Nombre], [Telefono], [FechaRegistro], [PrecioTotal], [Anticipo], [Saldo], [FechaEvento], [HoraEvento], [Descripcion], [Usuario], [DiasFaltantes], [DiasPendEstado], [FechaMensaje]) VALUES (N'2022', N'Noviembre', 1, N'Juan Diaz Leon', N'9822255335', N'14-11-22', 500, 250, 250, N'15-11-22', N'3 pm', N'Boda de Marifer ', N'Alex', 1, N'Preparación', NULL)
INSERT [dbo].[db_Pedidos] ([AÑO], [MES], [id_pedido], [Nombre], [Telefono], [FechaRegistro], [PrecioTotal], [Anticipo], [Saldo], [FechaEvento], [HoraEvento], [Descripcion], [Usuario], [DiasFaltantes], [DiasPendEstado], [FechaMensaje]) VALUES (N'2022', N'Noviembre', 2, N'luzma hernandez', N'9811111111', N'14-11-22', 300, 150, 150, N'18-11-22', N'2 PM', N'XV Años', N'Alex', 4, N'Pendiente', NULL)
INSERT [dbo].[db_Pedidos] ([AÑO], [MES], [id_pedido], [Nombre], [Telefono], [FechaRegistro], [PrecioTotal], [Anticipo], [Saldo], [FechaEvento], [HoraEvento], [Descripcion], [Usuario], [DiasFaltantes], [DiasPendEstado], [FechaMensaje]) VALUES (N'2022', N'Noviembre', 3, N'Maria Caamal', N'9811824060', N'14-11-22', 360, 180, 180, N'16-11-22', N'5 pm', N'Una boda en la peña, centro de mesa con flores blancas ', N'Alex', 2, N'Preparación', NULL)
GO
INSERT [dbo].[db_Perecederos] ([Año], [MES], [id_perecedero], [Producto], [Cantidad], [FechaRegistro], [FechaVencimiento], [DiasPendientes], [Estado], [StockVencido]) VALUES (N'2022', N'Noviembre', 1, N'rosa', 6, N'14-11-22', N'24-11-22', N'10', N'Util', 0)
INSERT [dbo].[db_Perecederos] ([Año], [MES], [id_perecedero], [Producto], [Cantidad], [FechaRegistro], [FechaVencimiento], [DiasPendientes], [Estado], [StockVencido]) VALUES (N'2022', N'Noviembre', 2, N'Romero', 5, N'14-11-22', N'24-11-22', N'10', N'Util', 0)
GO
INSERT [dbo].[db_ProductosLimpieza] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (1, N'Mechudo', 25, 30, 3, 1, N'Dipral', N'e20', N'Activo')
INSERT [dbo].[db_ProductosLimpieza] ([id], [Producto], [Precio_Compra], [Precio_Venta], [Cantidad], [id_Proveedor], [Proveedor], [Ubicacion], [Estado]) VALUES (2, N'Franela roja', 12, 18, 10, 2, N'Carlos Chuc', N'b1', N'Activo')
GO
INSERT [dbo].[db_ProductosLimpieza_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (1, N'Dipral', N'98862215', N'Activo')
INSERT [dbo].[db_ProductosLimpieza_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (2, N'Carlos Chuc', N'982215828', N'Activo')
INSERT [dbo].[db_ProductosLimpieza_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (3, N'Josue', N'98224633', N'Activo')
INSERT [dbo].[db_ProductosLimpieza_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (4, N'Almacen Don Baraton', N'9811102551', N'Activo')
INSERT [dbo].[db_ProductosLimpieza_Proveedor] ([id_Proveedor], [Proveedor], [Telefono], [Estado]) VALUES (5, N'Limpiezito ', N'9812253615', N'Activo')
GO
